<?php
    require_once "../../Core/config/paths.php";

    require_once CONFIG_DIR."constants.php";
    require_once CONFIG_DIR."urls.php";
    require_once LOADER;

    use Fluid\Fluid;
    use Fluid\Session\Session;

    Session::start();

    $Fluid = Fluid::getInstance();
    $Fluid->setMode("development");
    $Fluid->setTimezone();
    $Fluid->start();



