<?php
return [
    'main' => [
        'pdoType' => 'mysql',
        'host' => 'localhost',
        'login' => 'root',
        'password' => 'root',
        'name' => 'fluid-example',
        'port' => '3306',
        'prefix' => 'ff_',
        'default' => true
    ]
];
