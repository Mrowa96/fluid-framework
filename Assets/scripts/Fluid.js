(function () {
    var Fluid = {
        info: {
            name: "Fluid Elements",
            id: 0,
            version: "0.3",
            author: "Paweł Mrowiec, mrowa94@gmail.com"
        },
        wrapper: null,
        started: false,
        elements: {},
        dataName: 'fluid',
        protectedModulesIndex: 10,
        modules: {
            0: 'Fluid',
            1: 'Library',
            2: 'Http',
            3: 'Loader',
            4: 'Browser',
            5: 'Dim',
            10: 'Modal',
            11: 'Panel',
            15: 'Form',
            16: 'List',
            17: 'FileManager',
            18: 'TableView'
        },
        Lib: {},
        Http: {},
        Loader: {},
        Browser: {},

        init: function () {
            window.addEventListener("load", function () {
                if (Fluid.started === false) {
                    Fluid.Lib = new Fluid.Library();
                    Fluid.Http = new Fluid.HttpLib();
                    Fluid.Loader = new Fluid.Loader();
                    Fluid.Browser = new Fluid.Browser();

                    Fluid.Loader.load(Fluid);
                    Fluid.wrapper = document.getElementsByTagName("body")[0];
                    Fluid.started = true;

                    Fluid.addRequiredListeners();

                    window.Fluid = Fluid;

                    console.info("Fluid started.");
                }
                else {
                    Fluid.alreadyStarted();
                }
            }, false);
        },
        alreadyStarted: function () {
            Fluid.Message(0, 0);
            return Fluid;
        },
        checkCaller: function (mod) {
            if (mod != Fluid) {
                return true;
            }
            else {
                throw Fluid.Message(0, null, mod, false);
            }
        },
        addToElements: function (element) {
            if (Fluid.Lib.is(element)) {
                if (Fluid.Lib.is(this.elements[element.info.name]) === false) {
                    this.elements[element.info.name] = [];
                }

                this.elements[element.info.name].push(element);
            }
        },
        addRequiredListeners: function(){
            implementTouchPanelOpen();

            function implementTouchPanelOpen(){
                document.addEventListener("touchstart", function(e){
                    var touches, touch;

                    e = e || window.event;
                    touches = e.changedTouches;

                    if(touches.length && Fluid.elements.Panel && Fluid.elements.Panel.length){
                        touch = touches[0];

                        Fluid.elements.Panel.forEach(function(panel){
                            // if hidden
                            if(panel.conf.touchShow === true && panel.hidden === true){
                                switch(panel.conf.position){
                                    case "left":
                                        if(0 <= touch.clientX && panel.touchArea >= touch.clientX){
                                            Fluid.Browser.panelsToOpen[panel.conf.position].push(panel);
                                        }
                                        break;
                                    case "bottom":
                                        if(Fluid.Lib.getHeight() >= touch.clientY && (Fluid.Lib.getHeight() - panel.touchArea) <= touch.clientY){
                                            Fluid.Browser.panelsToOpen[panel.conf.position].push(panel);
                                        }
                                        break;
                                }
                            }
                        });
                    }
                }, false);
                document.addEventListener("touchmove", function(e){
                    var touches, touch, differenceX, differenceY;

                    e = e || window.event;
                    touches = e.changedTouches;

                    if(touches.length && Fluid.elements.Panel && Fluid.elements.Panel.length) {
                        touch = touches[0];

                        if(Fluid.Browser.moveAttemps > 0){
                            if(Fluid.Browser.moveAttemps === 5){
                                Fluid.Browser.moveStartPos = {
                                    x: touch.clientX,
                                    y: touch.clientY
                                }
                            }

                            Fluid.Browser.moveAttemps--;
                        }
                        else{
                            if(Fluid.Browser.moveAttemps === 0){
                                differenceX = Math.abs(touch.clientX - Fluid.Browser.moveStartPos.x);
                                differenceY = Math.abs(touch.clientY - Fluid.Browser.moveStartPos.y);

                                if(Fluid.Browser.moveStartPos.x < Fluid.Lib.getWidth()/2){
                                    if(Fluid.Browser.moveStartPos.y < Fluid.Lib.getHeight()/2){
                                        if(differenceX > differenceY){
                                            Fluid.Browser.selectedPanelPosition = "left";
                                        }
                                        else{
                                            Fluid.Browser.selectedPanelPosition = "top";
                                        }
                                    }
                                    else{
                                        if(differenceX > differenceY){
                                            Fluid.Browser.selectedPanelPosition = "left";
                                        }
                                        else{
                                            Fluid.Browser.selectedPanelPosition = "bottom";
                                        }
                                    }
                                }
                                else{
                                    if(Fluid.Browser.moveStartPos.y < Fluid.Lib.getHeight()/2){
                                        if(differenceX > differenceY){
                                            Fluid.Browser.selectedPanelPosition = "right";
                                        }
                                        else{
                                            Fluid.Browser.selectedPanelPosition = "top";
                                        }
                                    }
                                    else{
                                        if(differenceX > differenceY){
                                            Fluid.Browser.selectedPanelPosition = "right";
                                        }
                                        else{
                                            Fluid.Browser.selectedPanelPosition = "bottom";
                                        }
                                    }
                                }

                                Fluid.Browser.moveAttemps = null;
                            }
                            else if(Fluid.Browser.moveAttemps === null){
                                if(Fluid.Browser.panelsToOpen[Fluid.Browser.selectedPanelPosition].length){
                                    Fluid.Browser.panelsToOpen[Fluid.Browser.selectedPanelPosition][0].show();
                                }
                            }
                        }

                    }
                }, false);

                document.addEventListener("touchend",onCancel, false);
                document.addEventListener("touchcancel",onCancel, false);

                function onCancel(){
                    Fluid.Browser.refreshMoveAttemps();
                    Fluid.Browser.selectedPanelPosition = null;
                }
            }
        }
    };

    Fluid.Loader = function () {
        this.name = null;
        this.module = null;
        this.temporary = {};
    };
    Fluid.Loader.prototype.load = function (module) {
        var domElements, _this = this;

        if (Fluid.Lib.is([module, module.dataName])) {
            this.name = module.dataName;
            this.module = module;
            this.wrapper = module.wrapper;

            domElements = Fluid.Lib.toArray(document.getElementsByTagName("*"));

            domElements.forEach(function (elm) {
                if (elm.dataset[_this.name]) {
                    _this.temporary.element = elm;
                    Fluid.Loader.configureElement();
                    Fluid.Loader.initializeElement();
                }
            });
        }
    };
    Fluid.Loader.prototype.configureElement = function () {
        var confObj = {},
            conf,
            property,
            indexColon = 0,
            lastIndexColon = 0,
            indexComma = 0,
            indexApostophe = 0,
            nextApostophe = 0;

        if (Fluid.started === false) {
            if (Fluid.Lib.is(this.temporary.element)) {
                conf = this.temporary.element.dataset[this.name];

                while (conf.indexOf(":", indexColon) !== -1 && indexColon !== -1) {
                    indexColon = conf.indexOf(":", indexColon);
                    property = conf.substr(indexComma, indexColon - indexComma).trim();
                    indexComma = conf.indexOf(",", indexColon);
                    indexApostophe = conf.indexOf("'", indexColon);
                    indexColon++;

                    if (indexApostophe < indexComma && indexApostophe !== -1) {
                        nextApostophe = conf.indexOf("'", indexApostophe + 1);
                        indexComma = conf.indexOf(",", nextApostophe);
                        indexColon = conf.indexOf(":", nextApostophe);
                        value = conf.substr(indexApostophe + 1, Math.abs(nextApostophe - indexApostophe - 1)).trim();
                    }
                    else {
                        value = conf.substr(indexColon, Math.abs(indexComma - indexColon)).trim();
                        if (value[0] === "'") {
                            value = value.substr(1);
                        }

                        if (value[value.length - 1] === "'") {
                            value = value.substr(0, value.length - 1);
                        }
                    }

                    confObj[property] = Fluid.Lib.strBoolToBool(value);

                    lastIndexColon = indexColon;
                    indexComma++;
                }

                this.temporary.configuration = confObj;
                this.temporary.element.removeAttribute("data-" + this.name);
            }
        }
        else {
            Fluid.alreadyStarted();
        }
    };
    Fluid.Loader.prototype.initializeElement = function () {
        var temp = this.temporary,
            modules, mod;

        if (Fluid.Lib.is(temp.element, temp.configuration)) {
            temp.configuration.createdBy = "loader";

            if (Fluid.Lib.is(temp.configuration.element)) {
                mod = temp.configuration.element;
            }
            else {
                Fluid.Message(0, 1);
                return;
            }

            if (Fluid.Lib.is(mod)) {
                mod = mod[0].toUpperCase() + mod.substr(1);
                modules = Fluid.Lib.toArray(this.module['modules']);

                if (modules.indexOf(mod) !== -1 && modules.indexOf(mod) < Fluid.protectedModulesIndex) {
                    new this.module[mod](temp.configuration, temp.element);
                }
            }
        }
    };
    Fluid.Loader.prototype.mergeConfiguration = function (element) {
        var conf = {},
            dConf = {},
            newConf = {};

        if (Fluid.Lib.is(element, element.defaultConf, element.conf)) {
            conf = element.conf;
            dConf = element.defaultConf;

            for (prop in dConf) {
                if (dConf.hasOwnProperty(prop)) {
                    if (conf[prop] !== undefined) {
                        newConf[prop] = conf[prop];
                    }
                    else {
                        newConf[prop] = dConf[prop];
                    }
                }
            }

            for (prop in conf) {
                if (conf.hasOwnProperty(prop)) {
                    if (newConf[prop] === undefined) {
                        newConf[prop] = conf[prop];
                    }
                }
            }

            element.conf = newConf;
        }
    };

    Fluid.Browser = function () {
        this.info = {
            name: "Browser",
            id: 4
        };
        this.dim = null;
        this.selectedPanel = null;
        this.selectedPanelPosition = null;
        this.moveAttemps = 5;
        this.moveStartPos = {};
        this.panelsToOpen = {
            left: [],
            right: [],
            top: [],
            bottom: []
        };

        Fluid.checkCaller(this);
    };
    Fluid.Browser.prototype.refreshMoveAttemps = function(){
        this.moveAttemps = 5;
    }
    Fluid.Browser.prototype.handleOffEvents = function (offConf) {
        /*
         *    offConf = {
         *        element: Node,
         *        notCLicked: function(element){},
         *        excludedElements: [Node, Node, ...]
         *    }
         */

        if (Fluid.Lib.is(offConf)) {
            document.addEventListener("click", function (e) {
                var evt = e || window.event,
                    element = offConf.element,
                    excludedElements = offConf.excludedElements || [],
                    callBack = offConf.notClicked,
                    clicked = evt.target,
                    parent = clicked;

                while (parent !== element) {
                    if (excludedElements.length && excludedElements.indexOf(parent) !== -1) {
                        break;
                    }
                    else if (parent !== document && parent) {
                        parent = parent.parentNode;
                    }
                    else {
                        callBack(element);
                        break;
                    }
                }
            }, false);
        }
    };
    Fluid.Browser.prototype.hidePanels = function(){
        var iteration = 0;

        Fluid.elements.Panel.forEach(function(panel){
            if(panel.hidden !== true){
                panel.hide(true);
                iteration++;
            }
        });

        return iteration > 0;
    };

    Fluid.Dim = function (conf) {
        this.info = {
            name: "Dim",
            id: 5
        };
        this.defaultConf = {
            background: "#000",
            opacityWhenShowed: 0.6,
            duration: ".3s",
            delay: "0s",
            effect: "ease",
            wrapper : (Fluid.wrapper !== null) ? Fluid.wrapper : document.getElementsByClassName("wrapper")[0]
        };
        this.conf = {};
        this.element = null;

        Fluid.checkCaller(this);

        if(Fluid.Lib.is(conf)){
            this.conf = conf;
        }

        if(Fluid.Lib.is(Fluid.elements.Dim) === false || Fluid.elements.Dim.length === 0){
            Fluid.addToElements(this);
            Fluid.Loader.mergeConfiguration(this);

            this.createElement();

            return this;
        }
        else if(Fluid.Lib.is(Fluid.elements.Dim[0])){
            var dimObject = Fluid.elements.Dim[0];

            dimObject.conf = this.conf;
            Fluid.Loader.mergeConfiguration(dimObject);

            if(dimObject.element === null){
                dimObject.createElement();
            }

            return dimObject;
        }
        else{
            //throw error
        }
    };
    Fluid.Dim.prototype.hide = function(){
        this.element.style.opacity = 0;

        this.element.addEventListener("transitionend", this.onHide, false);
    };
    Fluid.Dim.prototype.show = function(){
        this.element.style.zIndex = 100;
        this.element.style.opacity = this.conf.opacityWhenShowed;

        this.element.removeEventListener("transitionend", this.onHide, false);
    };
    Fluid.Dim.prototype.onHide = function(){
        this.style.zIndex = -1;
    };
    Fluid.Dim.prototype.createElement = function(){
        this.element = document.createElement("div");
        this.element.classList.add("dim");
        this.conf.wrapper.appendChild(this.element);
        this.element.style.background = this.conf.background;
        this.element.style.transition = "opacity " + this.conf.duration + " " + this.conf.effect + " " + this.conf.delay;;

        Fluid.Browser.dim = this;
    };

    Fluid.Modal = function (conf, elm) {
        /** Configuration:
         * show - true/false/Ns,
         * hide - true/false/Ns,
         * close - true/false/Ns,
         * showOnce - Ns,
         * header - string/HTMLElement/false,
         * content - string/HTMLElement/false,
         * contentAjax - string/false,
         * footer - string/HTMLElement/false,
         * create - true/false,
         * actions - hide/close/minimize/maximize/
         * view - standard/float
         * animations - true/false,
         * type - dialog/alert/prompt
         **/

        this.info = {
            name: "Modal",
            id: 10
        };
        this.defaultConf = {
            show: true,
            hide: false,
            close: false,
            showOnce: false,
            header: "Example title",
            content: false,
            contentAjax: false,
            footer: false,
            create: false,
            actions: {
                events: "minimize hide",
                parent: "header"
            },
            view: "standard",
            animation: false,
            type: "modal",
            promptSendButtonText: "Wyślij",
            onPromptSubmit: function (value) {
            }
        };
        this.conf = {};
        this.element = null;
        this.hidden = false;
        this.initialized = false;
        this.blocks = [];
        this.actions = [];

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        if (Fluid.Lib.is(conf)) {
            this.conf = conf;
            Fluid.Loader.mergeConfiguration(this);

            if (Fluid.Lib.is(elm)) {
                this.element = elm;
            }
            else {
                this.create();
            }

            this.init();
            this.addListeners();

            return this;
        }
        else {
            Fluid.Message(10, 0);
            return Fluid;
        }
    };
    Fluid.Modal.prototype.init = function () {
        var _this = this,
            content,
            tempActions;

        if (this.initialized === false) {
            this.hide(true);
            this.element.classList.contains("modal") === false ? this.element.classList.add("modal") : null;

            if (this.conf.createdBy === "loader") {
                content = this.element.innerHTML;

                if (content.length) {
                    this.conf.content = content;
                    this.element.innerHTML = "";
                }

                if (typeof(this.conf.actions) === "string") {
                    tempActions = this.conf.actions;
                    this.conf.actions = {
                        events: tempActions
                    };
                }

                if (this.conf.actionsPosition) {
                    this.conf.actions.position = this.conf.actionsPosition;
                }

                if (this.conf.actionsParent) {
                    this.conf.actions.parent = this.conf.actionsParent;
                }

                initHeader();
                initContent();
            }

            initActions();
            initBlocks();
            initVisibility();

            this.initialized = true;
        }
        else {
            Fluid.Message(10, 5);
        }

        function initHeader() {
            if (_this.conf.header) {
                _this.create("header");
            }
        }

        function initActions() {
            var events, positions, parent;

            if (_this.conf.actions) {
                if (_this.conf.actions.events) {
                    if (typeof(_this.conf.actions.events) === "string") {
                        events = _this.conf.actions.events.split(" ");
                        _this.conf.actions.events = [];
                        events.forEach(function (event) {
                            _this.conf.actions.events.push(event.trim());
                        });
                    }
                    else {
                        //error
                    }
                }
                else {
                    _this.conf.actions.events = _this.defaultConf.actions.events;
                    return initActions();
                }

                /*if(_this.conf.actions.position){
                 if(typeof(_this.conf.actions.position) === "string"){
                 positions = _this.conf.actions.position.trim().split(" ");
                 _this.conf.actions.position = {};

                 if(positions.length === 2){
                 _this.conf.actions.position.x = positions[0];
                 _this.conf.actions.position.y = positions[1];
                 }
                 else if(positions.length === 1 && positions[0] === "center"){
                 _this.conf.actions.position.x = positions[0];
                 _this.conf.actions.position.y = positions[0];
                 }
                 else{
                 //return error?
                 }
                 }
                 else{
                 //error!
                 }

                 }
                 else{
                 _this.conf.actions.position = _this.defaultConf.actions.position;
                 return initActions();
                 }*/

                if (_this.conf.actions.parent && typeof(_this.conf.actions.parent) === "string") {
                    parent = _this.conf.actions.parent.trim();

                    if (parent.length && _this.blocks.length) {
                        _this.conf.actions.parent = null;

                        _this.blocks.forEach(function (block) {
                            switch (parent[0]) {
                                case ".":
                                    parent = parent.substr(1);
                                    _this.conf.actions.parent = block.getElementsByClassName(parent);
                                    return;

                                case "#":
                                    _this.conf.actions.parent = block.getElementById(parent);
                                    return;
                            }
                        });

                        if (_this.conf.actions.parent === null) {
                            _this.blocks.forEach(function (block) {
                                if (block.blockType === parent) {
                                    _this.conf.actions.parent = block;
                                    return;
                                }
                            });
                        }

                        if (_this.conf.actions.parent === null) {
                            _this.blocks.forEach(function (block) {
                                if (block.blockType === "header") {
                                    _this.conf.actions.parent = block;
                                    return;
                                }
                            });
                        }

                        if (_this.conf.actions.parent === null) {
                            _this.conf.actions.parent = _this.element;
                        }
                    }
                    else {
                        _this.conf.actions.parent = _this.element;
                    }
                }
                else {
                    _this.conf.actions.parent = _this.defaultConf.actions.parent;
                    return initActions();
                }

                _this.create("actions");
            }
        }

        function initContent() {
            if (_this.conf.contentAjax) {
                if (_this.conf.content === false) {

                }
                else {
                    Lib.Message(10, 4);
                }
            }
            else if (_this.conf.content) {
                _this.create("content")
            }
        }

        function initBlocks() {
            if (_this.blocks.length) {
                _this.blocks.forEach(function (block) {
                    _this.element.appendChild(block);
                });
            }
        }

        function initVisibility() {
            if (_this.conf.show) {
                if (_this.conf.hide === false) {
                    _this.show();
                }
            }
            else {
                if (_this.conf.hide) {
                    Fluid.Message(10, 2);
                }
            }
        }
    };
    Fluid.Modal.prototype.create = function (toCreate) {
        var header, title, content, actions, action, wrap,
            prompt, promptInput, promptSendButton,
            _this = this;

        if (Fluid.Lib.is(toCreate) === false) {
            toCreate = ['all'];
        }
        else if (typeof(toCreate) === "string") {
            toCreate = [toCreate];
        }

        toCreate.forEach(function (val) {
            switch (val) {
                case "header":
                    if (_this.conf.header instanceof HTMLElement) {
                        header = _this.conf.header;

                        header.classList.add("modal-header");
                        header.blockType = "header";

                        _this.element.appendChild(header);
                    }
                    else if (typeof(_this.conf.header) === "string") {
                        header = document.createElement("header");
                        title = document.createElement("span");

                        header.classList.add("modal-header");
                        header.blockType = "header";
                        title.classList.add("modal-title");
                        title.innerHTML = _this.conf.header;

                        header.appendChild(title);
                        _this.blocks.push(header);
                    }
                    break;
                case "content":
                    if (_this.conf.content instanceof HTMLElement) {

                    }
                    else if (typeof(_this.conf.content) === "string") {
                        content = document.createElement("section");

                        content.classList.add("modal-content");
                        content.blockType = "content";
                        content.innerHTML = _this.conf.content;

                        _this.blocks.push(content);

                        switch (_this.conf.type) {
                            case "prompt":
                                promptInput = document.createElement("input");
                                promptSendButton = document.createElement("button");

                                promptInput.setAttribute("name", "promptValue");
                                promptInput.classList.add("promptValue");
                                promptSendButton.classList.add("promptSendButton");

                                _this.promptInput = promptInput;
                                _this.promptSendButton = promptSendButton;
                                _this.promptSendButton.innerHTML = _this.conf.promptSendButtonText;

                                _this.promptSendButton.addEventListener("click", function () {
                                    _this.conf.onPromptSubmit(_this.promptInput.value);
                                });

                                content.appendChild(promptInput);
                                content.appendChild(promptSendButton);
                                break;
                            case "modal": //rename to default?

                                break;
                        }
                    }
                    else if (_this.conf.content === false) {
                        _this.element.innerHTML = "";
                    }
                    break;
                case "actions":
                    if (_this.conf.actions) {
                        actions = document.createElement("div");
                        actions.classList.add("modal-actions");

                        if (_this.conf.actions.events) {
                            _this.conf.actions.events.forEach(function (event) {
                                action = document.createElement("span");
                                action.classList.add("action", event);
                                action.actionType = event;
                                _this.actions.push(action);
                                actions.appendChild(action);
                            })
                        }

                        if (_this.conf.actions.parent) {
                            _this.conf.actions.parent.appendChild(actions);
                        }
                    }

                    break;
                case "all":
                    wrap = document.createElement("div");

                    _this.element = wrap;
                    _this.create(["header", "content"]);
                    Fluid.wrapper.appendChild(_this.element);
                    break;
            }
        });
    };
    Fluid.Modal.prototype.addListeners = function () {
        var _this = this;

        if (this.actions.length) {
            this.actions.forEach(function (action) {
                switch (action.actionType) {
                    case "close":
                        action.addEventListener("click", function () {
                            _this.close(true);
                        });
                        break;
                }
            });
        }

        /*Fluid.Browser.handleOffEvents({
         element: this.element,
         notClicked: function(element){
         console.log(_this)
         _this.hide(true);
         }
         });*/
    };
    Fluid.Modal.prototype.show = function () {
        if (this.conf.animation) {

        }
        else {
            if (this.hidden) {
                this.element.style.zIndex = "100000";
                this.element.style.visibility = "visible";
                this.hidden = false;
            }
            else {
                Fluid.Message(10, 3);
            }
        }
    };
    Fluid.Modal.prototype.hide = function (hideFast) {
        var hideFast = hideFast || false;

        if (hideFast) {
            this.element.style.zIndex = "-1";
            this.element.style.visibility = "hidden";
            this.hidden = true;
        }
        else {
            if (this.conf.animation) {

            }
            else {

            }
        }
    };
    Fluid.Modal.prototype.close = function (hideFast) {
        this.hide(hideFast);
        this.element.parentNode.removeChild(this.element);
    };

    Fluid.Form = function (conf, elm) {
        this.info = {
            name: "Form",
            id: 15
        };
        this.defaultConf = {
            ajax: true,
            submitButton: null,
            create: false,
            attributes: {}
        };
        this.conf = {};
        this.element = null;
        this.elements = {};
        this.radios = [];
        this.filesPreview = {};
        this.dataToSend = null;
        this.submitButton = null;
        this.filesList = null;
        this.extraSendData = {};
        this.initialized = false;
        this.validateType = 1;
        this.formName = null;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        if (Fluid.Lib.is(conf)) {
            this.conf = conf;

            Fluid.Loader.mergeConfiguration(this);

            if (Fluid.Lib.is(elm)) {
                this.element = elm;

                this.init();
                this.addListeners("submitButton");
            }
            else if(this.conf.create === true){
                this.create("form");
            }

            return this;
        }
        else {
            Fluid.Message(15, 0);
            return Fluid;
        }
    };
    Fluid.Form.prototype.init = function () {
        var _this = this;

        if (!this.initialized) {
            loadElements();
            this.loadParameters();

            if (Fluid.Lib.is(this.action) && Fluid.Lib.is(this.formName)) {
                if (!Fluid.Lib.is(this.conf.submitButton)) {
                    getSubmitButton();
                }
                else {
                    this.submitButton = document.querySelector(this.conf.submitButton);
                }

                this.initialized = true;
            }
            else {
                throw "error action formName - to do";
                //to do!
            }
        }

        function loadElements() {
            var inputs,
                textareas,
                selects,
                type,
                name;

            inputs = _this.element.querySelectorAll("input");
            textareas = _this.element.querySelectorAll("textarea");
            //selects = _this.element.getElementsByTagName("select");

            if (inputs.length > 0) {
                inputs = Fluid.Lib.toArray(inputs);

                inputs.forEach(function (input) {
                    type = input.getAttribute("type");
                    name = input.getAttribute("name");

                    if (type !== "hidden" && type !== "disabled") {
                        if (type === "submit") {
                            _this.submitButton = input;
                        }
                        else if (type === "checkbox") {
                            handleCheckbox(input);
                            _this.elements[name] = input;
                        }
                        else if (type === "radio") {
                            handleRadio(input);
                            _this.elements[name] = {type: "radio", value: null};
                        }
                        else if (type === "file") {
                            _this.addListeners("file", input)
                            _this.elements[name] = input;
                        }
                        else {
                            _this.elements[name] = input;
                        }
                    }
                });
            }

            if (textareas.length > 0) {
                textareas = Fluid.Lib.toArray(textareas);

                textareas.forEach(function (textarea) {
                    _this.elements[textarea.getAttribute("name")] = textarea;
                })
            }
        }

        function handleRadio(input) {
            var name = input.getAttribute("name"),
                control = input.parentNode.getElementsByClassName("radio-control"),
                radio = null;

            control = Fluid.Lib.toArray(control);
            control.forEach(function (element) {
                if (element.dataset.name === name) {
                    radio = element;
                    radio.dataset.checked = "false";
                    radio.input = input;
                    _this.radios.push(radio);
                }
            });

            if (Fluid.Lib.is(radio)) {
                _this.addListeners("radio", radio);
            }
        }

        function handleCheckbox(input) {
            var name = input.getAttribute("name"),
                control = input.parentNode.getElementsByClassName("checkbox-control"),
                checkbox = null;

            control = Fluid.Lib.toArray(control);
            control.forEach(function (element) {
                if (element.dataset.name === name) {
                    checkbox = element;
                    checkbox.dataset.checked = "false";
                    checkbox.input = input;
                    checkbox.input.value = "0";
                    delete(checkbox.dataset.name);

                    input.checkbox = checkbox;
                }
            });

            if (Fluid.Lib.is(checkbox)) {
                _this.addListeners("checkbox", checkbox);
            }
            else {
                checkbox = document.createElement("div");
                checkbox.classList.add("checkbox-control");
                checkbox.dataset.checked = "false";
                checkbox.input = input;
                checkbox.input.value = "0";

                input.checkbox = checkbox;
                input.parentNode.appendChild(checkbox);

                _this.addListeners("checkbox", checkbox);
            }
        }

        function getSubmitButton() {
            var button;

            if (!Fluid.Lib.is(_this.submitButton)) {
                button = _this.element.getElementsByTagName("button");

                if (button.length) {
                    button = button[0];
                    _this.submitButton = button;
                }
                else {
                    Fluid.Message(15, 1);
                }
            }
        }
    };
    Fluid.Form.prototype.loadParameters = function() {
        var action, method, formName;

        action = this.element.getAttribute("action");
        method = this.element.getAttribute("method");
        formName = this.element.getAttribute("id");

        this.action = (Fluid.Lib.is(action)) ? action : null;
        this.method = (Fluid.Lib.is(method)) ? method : "POST";
        this.formName = (Fluid.Lib.is(formName)) ? formName : null;
    }
    Fluid.Form.prototype.create = function(whatToCreate, attributes){
        var createdElement = null,
            extraElement = null,
            _this = this;

        attributes = attributes || {};

        createElement(whatToCreate);
        setAttributes();

        if(this.element !== createdElement){
            this.element.appendChild(createdElement);
        }

        return createdElement;

        function createElement(whatToCreate){
            switch(whatToCreate) {
                case "form":
                    createdElement = document.createElement(whatToCreate);
                    attributes = _this.conf.attributes;

                    _this.element = createdElement;
                    break;

                case "textInput":
                    createdElement = document.createElement("input");
                    addToAttributes("type", "text");
                    break;

                case "fileInput":
                    createdElement = document.createElement("input");
                    extraElement = document.createElement("i");

                    extraElement.classList.add("fa", "fa-upload", "fileControl");

                    _this.element.appendChild(extraElement);

                    addToAttributes("type", "file");
                    addToAttributes("multiple", "multiple");
                    break;
            }

            _this.loadParameters();
        }

        function addToAttributes(property, value){
            if(typeof(attributes) === "object"){
                attributes[property] = value;
            }
        }

        function setAttributes(){
            for(var attribute in attributes){
                if(attributes.hasOwnProperty(attribute)){
                    createdElement.setAttribute(attribute, attributes[attribute]);
                }
            }
        }
    };
    Fluid.Form.prototype.addListeners = function (type, element) {
        var _this = this;

        switch (type) {
            case "submitButton":
                if (Fluid.Lib.is(this.submitButton)) {
                    this.submitButton.addEventListener("click", function (e) {
                        if (_this.conf.ajax) {
                            e = e || window.event;
                            _this.prepareData();
                            _this.sendData();
                            e.preventDefault();
                        }
                    }, false);
                }

                break;

            case "checkbox":
                element.addEventListener("click", function () {
                    if (this.dataset.checked === "false") {
                        this.input.value = "1";
                        this.dataset.checked = "true";
                    }
                    else if (this.dataset.checked === "true") {
                        this.input.value = "0";
                        this.dataset.checked = "false";
                    }
                }, false);

                break;

            case "radio":
                element.addEventListener("click", function () {
                    var name = this.dataset.name;

                    _this.elements[name].value = this.input.value;

                    _this.radios.forEach(function (radio) {
                        radio.dataset.checked = "false";
                    });

                    this.dataset.checked = "true";

                }, false);

                break;

            case "file":
                if (Fluid.Lib.is(element)) {
                    element.addEventListener("change", function () {
                        var files, fileData, inputName, filePreviewObj;

                        files = Fluid.Lib.toArray(this.files);
                        files.forEach(function (file) {
                            fileData = new FormData();
                            inputName = element.getAttribute("name");

                            fileData.append("file", file);
                            fileData.append("fileInput", inputName);
                            fileData.append("formName", _this.formName);
                            fileData.append("extraData", _this.extraSendData);

                            filePreviewObj = new Fluid.Form.FilePreview(_this, element, file);

                            _this.fileToSend = fileData;
                            _this.sendFile(filePreviewObj);
                        });
                    }, false);
                }
                break;
        }
    };
    Fluid.Form.prototype.prepareData = function () {
        var normalData = {}, type, formData;

        formData = new FormData();

        for (var index in this.elements) {
            if (this.elements.hasOwnProperty(index)) {
                if (!this.elements[index].hasOwnProperty("type")) {
                    type = this.elements[index].getAttribute("type");
                }

                if (type && type === "file") {
                    normalData[index] = null;
                }
                else {
                    normalData[index] = this.elements[index].value;
                }
            }
        }

        formData.append("formName", this.formName);
        formData.append("extraData", this.extraSendData);
        formData.append("data", JSON.stringify(normalData));

        this.dataToSend = formData;
    };
    Fluid.Form.prototype.sendData = function () {
        var _this = this;

        Fluid.Http.post({
            url: this.action, toSend: this.dataToSend, onResponse: function (response) {
                _this.processData(response);
            }
        });
    };
    Fluid.Form.prototype.sendFile = function (filePreviewObj) {
        var completed = 0;

        Fluid.Http.post({
            url: this.action, toSend: this.fileToSend, onProgress: function (e) {
                if (e.lengthComputable) {
                    completed = (e.loaded / e.total) * 100;
                    filePreviewObj.progress.style.width = completed + "%";
                }
            }
        });
    };
    Fluid.Form.prototype.processData = function (response) {
        var response = JSON.parse(response),
            processedElement,
            errorBlock,
            _this = this;

        if (response.status === 0) {
            clearErrors();

            response = Fluid.Lib.toArray(response);

            switch (this.validateType) {
                case 1:
                    if (response.length) {
                        response.forEach(function (element) {
                            if (Fluid.Lib.is(_this.elements[element.elementName])) {
                                processedElement = _this.elements[element.elementName];

                                if (processedElement.type === "radio") {
                                    _this.radios.forEach(function (radio) {
                                        radio.classList.add("error");
                                    });
                                }
                                else if (processedElement.getAttribute("type") === "checkbox") {
                                    processedElement.checkbox.classList.add("error");
                                }
                                else {
                                    processedElement.classList.add("error");
                                    processedElement.setAttribute("placeholder", element.message);
                                }
                            }

                            if (element.elementName === "form") {
                                errorBlock = document.createElement("div");
                                errorBlock.classList.add("errorBlock");
                                errorBlock.innerHTML = element.message;

                                _this.element.appendChild(errorBlock);
                            }
                        });
                    }

                    break;
            }
        }
        else if (response.status === 1) {
            if (response.backUrl) {
                window.location.href = response.backUrl;
            }
        }

        function clearErrors() {
            var processedElement, errorBlock;

            errorBlock = _this.element.getElementsByClassName("errorBlock");

            switch (_this.validateType) {
                case 1:
                    for (index in _this.elements) {
                        processedElement = _this.elements[index];

                        if (processedElement.type === "radio") {
                            _this.radios.forEach(function (radio) {
                                radio.classList.remove("error");
                            });
                        }
                        else if (processedElement.getAttribute("type") === "checkbox") {
                            processedElement.checkbox.classList.remove("error");
                        }
                        else {
                            if (processedElement.classList.contains("error")) {
                                processedElement.classList.remove("error");
                            }

                            processedElement.removeAttribute("placeholder");
                        }
                    }

                    if (errorBlock.length) {
                        errorBlock = Fluid.Lib.toArray(errorBlock);

                        errorBlock.forEach(function (element) {
                            element.parentNode.removeChild(element);
                        })
                    }

                    break;
            }
        }
    };
    Fluid.Form.FilePreview = function (form, input, file) {
        var name = input.getAttribute("name"),
            previewBlock,
            progressBar,
            inputFilePreview;

        this.element = null;
        this.progress = null;

        if (!form.filesPreview[name]) {
            form.filesPreview[name] = [];
        }

        inputFilePreview = input.parentNode.parentNode.parentNode.getElementsByClassName("filesPreview");

        if (inputFilePreview.length) {
            inputFilePreview = inputFilePreview[0];

            previewBlock = document.createElement("div");
            progressBar = document.createElement("div");

            previewBlock.classList.add("previewBlock");
            progressBar.classList.add("progressBar");

            previewBlock.appendChild(progressBar);
            inputFilePreview.appendChild(previewBlock);
            7

            this.element = previewBlock;
            this.progress = progressBar;

            return this;
        }
    };

    Fluid.FileManager = function (conf, elm) {
        this.info = {
            name: "FileManager",
            id: 17
        };
        this.defaultConf = {
            path: '/upload',
            contentHeight: null
        };
        this.conf = {};
        this.element = null;
        this.interface = false;
        this.content = null;
        this.breadcrums = null;
        this.lists = {
            folderContext: null,
            fileContext: null
        };
        this.elements = [];
        this.paths = [];
        this.dataToSend = null;
        this.uploadForm = null;
        this.url = "/manage/files/content";
        this.tempUrl = this.url;
        this.chosenData = [];
        this.pasteButton = null;
        this.operationsButton = false;
        this.selected = [];

        this.changeNameModal = null;
        this.newFolderModal = null;

        this.initialized = false;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        if (Fluid.Lib.is(conf)) {
            this.conf = conf;

            Fluid.Loader.mergeConfiguration(this);

            if (Fluid.Lib.is(elm)) {
                this.element = elm;
            }

            this.init();

            return this;
        }
        else {
            Fluid.Message(17, 0);
            return Fluid;
        }
    };
    Fluid.FileManager.prototype.init = function () {
        var _this = this;

        if (!this.initialized) {
            this.setPath();

            if (this.conf.createdBy === "loader") {
                this.createInterface();
                this.addListeners("shortcuts");
                this.addListeners("managerWindow");
                this.updateContent();
            }
            else {
                if (Fluid.Lib.is(this.conf.element)) {
                    this.createInterface();
                    this.addListeners("shortcuts");
                    this.addListeners("managerWindow");
                    this.updateContent();
                }
                else {
                    Fluid.Message(17, 1);
                }
            }

            this.element.classList.add("fileManager");
            this.initialized = true;
        }
    };
    Fluid.FileManager.prototype.createInterface = function (elementToCreate) {
        var _this = this,
            headerBlock,
            columnsBlock,
            contentBlock,
            wrapBlock;

        if (this.interface === false) {
            wrapBlock = document.createElement("section");
            headerBlock = createHeader();
            columnsBlock = createColumns();
            contentBlock = createContent();

            wrapBlock.classList.add("wrapBlock");

            this.element.appendChild(wrapBlock);
            wrapBlock.appendChild(headerBlock);
            //wrapBlock.appendChild(columnsBlock);
            wrapBlock.appendChild(contentBlock);

            this.addListeners("contentResize", contentBlock);

            this.interface = true;
        }
        else if (Fluid.Lib.is(elementToCreate)) {
            switch (elementToCreate) {
                case "copyButton":
                    createPasteButton("copy");
                    break;
                case "moveButton":
                    createPasteButton("move");
                    break;
                case "emptyView":
                    createEmptyView();
                    break;
            }
        }

        function createHeader() {
            var prevButton,
                nextButton,
                homeButton,
                uploadButton,
                uploadForm,
                addFolderButton,
                settingsButton,
                breadcrumsElm,
                breadcrumsBlock,
                buttonsBlock,
                navigationBlock,
                clearBlock;

            headerBlock = document.createElement("header");
            prevButton = document.createElement("a");
            nextButton = document.createElement("a");
            homeButton = document.createElement("a");
            addFolderButton = document.createElement("a");
            settingsButton = document.createElement("a");
            breadcrumsElm = document.createElement("span");

            uploadForm = new Fluid.Form({
                create: true,
                attributes: {
                    action: "/manage/files/add",
                    method: "POST",
                    class: "uploadForm"
                }
            });
            uploadButton = uploadForm.create("fileInput");

            navigationBlock = document.createElement("div");
            buttonsBlock = document.createElement("div");
            breadcrumsBlock = document.createElement("div");
            clearBlock = document.createElement("div");

            breadcrumsBlock.classList.add("breadcrumsBlock");
            navigationBlock.classList.add("navigationBlock");
            buttonsBlock.classList.add("buttonsBlock");
            clearBlock.classList.add("clearFloat");

            prevButton.classList.add("prevButton");
            nextButton.classList.add("nextButton", "blocked");
            homeButton.classList.add("homeButton");
            uploadButton.classList.add("uploadButton");
            addFolderButton.classList.add("addFolderButton");
            settingsButton.classList.add("settingsButton");
            breadcrumsElm.classList.add("breadcrumsElm");

            nextButton.blocked = true;

            prevButton.setAttribute("href", "#");
            nextButton.setAttribute("href", "#");
            homeButton.setAttribute("href", "#");
            //uploadButton.setAttribute("href", "#");
            addFolderButton.setAttribute("href", "#");
            settingsButton.setAttribute("href", "#");

            prevButton.innerHTML = '<i class="icon fa fa-angle-left"></i>';
            nextButton.innerHTML = '<i class="icon fa fa-angle-right"></i>';
            homeButton.innerHTML = '<i class="icon fa fa-home"></i>';
            //uploadButton.innerHTML = '<i class="icon fa fa-upload"></i>';
            addFolderButton.innerHTML = '<i class="icon fa fa-folder"></i>';
            settingsButton.innerHTML = '<i class="icon fa fa-cog"></i>';

            _this.addListeners("prevButton", prevButton);
            _this.addListeners("nextButton", nextButton);
            _this.addListeners("homeButton", homeButton);
            //_this.addListeners("uploadButton", uploadButton);
            _this.addListeners("addFolderButton", addFolderButton);
            _this.addListeners("settingsButton", settingsButton);

            navigationBlock.appendChild(prevButton);
            //navigationBlock.appendChild(nextButton);
            navigationBlock.appendChild(homeButton);

            breadcrumsBlock.appendChild(breadcrumsElm);

            //buttonsBlock.appendChild(settingsButton);
            buttonsBlock.appendChild(addFolderButton);
            buttonsBlock.appendChild(uploadForm.element);

            headerBlock.appendChild(navigationBlock);
            headerBlock.appendChild(breadcrumsBlock);
            headerBlock.appendChild(buttonsBlock);
            headerBlock.appendChild(clearBlock);

            headerBlock.classList.add("header");
            _this.breadcrums = breadcrumsElm;
            _this.buttonsBlock = buttonsBlock;
            _this.uploadForm = uploadForm;
            _this.addListeners("fileUpload", uploadButton);

            return headerBlock;
        }

        function createColumns() {
            columnsBlock = document.createElement("div");

            columnsBlock.classList.add("columns");

            return columnsBlock;
        }

        function createContent() {
            _this.content = document.createElement("div");
            _this.content.classList.add("content");

            if (!Fluid.Lib.is(_this.conf.contentHeight)) {
                _this.setContentHeight();
            }
            else {
                _this.content.style.height = _this.conf.contentHeight;
            }

            return _this.content;
        }

        function createPasteButton(type) {
            var pasteButton;

            if (Fluid.Lib.is(_this.pasteButton)) {
                _this.pasteButton.parentNode.removeChild(_this.pasteButton);
                _this.pasteButton = null;
            }

            pasteButton = document.createElement("a");
            pasteButton.setAttribute("href", "#");
            pasteButton.classList.add("pasteButton");
            pasteButton.innerHTML = "<i class='icon fa fa-clipboard'></i>";
            pasteButton.type = type;

            _this.addListeners("pasteButton", pasteButton)

            _this.buttonsBlock.appendChild(pasteButton);
            _this.pasteButton = pasteButton;
        }

        function createEmptyView() {
            var emptyWrap, emptyIcon, emptyText;

            emptyWrap = document.createElement("div");
            emptyIcon = document.createElement("i");
            emptyText = document.createElement("span");

            emptyWrap.classList.add("emptyContent");
            emptyIcon.classList.add("fa", "fa-folder-o", "emptyIcon");
            emptyText.classList.add("emptyText");

            emptyText.innerHTML = "Katalog jest pusty";

            emptyWrap.appendChild(emptyIcon);
            emptyWrap.appendChild(emptyText);

            _this.content.appendChild(emptyWrap);
        }
    };
    Fluid.FileManager.prototype.updateContent = function () {
        var _this = this,
            formData = new FormData(),
            url = null,
            toSend = {};

        toSend.path = this.getLastPath();

        if (Fluid.Lib.is(this.dataToSend)) {
            toSend.data = this.dataToSend;
        }

        if (Fluid.Lib.is(this.tempUrl)) {
            url = this.tempUrl;
            this.tempUrl = this.url;
        }

        formData.append("data", JSON.stringify(toSend));

        Fluid.Http.post({
            url: url,
            toSend: formData,
            onResponse: function (response) {
                var data = JSON.parse(response);

                _this.clearContent();
                _this.uploadForm.extraSendData = JSON.stringify({
                    dir: _this.getLastPath()
                });

                if (data.length) {
                    data.forEach(function (elm) {
                        _this.createElement(elm);
                        _this.addListeners("element", elm)
                    });
                }
                else {
                    _this.createInterface("emptyView");
                }
            },
            onProgress: function (e) {
            }
        });
    };
    Fluid.FileManager.prototype.setPath = function () {
        var browserPath;

        browserPath = Fluid.Lib.getUrlParam("location");

        if (Fluid.Lib.is(browserPath) && !Fluid.Lib.isArray(browserPath)) {
            this.paths.push(browserPath);
        }
        else {
            this.paths.push(this.conf.path);
        }
    };
    Fluid.FileManager.prototype.clearContent = function () {
        this.elements = [];
        this.content.innerHTML = "";
    };
    Fluid.FileManager.prototype.clearLists = function () {
        //to delete?
        if (Fluid.Lib.is(this.lists.folderContext)) {
            this.lists.folderContext.remove();
        }
        if (Fluid.Lib.is(this.lists.fileContext)) {
            this.lists.fileContext.remove();
        }
    };
    Fluid.FileManager.prototype.clearPaste = function (type) {
        this.chosenData = [];

        if (this.elements.length > 0) {
            this.elements.forEach(function (element) {
                if (element.classList.contains("move")) {
                    element.classList.remove("move");
                }
                else if (element.classList.contains("copy")) {
                    element.classList.remove("copy");
                }
            });
        }
    };
    Fluid.FileManager.prototype.getLastPath = function () {
        if (this.paths.length > 0) {
            return this.paths[this.paths.length - 1];
        }
        else if (Fluid.Lib.is(this.conf.path)) {
            return this.conf.path;
        }
        else {
            Fluid.Message(17, 3);
        }
    };
    Fluid.FileManager.prototype.createElement = function (element) {
        var elementBlock = document.createElement("div"),
            elementThumbWrap = document.createElement("div"),
            elementThumb = document.createElement("img"),
            elementNameWrap = document.createElement("div"),
            elementName = document.createElement("span"),
            elementSizeWrap = document.createElement("div"),
            elementSize = document.createElement("span"),
            type, thumb, _this = this;

        if (Fluid.Lib.is(element)) {
            elementBlock.classList.add("element", "row");
            elementBlock.dataset.path = element.path;

            if (element.type === "file") {
                if (element.thumb !== null) {
                    elementThumb = generateThumb();
                    elementThumbWrap.classList.add("elementThumbWrap", "col-lg-1", "col-md-1", "col-sm-2", "col-xs-3");

                    elementThumbWrap.appendChild(elementThumb);
                    elementBlock.appendChild(elementThumbWrap);
                }

                if (Fluid.Lib.is([element.name, element.ext])) {
                    elementName.innerHTML = element.name + "." + element.ext;
                    elementNameWrap.classList.add("elementNameWrap", "col-lg-8", "col-md-8", "col-sm-9", "col-xs-9");

                    elementNameWrap.appendChild(elementName);
                    elementBlock.appendChild(elementNameWrap);
                }

                if (Fluid.Lib.is([element.size, element.sizeUnit])) {
                    elementSize.innerHTML = element.size + " " + element.sizeUnit;
                    elementSizeWrap.classList.add("elementSizeWrap", "col-lg-2", "col-md-2", "col-sm-1", "hidden-xs");

                    elementSizeWrap.appendChild(elementSize);
                    elementBlock.appendChild(elementSizeWrap);
                }

                elementBlock.dataset.name = element.name + "." + element.ext;
                elementBlock.classList.add("file");
                this.addListeners("file", elementBlock);
            }
            else if (element.type === "folder") {
                elementThumb = generateThumb(true);
                elementThumbWrap.classList.add("elementThumbWrap", "col-lg-1", "col-md-1", "col-sm-2", "col-xs-3");

                elementThumbWrap.appendChild(elementThumb);
                elementBlock.appendChild(elementThumbWrap);

                if (Fluid.Lib.is([element.name])) {
                    elementName.innerHTML = element.name;
                    elementNameWrap.classList.add("elementNameWrap", "col-lg-8", "col-md-8", "col-sm-9", "col-xs-9");

                    elementNameWrap.appendChild(elementName);
                    elementBlock.appendChild(elementNameWrap);
                }

                if (Fluid.Lib.is([element.size, element.sizeUnit])) {
                    elementSize.innerHTML = element.size + " " + element.sizeUnit;
                    elementSizeWrap.classList.add("elementSizeWrap", "col-lg-1", "col-md-1", "col-sm-1", "col-xs-hidden");

                    elementSizeWrap.appendChild(elementSize);
                    elementBlock.appendChild(elementSizeWrap);
                }

                elementBlock.dataset.name = element.name;
                elementBlock.classList.add("folder");
                this.addListeners("folder", elementBlock);
            }

            elementBlock.dataset.dir = element.dir;

            this.elements.push(elementBlock);
            this.content.appendChild(elementBlock);
        }

        function generateThumb(folder) {
            var thumb = null,
                isFolder = folder || false;

            if (isFolder === false) {
                switch (element.category) {
                    case "image":
                        if (Fluid.Lib.is(element.thumb)) {
                            thumb = new Image();
                            thumb.src = element.thumb;
                            thumb.classList.add("thumb", "img-responsive");
                        }

                        break;

                    case "document":
                        thumb = document.createElement("i");
                        thumb.classList.add("fa", "fa-file-text-o", "thumb");

                        break;
                }
            }
            else {
                thumb = document.createElement("i");
                thumb.classList.add("fa", "fa-folder", "thumb");
            }

            return thumb;
        }
    };
    Fluid.FileManager.prototype.setContentHeight = function () {
        var height;

        if (Fluid.Lib.is(this.content)) {
            height = Fluid.Lib.getHeight();

            this.content.style.height = height - 130 + "px";
        }
    };
    Fluid.FileManager.prototype.addListeners = function (type, elm) {
        var _this = this;

        switch (type) {
            case "prevButton":
                if (Fluid.Lib.is(elm)) {
                    elm.addEventListener("click", function () {
                        _this.paths.pop();
                        _this.updateContent();
                    }, false);
                }
                break;
            case "nextButton":
                if (Fluid.Lib.is(elm)) {
                    elm.addEventListener("click", function () {

                    }, false);
                }
                break;
            case "homeButton":
                if (Fluid.Lib.is(elm)) {
                    elm.addEventListener("click", function () {
                        _this.paths = [_this.conf.path];
                        _this.updateContent();
                    }, false);
                }
                break;
            case "contentResize":
                if (Fluid.Lib.is(elm)) {
                    window.addEventListener("resize", function () {
                        _this.setContentHeight();
                    }, false);
                }
                break;
            case "addFolderButton":
                if (Fluid.Lib.is(elm)) {
                    elm.addEventListener("click", function () {
                        _this.newFolderModal = new Fluid.Modal({
                            header: "Wprowadź nazwę",
                            content: "<span class='text-center'>Wprowadź nazwę nowego folderu</span>",
                            actions: {
                                events: "close",
                                parent: "header"
                            },
                            type: "prompt",
                            promptSendButtonText: "Dodaj",
                            onPromptSubmit: function (value) {
                                var formData = new FormData(), toSend, url;

                                toSend = {
                                    name: value,
                                    dir: _this.getLastPath()
                                };
                                url = "/manage/files/newFolder"
                                formData.append("data", JSON.stringify(toSend));

                                Fluid.Http.post({
                                    url: url,
                                    toSend: formData,
                                    onResponse: function (response) {
                                        var result = JSON.parse(response);

                                        if (result.failed === false) {
                                            _this.updateContent();
                                            _this.newFolderModal.hide(true);
                                        }
                                        else {
                                            //display modal with error
                                        }
                                    }
                                });
                            }
                        });
                    }, false);
                }
                break;
            case "pasteButton":
                if (Fluid.Lib.is(elm)) {
                    elm.addEventListener("click", function () {
                        var __this = this;

                        if (_this.chosenData.length > 0) {
                            var formData = new FormData(), toSend, url;

                            toSend = {
                                paths: _this.chosenData,
                                placeToPaste: _this.getLastPath()
                            };
                            formData.append("data", JSON.stringify(toSend));
                            if (this.type === "copy") {
                                url = "/manage/files/copy";
                            }
                            else if (this.type === "move") {
                                url = "/manage/files/move";
                            }

                            Fluid.Http.post({
                                url: url,
                                toSend: formData,
                                onResponse: function (response) {
                                    var result = JSON.parse(response);

                                    if (result.failed === false) {
                                        _this.clearPaste(__this.type)
                                        _this.updateContent();
                                    }
                                    else {
                                        //display modal with error
                                    }

                                    __this.parentNode.removeChild(__this);
                                }
                            });
                        }
                        else {
                            //display modal with message that no files were selected
                        }
                    }, false);
                }
                break;
            case "folder":
                if (Fluid.Lib.is(elm)) {
                    elm.addEventListener("dblclick", function () {
                        _this.paths.push(this.dataset.path);
                        _this.updateContent();
                    }, false);

                    elm.addEventListener("click", function () {
                        handleSelection(this);
                    }, false);

                    elm.addEventListener("contextmenu", function (e) {
                        var list;

                        e = e || window.event;

                        _this.clearLists();

                        list = new Fluid.List({
                            name: 'folderContextList',
                            append: ".wrapper",
                            canHide: false,
                            hidden: false,
                            position: {x: e.clientX, y: e.clientY},
                            rows: [
                                {value: 0, element: 'Otwórz'},
                                {value: 1, element: 'Wytnij'},
                                {value: 2, element: 'Skopiuj'},
                                {value: 3, element: 'Wklej'},
                                {value: 4, element: 'Usuń'},
                                {value: 5, element: 'Zmień nazwę'},
                            ],
                            rowsPrefix: '<a href="#">',
                            rowsSuffix: '</a>',
                            emitter: elm,
                            onSelect: function (list) {
                                switch (list.selectedValue) {
                                    case "0":
                                        _this.paths.push(list.emmiter.dataset.path);
                                        _this.updateContent();
                                        break;
                                    case "1":
                                        copyOrMoveAction("move", list);
                                        break;
                                    case "2":
                                        copyOrMoveAction("copy", list);
                                        break;
                                    case "3":
                                        break;
                                    case "4":
                                        deleteElements(list);
                                        break;
                                    case "5":
                                        changeName(list, "folder");
                                        break;
                                }

                                list.hide();
                            }
                        });

                        _this.lists.folderContext = list;

                        e.preventDefault();
                    }, false);
                }
                break;
            case "file":
                if (Fluid.Lib.is(elm)) {
                    elm.addEventListener("click", function () {
                        handleSelection(this);
                    }, false);

                    elm.addEventListener("contextmenu", function (e) {
                        var list;

                        e = e || window.event;

                        _this.clearLists();

                        list = new Fluid.List({
                            name: 'folderContextList',
                            append: ".wrapper",
                            canHide: false,
                            hidden: false,
                            position: {x: e.clientX, y: e.clientY},
                            rows: [
                                {value: 0, element: 'Pobierz'},
                                {value: 1, element: 'Wytnij'},
                                {value: 2, element: 'Skopiuj'},
                                {value: 4, element: 'Usuń'},
                                {value: 5, element: 'Zmień nazwę'},
                                {value: 6, element: 'Właściwości'}
                            ],
                            rowsPrefix: '<a href="#">',
                            rowsSuffix: '</a>',
                            emitter: elm,
                            onSelect: function (list) {
                                var find = false;

                                switch (list.selectedValue) {
                                    case "0":
                                        window.location = "/manage/files/download?path=" + list.emmiter.dataset.path;
                                        break;
                                    case "1":
                                        copyOrMoveAction("move", list);
                                        break;
                                    case "2":
                                        copyOrMoveAction("copy", list);
                                        break;
                                    case "4":
                                        deleteElements(list);
                                        break;
                                    case "5":
                                        changeName(list, "file");
                                        break;
                                    case "6":
                                        new Fluid.Modal({
                                            header: "Właściwości - " + list.emmiter.dataset.name,
                                            content: "<span class='text-center'>Tu będą właściwości pliku.</span>",
                                            actions: {
                                                events: "close",
                                                parent: "header"
                                            },
                                            view: "standard",
                                            animation: false,
                                            type: "modal"
                                        });
                                        break;
                                }

                                list.hide();
                            }
                        });

                        _this.lists.fileContext = list;

                        e.preventDefault();
                    }, false);
                }
                break;
            case "shortcuts":
                document.addEventListener("keydown", function (e) {
                    e = e || window.event;

                    if (e.keyCode === 17) {
                        _this.operationsButton = true;
                    }

                    if (_this.operationsButton === true) {
                        console.log("operation", e.keyCode)
                        switch (e.keyCode) {

                        }
                    }

                }, false);
                document.addEventListener("keyup", function (e) {
                    e = e || window.event;

                    if (e.keyCode === 17) {
                        _this.operationsButton = false;
                    }
                }, false);
                break;
            case "managerWindow":
                _this.element.addEventListener("contextmenu", function (e) {
                    e = e || window.event;

                    e.preventDefault();

                    return false;
                }, false);
                break;
            case "fileUpload":
                if (Fluid.Lib.is(elm)){
                    elm.addEventListener("change", function () {
                        var files, fileData;

                        files = Fluid.Lib.toArray(this.files);

                        files.forEach(function (file) {
                            fileData = new FormData();

                            fileData.append("file", file);
                            fileData.append("extraData", JSON.stringify({
                                dir: _this.getLastPath()
                            }));

                            Fluid.Http.post({
                                url: _this.uploadForm.action,
                                toSend: fileData,
                                onResponse: function(){
                                    _this.updateContent();
                                },
                                onProgress: function (e) {
                                    if (e.lengthComputable) {
                                        //completed = (e.loaded / e.total) * 100;
                                        //filePreviewObj.progress.style.width = completed + "%";
                                    }
                                }
                            });
                        });
                    }, false);
                }
                break;
        }

        function handleSelection(elm) {
            if (_this.operationsButton === true) {
                if (elm.classList.contains("selected") && Fluid.Lib.is(elm.selectedIndex)) {
                    elm.classList.remove("selected");

                    if (_this.selected.indexOf(elm.selectedIndex) !== -1) {
                        _this.selected.splice(elm.selectedIndex, 1);
                    }
                }
                else {
                    elm.classList.add("selected");
                    elm.selectedIndex = _this.selected.push({
                        name: elm.dataset.name,
                        dir: elm.dataset.dir,
                        path: elm.dataset.path,
                        element: elm
                    });
                }
            }
            else {
                _this.selected = [];

                if (_this.elements.length) {
                    _this.elements.forEach(function (elm) {
                        if (elm.classList.contains("selected")) {
                            elm.classList.remove("selected");
                        }
                    });
                }
            }
        }

        function changeName(list, type) {
            var content;

            switch(type){
                case "folder":
                    content = "<span class='text-center'>Wprowadź nową nazwę folderu</span>";
                    break;
                case "file":
                    content = "<span class='text-center'>Wprowadź nową nazwę pliku</span>";
                    break;
            }

            _this.changeNameModal= new Fluid.Modal({
                header: "Zmień nazwę - " + list.emmiter.dataset.name,
                content: content,
                actions: {
                    events: "close",
                    parent: "header"
                },
                type: "prompt",
                promptSendButtonText: "Zmień",
                onPromptSubmit: function (value) {
                    var formData = new FormData(), toSend, url, modal;

                    toSend = {
                        oldName: list.emmiter.dataset.name,
                        newName: value,
                        dir: list.emmiter.dataset.dir
                    };
                    url = "/manage/files/rename"
                    formData.append("data", JSON.stringify(toSend));

                    Fluid.Http.post({
                        url: url,
                        toSend: formData,
                        onResponse: function (response) {
                            var result = JSON.parse(response);

                            if (result.failed === false) {
                                _this.updateContent();
                                _this.changeNameModal.hide(true);
                            }
                            else {
                                //display modal with error
                            }
                        }
                    });
                }
            });
        }

        function deleteElements(list) {
            var formData = new FormData(), url;

            _this.prepareChosenData(list, "delete");

            url = "/manage/files/delete";
            formData.append("data", JSON.stringify(_this.chosenData));

            Fluid.Http.post({
                url: url,
                toSend: formData,
                onResponse: function (response) {
                    var result = JSON.parse(response);

                    if (result.failed === false) {
                        _this.updateContent();
                    }
                    else {
                        //display modal with error
                    }
                }
            });
        }

        function copyOrMoveAction(type, list) {
            _this.clearPaste(type);
            _this.prepareChosenData(list, type);
            _this.createInterface(type + "Button");
        }
    };
    Fluid.FileManager.prototype.prepareChosenData = function (list, type) {
        var find = false;

        if (this.selected.length > 0) {
            this.selected.forEach(function (selected) {
                selected.element.classList.add(type);

                if (selected.name === list.emmiter.dataset.name) {
                    find = true;
                }
            });

            this.chosenData = this.selected;

            if (find === false) {
                list.emmiter.classList.add(type);

                this.chosenData.push({
                    dir: list.emmiter.dataset.dir,
                    name: list.emmiter.dataset.name
                });
            }
        }
        else {
            list.emmiter.classList.add(type);

            this.chosenData.push({
                dir: list.emmiter.dataset.dir,
                name: list.emmiter.dataset.name
            })
        }
    };

    Fluid.List = function (conf, elm) {
        this.info = {
            name: "List",
            id: 16
        };
        this.defaultConf = {
            name: 'defaultList',
            direction: 'down',
            defaultValue: 0,
            rows: [
                {
                    value: 0,
                    element: 'Wybierz z listy'
                }
            ],
            rowsPrefix: '',
            rowsSuffix: '',
            append: null,
            position: {x: 0, y: 0},
            canHide: true,
            onSelect: function (list) {
            },
            emitter: null,
            emitterEvent: null,
            hidden: true
        };
        this.conf = {};
        this.element = null;
        this.rows = [];
        this.emmiter = null;
        this.emitterEvent = null;
        this.selectedValue = null;
        this.hidden = null;
        this.initialized = false;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        if (Fluid.Lib.is(conf)) {
            this.conf = conf;

            Fluid.Loader.mergeConfiguration(this);

            if (Fluid.Lib.is(elm)) {
                this.element = elm;
            }

            this.init();
            this.addListeners("hideOnCLick");

            return this;
        }
        else {
            Fluid.Message(16, 0);
            return Fluid;
        }
    };
    Fluid.List.prototype.init = function () {
        var _this = this;

        if (!this.initialized) {
            if (this.conf.createdBy === "loader") {
                //console.log(this.conf)
            }
            else {
                if (Fluid.Lib.is(this.conf.append)) {
                    createList();
                }
                else {
                    Fluid.Message(16, 1);
                }
            }

            if (Fluid.Lib.is(this.conf.emitter)) {
                addEmitter();
            }

            displayStuff();

            this.element.classList.add("list");
            this.initialized = true;
        }

        function createList() {
            var list = document.createElement("ul"),
                li, appendParent;

            addRows();
            appendList();
            configureElement();
            addStaticPosition();

            _this.element = list;

            function addRows() {
                _this.conf.rows.forEach(function (row) {
                    li = document.createElement("li");

                    if (Fluid.Lib.is(row.element)) {
                        switch (typeof(row.element)) {
                            case "string":
                                li.innerHTML = _this.conf.rowsPrefix + row.element + _this.conf.rowsSuffix;

                                break;

                            case "object":
                                li.appendChild(row.element);
                        }

                        if (Fluid.Lib.is(row.value)) {
                            li.dataset.value = row.value;
                        }

                        if (!row.noEvent) {
                            _this.addListeners("row", li);
                        }

                        list.appendChild(li);
                    }
                    else {
                        //error!
                    }
                });
            }

            function configureElement() {
                list.classList.add("list");
                list.dataset.name = _this.conf.name;

                list.style.visibility = "hidden";
                list.style.display = "block";

                list.prop = list.getBoundingClientRect();
                list.styles = window.getComputedStyle(list);

                list.style.display = "";
                list.style.visibility = "";
            }

            function addStaticPosition() {
                var dHeight = Fluid.Lib.getHeight(),
                    dWidth = Fluid.Lib.getWidth(),
                    pos = {
                        x: 0,
                        y: 0
                    };

                if (Fluid.Lib.is([_this.conf.position.x, _this.conf.position.y])) {
                    list.style.position = "fixed";

                    if (_this.conf.position.y + list.prop.height >= dHeight) {
                        pos.y = dHeight - list.prop.height - 20;
                    }
                    else {
                        pos.y = _this.conf.position.y;
                    }

                    if (_this.conf.position.x + list.prop.width >= dWidth) {
                        pos.x = dWidth - list.prop.width - 20;
                    }
                    else {
                        pos.x = _this.conf.position.x;
                    }

                    list.style.left = pos.x + "px";
                    list.style.top = pos.y + "px";
                }
            }

            function appendList() {
                switch (typeof(_this.conf.append)) {
                    case "string":
                        appendParent = document.querySelector(_this.conf.append);

                        if (Fluid.Lib.is(appendParent)) {
                            appendParent.appendChild(list);
                        }
                        break;

                    case "object":
                        _this.conf.append.appendChild(list);
                        break;
                }
            }
        }

        function addEmitter() {
            if (typeof(_this.conf.emitter) === "object") {
                _this.emmiter = _this.conf.emitter;
            }
            else if (typeof(_this.conf.emitter) === "string") {
                _this.emmiter = document.querySelector(_this.conf.emitter);
            }

            if (Fluid.Lib.is(_this.conf.emitterEvent)) {
                _this.emitterEvent = _this.conf.emitterEvent;
            }
            else {
                _this.emitterEvent = "click";
            }

            _this.addListeners("emitter")
        }

        function displayStuff() {
            _this.hidden = _this.conf.hidden;

            if (_this.hidden === true) {
                _this.hide();
            }
            else {
                _this.show();
            }

        }
    };
    Fluid.List.prototype.addListeners = function (type, elm) {
        var _this = this;

        switch (type) {
            case "row":
                if (Fluid.Lib.is(elm)) {
                    elm.addEventListener("click", function () {
                        if (this.dataset.value) {
                            _this.selectedValue = this.dataset.value;
                        }

                        _this.conf.onSelect(_this);
                    }, false);
                }
                break;

            case "emitter":
                if (Fluid.Lib.is(this.emmiter)) {
                    this.emmiter.addEventListener(this.emitterEvent, function () {
                        if (_this.hidden === true) {
                            _this.show();
                        }
                        else if (_this.hidden === false) {
                            _this.hide();
                        }
                    }, false);
                }
                break;

            case "hideOnCLick":
                Fluid.Browser.handleOffEvents({
                    element: _this.element,
                    notClicked: function (element) {
                        if (_this.conf.canHide) {
                            _this.hide();
                        }
                        else {
                            _this.remove();
                        }

                    },
                    excludedElements: [_this.emmiter]
                });

                break;
        }
    };
    Fluid.List.prototype.show = function () {
        this.hidden = false;

        this.element.classList.remove("listClose");
        this.element.classList.add("listOpen");
    };
    Fluid.List.prototype.hide = function () {
        this.hidden = true;

        this.element.classList.remove("listOpen");
        this.element.classList.add("listClose");
    };
    Fluid.List.prototype.remove = function () {
        this.hide();

        if (Fluid.Lib.is(this.element.parentNode)) {
            this.element.parentNode.removeChild(this.element);
        }

        delete this;
    };

    Fluid.Panel = function(conf, elm){
        this.info = {
            name: "Panel",
            id: 11
        };
        this.defaultConf = {
            position: "left",
            title: "Przykładowy panel",
            animation: true,
            dim: true,
            touchShow: false,
            touchHide: true,
            create: false,
            showEmitter: "#showPanelEmitter",
            hideEmitter: "#hidePanelEmitter",
            hidden: true,
            nextPanelTimeLoad: 500
        };
        this.conf = {};
        this.element = null;
        this.elementProp = {};
        this.hideBtn = null;
        this.showBtn = null;
        this.data = {
            panel: {
                left: "none",
                right: "none",
                top: "none",
                bottom: "none",
                translate: ""
            },
            hideBtn: {
                left: "none",
                right: "none",
                top: "none",
                bottom: "none",
                class: ""
            }
        };
        this.hidden = null;
        this.touchArea = 20;

        this.initialized = false;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        if (Fluid.Lib.is(conf)) {
            this.conf = conf;

            Fluid.Loader.mergeConfiguration(this);

            if (Fluid.Lib.is(elm)) {
                this.element = elm;
                this.elementProp = elm.getBoundingClientRect();
            }

            this.hidden = this.conf.hidden === true;

            this.init();
            this.addListeners("init");

            return this;
        }
        else {
            Fluid.Message(11, 0);
            return Fluid;
        }
    };
    Fluid.Panel.prototype.init = function(){
        if(Fluid.Lib.is(this.element)){
            this.element.classList.add("panel", "panel" + this.conf.position.toUpperCase());

            this.handleTitle();
            this.handleDim();
            this.handleEmitters();
            this.prepareElementsData();
            this.setElementsData(true);

            this.initialized = true;
        }
    };
    Fluid.Panel.prototype.handleEmitters = function() {
        var button;

        if(this.conf.hideEmitter){
            button = document.querySelector(this.conf.hideEmitter);

            if(button){
                this.hideBtn = button;

                button.classList.add("fa", "hideBtn");
                this.addListeners("hide", button);
            }
            else{
                //create one ?
            }
        }

        if(this.conf.showEmitter){
            button = document.querySelector(this.conf.showEmitter);

            if(button){
                this.showBtn = button;

                button.classList.add("showBtn");
                this.addListeners("show", button);
            }
            else{
                //create one ?
            }
        }
    };
    Fluid.Panel.prototype.handleTitle = function() {
        var title = null;

        if(this.conf.title.length > 0){
            title = document.createElement("div");
            title.classList.add("title");
            title.innerHTML = this.conf.title;

            this.element.insertBefore(title, this.element.childNodes[0]);
        }
    };
    Fluid.Panel.prototype.handleDim = function() {
        if(this.conf.dim === true){
            if(Fluid.Browser.dim === null){
                new Fluid.Dim();
            }
        }
    };
    Fluid.Panel.prototype.prepareElementsData = function() {
        this.updateDimensions();

        switch (this.conf.position) {
            case "left":
                this.data.panel.left = 0;
                this.data.panel.top = 0;
                this.data.panel.translate = "translateX(-" + this.elementProp.width + "px)";
                this.data.panel.width = null;
                this.data.panel.height = Fluid.Lib.getHeight() + "px";

                this.data.hideBtn.top = "1rem";
                this.data.hideBtn.right = "1.5rem";
                this.data.hideBtn.class = "fa-angle-left";
                break;

            case "right":
                this.data.panel.right = 0;
                this.data.panel.top = 0;
                this.data.panel.translate = "translateX(" + this.elementProp.width + "px)";
                this.data.panel.width = null;
                this.data.panel.height = Fluid.Lib.getHeight() + "px";

                this.data.hideBtn.top = "1rem";
                this.data.hideBtn.left = "1.5rem";
                this.data.hideBtn.class = "fa-angle-right";
                break;
            case "top":
                this.data.panel.left = 0;
                this.data.panel.top = 0;
                this.data.panel.translate = "translateY(-"+this.elementProp.height+"px)";
                this.data.panel.width = Fluid.Lib.getWidth() + "px";
                this.data.panel.height = null;

                this.data.hideBtn.bottom = "1rem";
                this.data.hideBtn.right = "1.5rem";
                this.data.hideBtn.class = "fa-angle-up";
                break;
            case "bottom":
                this.data.panel.left = 0;
                this.data.panel.bottom = 0;
                this.data.panel.translate = "translateY("+this.elementProp.height+"px)";
                this.data.panel.width = Fluid.Lib.getWidth() + "px";
                this.data.panel.height = null;

                this.data.hideBtn.top = "1rem";
                this.data.hideBtn.right = "1.5rem";
                this.data.hideBtn.class = "fa-angle-down";
                break;
        }
    };
    Fluid.Panel.prototype.setElementsData = function(firstUse) {
        var _this = this;

        firstUse = firstUse || false;

        this.element.style.left = this.data.panel.left;
        this.element.style.right = this.data.panel.right;
        this.element.style.top = this.data.panel.top;
        this.element.style.bottom = this.data.panel.bottom;

        if(this.data.panel.width !== null){
            this.element.style.width = this.data.panel.width;
        }

        if(this.data.panel.height !== null){
            this.element.style.height = this.data.panel.height;
        }

        if (this.conf.hideEmitter && this.hideBtn !== null) {
            this.hideBtn.classList.add(this.data.hideBtn.class);
            this.hideBtn.style.top = this.data.hideBtn.top;
            this.hideBtn.style.bottom = this.data.hideBtn.bottom;
            this.hideBtn.style.left = this.data.hideBtn.left;
            this.hideBtn.style.right = this.data.hideBtn.right;
        }

        if(this.hidden === true) {
            handleTransition(function () {
                Fluid.Lib.setTransform(_this.element, _this.data.panel.translate);
            }, firstUse);
        }

        function handleTransition(func, firstUse){
            var backupStyles,
                properties = ["transition-property", "transition-duration", "transition-timing-function", "transition-delay"];

            if(Fluid.Lib.isFunction(func)){
                backupStyles = Fluid.Lib.getStyle(_this.element, properties);

                properties.forEach(function(prop){
                    _this.element.style[Fluid.Lib.dashToUpper(prop)] = "none";
                });

                func();

                if(firstUse){
                    setTimeout(function(){
                        properties.forEach(function(prop){
                            _this.element.style[Fluid.Lib.dashToUpper(prop)] = backupStyles[prop];
                        });
                    }, 100)
                }
                else{
                    properties.forEach(function(prop){
                        _this.element.style[Fluid.Lib.dashToUpper(prop)] = backupStyles[prop];
                    });
                }
            }
        }
    };
    Fluid.Panel.prototype.updateDimensions = function(){
        this.elementProp = this.element.getBoundingClientRect();
    };
    Fluid.Panel.prototype.show = function(){
        var wasHidden = Fluid.Browser.hidePanels(),
            time = 0,
            _this = this;

        this.prepareElementsData();
        this.setElementsData();
        this.hidden = false;

        if(wasHidden){
            time = this.conf.nextPanelTimeLoad;
        }

        setTimeout(function(){
            Fluid.Lib.setTransform(_this.element, "translate(0px)");

            if(_this.conf.dim === true) {
                Fluid.Browser.dim.show();
            }
        }, time);
    };
    Fluid.Panel.prototype.hide = function(){
        this.hidden = true;

        Fluid.Lib.setTransform(this.element, this.data.panel.translate);

        if(this.conf.dim === true){
            Fluid.Browser.dim.hide();
        }
    };
    Fluid.Panel.prototype.addListeners = function(type, elm){
        var _this = this;

        if(type){
            switch(type){
                case "init":
                    window.addEventListener("resize", function(){
                        _this.prepareElementsData();
                        _this.setElementsData();
                    });
                    break;
                case "hide":
                    if(elm){
                        elm.addEventListener("click", function(){
                            if(_this.hidden === false){
                                _this.hide();
                            }
                        }, false);
                    }
                    break;
                case "show":
                    if(elm){
                        elm.addEventListener("click", function(){
                            if(_this.hidden === true){
                                _this.show();
                            }
                            else{
                                _this.hide();
                            }

                        }, false);
                    }
                    break;
            }
        }
    };

    Fluid.TableView = function(conf, elm){
        this.info = {
            name: "TableView",
            id: 18
        };
        this.defaultConf = {
            ajax: true
        };
        this.conf = {};
        this.element = null;
        this.tableStandard = null;
        this.tableMobile = null;
        this.tableFoot = null;
        this.standardRows = [];
        this.actions = [];
        this.initialized = false;

        Fluid.checkCaller(this);
        Fluid.addToElements(this);

        if (Fluid.Lib.is(conf)) {
            this.conf = conf;

            Fluid.Loader.mergeConfiguration(this);

            if (Fluid.Lib.is(elm)) {
                this.element = elm;
            }

            this.init();

            return this;
        }
        else {
            Fluid.Message(18, 0);
            return Fluid;
        }
    };
    Fluid.TableView.prototype.init = function(){
        var _this = this;

        loadMainElements();
        initStandardView();
        initActions();
        this.addSelection();

        function loadMainElements(){
            var nodes = Fluid.Lib.toArray(_this.element.getElementsByTagName("*"));

            nodes.forEach(function(node){
                if(node.dataset.tableViewRole){
                    switch(node.dataset.tableViewRole){
                        case "tableStandard":
                            _this.tableStandard = node;
                            node.removeAttribute("data-table-view-role");
                            break;
                        case "tableMobile":
                            _this.tableMobile = node;
                            node.removeAttribute("data-table-view-role");
                            break;
                        case "tableFoot":
                            _this.tableFoot = node;
                            node.removeAttribute("data-table-view-role");
                            break;
                    }
                }
            });
        }
        function initStandardView(){
            var rows  = Fluid.Lib.toArray(_this.tableStandard.getElementsByClassName("tableRow"));

            rows.forEach(function(row){
                if(row.dataset.id){
                    row.dbId = row.dataset.id;
                    row.removeAttribute("data-id");
                    _this.standardRows.push(row);

                    _this.addListeners("row", row);
                }
            });
        }
        function initActions(){
            var row, children;

            if(_this.standardRows.length > 0){
                row = _this.standardRows[0];
                children = Fluid.Lib.toArray(row.getElementsByTagName("*"));

                children.forEach(function(node){
                    if(node.dataset.tableViewRole === "action"){
                        _this.actions.push({
                            url: node.getAttribute("href"),
                            name: node.innerHTML
                        });
                    }
                });
            }
        }
    };
    Fluid.TableView.prototype.addListeners = function(type, elm){
        var _this = this;

        if(Fluid.Lib.is(type)){
            switch(type){
                case "row":
                    if(Fluid.Lib.is(elm)){
                        elm.addEventListener("contextmenu", function (e) {
                            var listConf;

                            e = e || window.event;

                            if(_this.actions.length > 0){
                                listConf = {
                                    name: 'tableViewContextList',
                                    append: ".wrapper",
                                    canHide: false,
                                    hidden: false,
                                    position: {x: e.clientX, y: e.clientY},
                                    emitter: elm,
                                    rows: []
                                };

                                _this.actions.forEach(function(action, index){
                                    listConf.rows.push({
                                        value: index,
                                        element: "<a href='"+action.url+"'>"+action.name+"</a>"
                                    });
                                });

                                listConf.onSelect = function (list) {
                                    list.hide();
                                };

                                new Fluid.List(listConf);
                            }

                            e.preventDefault();
                        }, false);
                    }
                    break;
            }
        }
    };
    Fluid.TableView.prototype.addSelection = function(row){
        row = row || null;

        //if(rro)
    };

    Fluid.Library = function () {
        this.info = {
            name: "Library",
            id: 1
        };
        this.errors = [];
    };
    Fluid.Library.prototype.is = function (obj) {
        var arg = Fluid.Lib.toArray(arguments),
            errors = 0;

        arg.forEach(function (obj) {
            if (obj === "undefined" || obj === undefined || obj === false || obj === "false" || obj === null || obj === "null") {
                errors++;
            }
        });

        if (errors === 0) {
            return true;
        }
        else {
            this.errors = errors;
            return false;
        }
    };
    Fluid.Library.prototype.toArray = function (obj) {
        var array = [];

        if (obj instanceof Array) {
            array = obj;
        }
        else if (obj instanceof Object) {
            for (prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    array.push(obj[prop]);
                }
            }
        }
        else if (typeof(obj) === "string") {
            array.push(obj);
        }

        return array;
    };
    Fluid.Library.prototype.setTransform = function(elm, property){
        elm.style.WebkitTransform = property;
        elm.style.MozTransform = property;
        elm.style.MsTransform = property;
        elm.style.OTransform = property;
        elm.style.transform = property;
    };
    Fluid.Library.prototype.dashToUpper = function(word){
        var newWord = "";

        if(word.length){
            for(var i = 0; i< word.length; i++){
                if(word[i] === "-"){
                    newWord += word[i + 1].toUpperCase();
                    i++;
                }
                else{
                    newWord += word[i];
                }

            }
        }

        return newWord;
    };
    Fluid.Library.prototype.getStyle = function (elm, props) {
        var elementStyles,
            resultStyles = {};

        elementStyles = window.getComputedStyle(elm, null);

        if(Fluid.Lib.isArray(props) === false){
            props = [props];
        }

        props.forEach(function(prop){
            resultStyles[prop] = elementStyles.getPropertyValue(prop);
        });

        if(resultStyles.length <= 0){
            return null;
        }
        else{
            return resultStyles;
        }
    };
    Fluid.Library.prototype.getUrlParam = function (param) {
        var params = {},
            hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

        param = param || null;

        for (var i = 0, hash = null; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            params[hash[0]] = hash[1];
        }

        if (param === null) {
            return params;
        }
        else {
            if (Fluid.Lib.is(params[param])) {
                return params[param];
            }
            else {
                //Fluid.Message();
            }
        }
    };
    Fluid.Library.prototype.strBoolToBool = function (str) {
        var result = str;

        if (typeof(str) === "string") {
            if (str === "true") {
                result = true;
            }
            else if (str === "false") {
                result = false;
            }
        }

        return result;
    };
    Fluid.Library.prototype.isFunction = function (func) {
        var getType = {};
        return func && getType.toString.call(func) === '[object Function]';
    };
    Fluid.Library.prototype.getHeight = function () {
        var body = document.body,
            bodyProp = body.getBoundingClientRect();

        return bodyProp.height;
    };
    Fluid.Library.prototype.getWidth = function () {
        var body = document.body,
            bodyProp = body.getBoundingClientRect();

        return bodyProp.width;
    };
    Fluid.Library.prototype.isArray = function (array) {
        return Array.isArray(array);
    };
    Fluid.Library.prototype.hasChild = function(haystack, needle){
        var children, response = false;

        if(Fluid.Lib.is([haystack, needle])){
            children = Fluid.Lib.toArray(haystack.childNodes);

            if(children.length > 0){
                children.forEach(function(node){
                    if(node.tagName.toLowerCase() === needle.toLowerCase()){
                        response = true;
                        return true;
                    }
                });
            }
        }

        return response;
    };

    Fluid.HttpLib = function () {
        this.info = {
            name: "Http",
            id: 2
        };
        this.debug = true;
        this.http = new XMLHttpRequest();
    };
    Fluid.HttpLib.prototype.post = function (conf) {
        var _this = this;

        /*
         Params:
         url: "",
         onProgress: function(){},
         onResponse: function(){},
         toSend: (json) {}
         */

        if (Fluid.Lib.is(conf.url)) {
            this.http.open("POST", conf.url, true);
            this.http.setRequestHeader("contentType", "application/x-www-form-urlencoded");
            this.http.onreadystatechange = function () {
                if (_this.http.status === 200 && _this.http.readyState === 4) {
                    if (_this.debug) {
                        console.log(_this.http.responseText);
                    }

                    if (Fluid.Lib.is(conf.onResponse) && Fluid.Lib.isFunction(conf.onResponse)) {
                        conf.onResponse(_this.http.responseText);
                    }
                }
            };

            this.http.upload.addEventListener("progress", function (e) {
                if (Fluid.Lib.is(conf.onProgress) && Fluid.Lib.isFunction(conf.onProgress)) {
                    conf.onProgress(e);
                }
            }, false);

            if (!Fluid.Lib.is(conf.toSend)) {
                this.http.send();
            }
            else {
                if (_this.debug) {
                    console.log(conf.toSend);
                }

                this.http.send(conf.toSend);
            }
        }

    };
    Fluid.HttpLib.prototype.get = function (url, toSend, callback) {
        //to rewrite
        var _this = this;

        this.http.open("GET", url, true);
        this.http.onreadystatechange = function () {
            if (_this.http.status === 200 && _this.http.readyState === 4) {
                callback(_this.http.responseText);
            }
        };

        if (!toSend) {
            this.http.send();
        }
        else {
            this.http.send(toSend);
        }
    };

    Fluid.Message = function (index, subIndex, mod, display, type) {
        var modName = null,
            type = type || "warn",
            info = {
                name: "Message",
            },
            messages = {};

        if (Fluid.Lib.is(mod)) {
            modName = mod.info.name || "Some Module";
        }

        if (display !== false) {
            display = true;
        }

        messages = {
            0: {
                0: Fluid.info.name + " are already started.",
                1: Fluid.info.name + " Your configuration is corruped."
            },
            1: {
                0: Fluid.info.name + " - " + Fluid.modules[1] + ": Error array is not empty.",
            },
            10: {
                0: Fluid.info.name + " - " + Fluid.modules[10] + ": You must provide configuration.",
                1: Fluid.info.name + " - " + Fluid.modules[10] + ": You can't show and hide modal in the same time.",
                2: Fluid.info.name + " - " + Fluid.modules[10] + ": You can't hide modal, if you haven't showed it before.",
                3: Fluid.info.name + " - " + Fluid.modules[10] + ": You can't show modal, if you haven't hidden it before.",
                4: Fluid.info.name + " - " + Fluid.modules[10] + ": You can't get ajax content and ordinary content in the same time.",
                5: Fluid.info.name + " - " + Fluid.modules[10] + ": Already initialized.",
            },
            11: {
                0: Fluid.info.name + " - " + Fluid.modules[11] + ": You must provide configuration.",
            },
            15: {
                0: Fluid.info.name + " - " + Fluid.modules[15] + ": You must provide configuration.",
                1: Fluid.info.name + " - " + Fluid.modules[15] + ": You must provide some submit button.",
            },
            16: {
                0: Fluid.info.name + " - " + Fluid.modules[16] + ": You must provide configuration.",
                1: Fluid.info.name + " - " + Fluid.modules[16] + ": You must pass `append` property.",
            },
            17: {
                0: Fluid.info.name + " - " + Fluid.modules[17] + ": You must provide configuration.",
            },
            18: {
                0: Fluid.info.name + " - " + Fluid.modules[18] + ": You must provide configuration.",
            },
            x: Fluid.info.name + " - " + modName + ": Cannot call " + modName + " as regular function.",
        };

        if (Fluid.Lib.is(index, subIndex)) {
            if (display) {
                console[type](messages[index][subIndex]);
            }
            else {
                return messages[index][subIndex];
            }
            return;
        }

        if (Fluid.Lib.is(index, mod)) {
            if (display) {
                console[type](messages[index]);
                return;
            }
            else {
                return messages[index];
            }
        }

        console.error(Fluid.info.name + " - " + info.name + ": You didn't privide required parameters.");
        return;
    };

    Fluid.init();
}());