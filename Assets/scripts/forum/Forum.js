(function() {
    var Forum = {
        wrapper: document.getElementsByClassName("wrapper")[0],
        started: false,
        elements: {},
        temporary: {},
        dataName: 'forum',

        init: function () {
            window.addEventListener("load", function() {
                if (Fluid && Forum.started === false) {
                    Fluid.Loader.load(Forum);

                    console.info("Forum started.");
                }
            }, false);
        }
    };

    Forum.init();
}());
