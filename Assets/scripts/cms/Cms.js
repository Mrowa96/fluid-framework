(function() {
    var Cms = {
        wrapper: document.getElementsByTagName("body")[0],
        started: false,
        elements: {},
        temporary: {},
        dataName: 'cms',

        init: function () {
            window.addEventListener("load", function() {
                if (Fluid && Cms.started === false) {
                    Fluid.Loader.load(Cms);

                    console.info("Cms started.");
                }
            }, false);
        }
    };

    Cms.init();
}());
