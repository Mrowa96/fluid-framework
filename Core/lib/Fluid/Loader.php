<?php
    use Fluid\Fluid;

    function __autoload($className) {
        $match = false;
        $parts = explode('\\', $className);
        $fileName = end($parts).".php";

        if($parts[0] == "Fluid"){
            array_shift($parts);

            $path = implode("/", $parts);

            if(is_file(FLUID_DIR.$path.".php")){
                include FLUID_DIR.$path.".php";
                $match = true;
            }
        }

        if(!$match){
            $module = Fluid::$module;
            if(checkModuleFiles($module['name'], $fileName) === false){
                if(checkModuleFiles("default", $fileName) === false){
                    throw new Exception("Cannot load specyfic class - ".$fileName);
                }
            }
        }
    }

    function checkModuleFiles($moduleName, $fileName){
        if(file_exists(CONTROLLERS_DIR.$moduleName.DS.$fileName)){
            include CONTROLLERS_DIR.$moduleName.DS.$fileName;
            return true;
        }
        else if(file_exists(MODELS_DIR.$moduleName.DS.$fileName)){
            include MODELS_DIR.$moduleName.DS.$fileName;
            return true;
        }
        else if(file_exists(MODELS_DIR.$fileName)){
            include MODELS_DIR.$fileName;
            return true;
        }
        else if(file_exists(PLUGINS_DIR.$moduleName.DS.$fileName)){
            include PLUGINS_DIR.$moduleName.DS.$fileName;
            return true;
        }

        return false;
    }

    function exceptionHandlerProduction($exception) {
        $fluid = Fluid::getInstance();

        $fluid->goToError($exception);
    }
