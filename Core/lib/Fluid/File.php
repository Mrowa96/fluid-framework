<?php
    namespace Fluid;

    class File{

        public $name;
        public $size;
        public $ext;
        public $dir;
        public $type;
        public $sizeUnit;
        private $file;
        private $fileData;
        private $types = [
            'image' => ['png', 'jpg', 'bmp', 'gif', 'tiff'],
            'music' => ['mp3'],
            'document' => ['docx', 'pdf', 'txt']
        ];

        public function __construct($file, $newFile = false){
            if($newFile === false && file_exists($file) && is_file($file)){
                $info = pathinfo($file);
                $sizeInfo = Lib::humanBytesFormat(filesize($file));

                $this->file = $file;
                $this->name = $info['filename'];
                if(isset($info['extension'])){
                    $this->ext = $info['extension'];
                }
                $this->dir = $info['dirname'];
                $this->size = $sizeInfo['size'];
                $this->sizeUnit = $sizeInfo['sizeUnit'];
                $this->type = $this->getType();

            }
            else if($newFile === true){
                $this->fileData = $file;

                $this->saveFile();
            }
            else{
                throw new \Exception("File ".$file." doesn't exist in given path");
            }
        }

        public function saveFile(){
            move_uploaded_file($this->fileData['tmp_name'], $this->fileData['path']);
        }

        private function getType(){
            $cat = null;

            if(!empty($this->ext)){
                foreach($this->types AS $category => $type){
                    if(in_array($this->ext, $type)){
                        $cat = $category;
                    }
                }

                if($cat === null){
                    throw new \Exception("File doesn't pass requirements.");
                }
                else{
                    return $cat;
                }
            }
        }
    }