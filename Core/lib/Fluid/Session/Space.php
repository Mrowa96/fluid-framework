<?php
    namespace Fluid\Session;

    class Space{

        public $name;

        public function __construct($name){
            $this->name = $name;
        }

        public function is($propName){
            if(isset($this->{$propName}) && !empty($this->{$propName})){
                return true;
            }
            else{
                return false;
            }
        }

        public function get($propName){
            if($this->is($propName)){
                return $this->{$propName};
            }
            else{
                return null;
            }
        }
    }