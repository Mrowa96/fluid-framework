<?php
    namespace Fluid\Session;

    class Session{

        public static function start(){
            session_start();
        }

        public static function isStarted(){
            return session_status() === PHP_SESSION_ACTIVE ? true : false;
        }

        public static function close(){
            session_destroy();
        }

        public static function &get($varName){
            if(!isset($_SESSION[$varName])){

                $_SESSION[$varName] = null;
            }

            return $_SESSION[$varName];
        }

        public static function add($varName, $valValue){
            if(!isset($_SESSION[$varName])){
                $_SESSION[$varName] = $valValue;

                return $_SESSION[$varName];
            }
            else{
                return false;
            }
        }

        public static function set($varName, $varValue){
            if(isset($_SESSION[$varName])){
                $_SESSION[$varName] = $varValue;

                return true;
            }
            else{
                return false;
            }
        }

        public static function remove($varName){
            if(isset($_SESSION[$varName])){
                unset($_SESSION[$varName]);

                return true;
            }
            else{
                return false;
            }
        }

        public static function space($name){
            if(!empty($name)){
                if(!isset($_SESSION['namespaces'])){
                    $_SESSION['namespaces'] = [];
                }

                if(isset($_SESSION['namespaces'][$name])){
                    return $_SESSION['namespaces'][$name];
                }
                else{
                    $namespace = new Space($name);
                    $_SESSION['namespaces'][$name] = $namespace;

                    return $_SESSION['namespaces'][$name];
                }
            }
        }

        public static function destroySpace($name){
            if(isset($_SESSION['namespaces'][$name])){
                unset($_SESSION['namespaces'][$name]);
            }
        }
    }