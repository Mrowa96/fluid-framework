<?php
    namespace Fluid\View;

    class Partial extends View{

        private $name;
        private $extension;
        private $data = [];
        private $view;
        private $oldProperties = [];

        public function __construct($name, $extension, $data, View $view){
            parent::__construct();
            $this->name = $name;
            $this->extension = $extension;
            $this->data = $data;
            $this->view = $view;
        }

        public function render(){
            if(defined("MODULE_PARTIALS_DIR")){
                $this->prepare();
                include MODULE_PARTIALS_DIR.DS.$this->name.$this->extension;
                $this->restore();
            }
            else{
                throw new \Exception("Constant MODULE_PARTIALS_DIR cannot be undefined.");
            }
        }

        private function prepare(){
            if(!empty($this->data)){
                foreach($this->data AS $key => $value){
                    if(is_numeric($key)){
                        throw new \Exception("Array key in partial passed data cannot be numeric.");
                    }

                    if(property_exists($this->view, $key)){
                        $this->oldProperties[$key] = $this->view->{$key};
                    }

                    $this->{$key} = $value;
                }
            }
        }

        private function restore(){
            if(!empty($this->oldProperties)){
                foreach($this->oldProperties AS $key => $value){
                    $this->view->{$key} = $value;
                }
            }
        }
    }