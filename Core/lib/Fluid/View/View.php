<?php
    namespace Fluid\View;

    use Fluid\Auth\Auth;
    use Fluid\Fluid;
    use Fluid\Log;
    use Fluid\Request;

    class View{
        public $layout;
        protected $extensions;
        protected $defaultExtensions = [
            'styles' => '.css',
            'scripts' => '.js',
            'views' => '.phtml'
        ];
        protected $noContent = false;
        protected $moduleAssets;
        protected $globalAssets;
        protected $Log;
        protected $Request;
        protected $Auth;
        protected $routers;

        public function __construct(){
            $this->Log = new Log();
            $this->Auth = new Auth();

            $config = Fluid::$module['config'];
            $this->routers = require CONFIG_DIR.MODULE_NAME.DS."routers.php";

            $this->extensions = (isset($config['extensions'])) ? $config['extensions'] : $this->defaultExtensions;
            $this->moduleAssets = (isset($config['assets'])) ? $config['assets'] : null;
            $this->globalAssets = (file_exists(CONFIG_DIR.DS.'assets.php')) ? require CONFIG_DIR.DS.'assets.php' : null;
        }

        public function partial($name, $passedData = []){
            $partial = new Partial($name, $this->extensions['views'], $passedData, $this);

            $partial->render();
        }

        public function renderLayout(){
            if(empty($this->layout)){
                $this->layout = "default";
            }

            if(file_exists(MODULE_LAYOUTS_DIR.DS.$this->layout.$this->extensions['views'])){
                include_once MODULE_LAYOUTS_DIR.DS.$this->layout.$this->extensions['views'];
            }
            else{
                throw new \Exception("Layout ".$this->layout." hasn't been found in layouts dir.");
            }
        }

        public function renderContent($controller = null, $action = null){
            $this->Request = Request::getInstance();

            if(!$this->noContent){
                $controller = (!empty($controller)) ? mb_strtolower($controller) : mb_strtolower($this->Request->params['controller']);
                $action =(!empty($action)) ? $action : $this->Request->params['action'];

                if(file_exists(VIEWS_DIR.MODULE_NAME.DS.$controller.DS.$action.$this->extensions['views'])){
                    include_once VIEWS_DIR.MODULE_NAME.DS.$controller.DS.$action.$this->extensions['views'];
                }
            }
        }

        public function t($text){
            return $text;
        }

        public function url($url, $params = []){
            if($url === "home"){
                $url = "/".Fluid::$module['url'];
            }
            else{
                $url = "/".Fluid::$module['url']."/".$url;
            }

            if(!empty($params)){
                $url .= "?";

                foreach($params AS $key => $value){
                    $url .= $key."=".$value."&";
                }

                $url = mb_strcut($url, 0, mb_strlen($url) - 1);
            }

            $url .= MODULE_URL_SUFFIX;

            return $url;
        }

        public function route($name, $params = []){
            $url = "";

            if(!empty($this->routers)){
                foreach($this->routers AS $routeName => $routeData){
                    if($routeName === $name){
                        $url = $routeData[0];

                        foreach($params AS $key => $param){
                            $url = str_replace("{".$key."}", $param, $url);
                        }

                        $url = MODULE_URL."/".$url.MODULE_URL_SUFFIX;
                        break;
                    }
                }
            }

            return $url;
        }

        public function getTitle(){}
        public function getKeywords(){}
        public function getDescription(){}

        public function getStyles(){
            $styles = "";
            $styles .= $this->checkSomething($this->globalAssets, "styles");
            $styles .= $this->checkSomething($this->moduleAssets, "styles", MODULE_NAME);

            return $styles;
        }

        public function getScripts(){
            $scripts = "";
            $scripts .= $this->checkSomething($this->globalAssets, "scripts");
            $scripts .= $this->checkSomething($this->moduleAssets, "scripts", MODULE_NAME);

            return $scripts;
        }

        public function getImage($name){
            if(file_exists(ASSETS_DIR.'images'.DS.MODULE_NAME.DS.$name)){
                return IMAGES_URL.MODULE_NAME.DS.$name;
            }
            else{
                return IMAGES_URL.'default'.DS.'no-image.jpg';
            }
        }

        /*public function goResponsive(){
            $responsive = "";
            $responsive .= $this->checkSomething($this->globalAssets, "others", null, "responsiveFramework");
            $responsive .= $this->checkSomething($this->moduleAssets, "others", MODULE_NAME, "responsiveFramework");

            return $responsive;
        }*/

        private function checkSomething($objToCheck, $indexToCheck, $moduleName = "", $secondIndex = null){
            $data = "";

            if(!empty($objToCheck)){
                if(isset($objToCheck[$indexToCheck]) && !empty($objToCheck[$indexToCheck])){
                    if(!empty($secondIndex) && isset($objToCheck[$indexToCheck][$secondIndex])){
                        $data .= $this->createResponsive($objToCheck[$indexToCheck][$secondIndex]);
                    }
                    else{
                        if($indexToCheck === "styles"){
                            foreach($objToCheck[$indexToCheck] AS $style){
                                $data .= $this->createStyle($style, $moduleName);
                            }
                        }
                        else if($indexToCheck === "scripts"){
                            foreach($objToCheck[$indexToCheck] AS $script){
                                $data .= $this->createScript($script, $moduleName);
                            }
                        }
                    }
                }
            }

            return $data;
        }

        /*private function createResponsive($other){
            if(is_string($other)){
                switch(strtolower($other)){
                    case "bootstrap":
                        define("RESPONSIVE_NAME", "bootstrap");

                        return '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
                                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
                                <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
                                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>';
                }
            }
            else if(is_array($other) && !empty($other)){
                foreach($other AS $o){
                    //to be done
                }
            }
        }*/

        private function createStyle($style, $moduleName){
            if(defined('ASSETS_URL')){
                if(is_string($style)){
                    $style = (strrpos($style, ".") !== false) ? $style : $style.$this->extensions['styles'];

                    return "<link rel='stylesheet' href='".STYLES_URL.$moduleName.DS.$style."' media='screen'>";
                }
                else if(is_array($style) && isset($style['href'])){
                    $line = "<link ";
                    $line .= (isset($style['rel'])) ? " rel='".$style['rel']."'" : "rel='stylesheet'";
                    $line .= " href='".STYLES_URL.$moduleName.DS.$style['href'].$this->extensions['styles']."'";
                    $line .= (isset($style['media'])) ? " media=".$style['media'] : "media='screen'";
                    $line .= ">";

                    return $line;
                }
            }
            else{
                throw new \Exception("Cdn adresses are not set up.");
            }
        }

        private function createScript($script, $moduleName){
            if(defined('ASSETS_URL')){
                if(is_string($script)){
                    $script = (strrpos($script, ".") !== false) ? $script : $script.$this->extensions['scripts'];

                    return "<script src='".SCRIPTS_URL.$moduleName.DS.$script."'></script>";
                }
                else{
                    //to write array implementation
                    return "";
                }
            }
            else{
                throw new \Exception("Cdn adresses are not set up.");
            }
        }
    }