<?php
    namespace Fluid\Form;

    use  Fluid\Validate\Validate AS Val;

    class Validate extends Form{
        
        public $response = true;
        private $field = null;
        private $data = array();
        private $files = array();
        private $parent = null;
        private $modelSchema = array();
        private $current = null;
        private $column = null;
        private $element = null;
        private $elementName = null;
        protected $required = false;
        protected $empty = false;
        private $acceptedFiles = [
            'image' => ['jpg', 'bmp', 'png', 'gif'],
            'video' => ['mp4', 'avi', 'mpg', 'wma'],
        ];

        public function __construct($obj, $data, $files = array()) {
            $this->data = $data;
            $this->parent = $obj;
            $this->files = $files;
        }
        
        public function validateFields(){
            foreach($this->data AS $key => $element){
                foreach($this->data[$key] AS $k => $val){
                    $this->current = $val;
                    $this->element = $k;
                    $this->elementName = 'data['.$key.']['.$k.']';
                    $this->column = $key.ucfirst($k);
                    $this->field = $this->parent->fields[$this->column]->form->fields[$this->column];

                    $this->isRequired();

                    if(isset($this->field['validate'])){
                        if(is_array($this->field['validate'])){
                            $type = $this->field['validate'][0];
                        }
                        else{
                            $type = $this->field['validate'];
                        }

                        switch($type){
                            case "email":
                                $this->checkEmail();
                                break;
                            case "regex":
                                $regex = $this->field['validate'][1];
                                if(!empty($regex)) {
                                    $this->checkRegex($regex);
                                }
                                break;
                            case "numeric":
                                $this->isNumeric();
                                break;
                            case "boolean":
                                break;

                            case "file":
                                $this->checkAcceptedFiles();
                                break;
                        }
                    }

                    if(substr($this->element, mb_strlen($this->element)-2) != "_r"){
                        $this->parent->toReturn[$key][$this->element] = $val;
                    }
                    else{
                        $original = substr($this->element, 0, mb_strlen($this->element)-2);
                        $repeat = $this->element;
                            
                        if($this->data[$key][$original] !== $this->data[$key][$repeat]){
                            $this->throwError(6);
                        }
                    }
                }
            }

            return $this->response;
        }
        
        public function checkRegex($regex){
            $validate = Val::regex($this->current, $regex);

            if (!$validate) {
                $this->throwError(5);
            }
        }
        
        public function checkEmail(){
            $validate = Val::email($this->current);

            if (!$validate) {
                $this->throwError(2);
            }
        }
        
        public function isRequired(){
            if(isset($this->field['required'])){
                if($this->field['required'] == true){
                    $this->required = true;
                    $this->isEmpty();
                }
                else{
                    $this->required = false;
                }
            }
        }
        
        public function isEmpty(){
            if($this->required){
                if (!Val::notEmpty($this->current) || $this->current == "0") {
                    $this->empty = true;
                    $this->throwError(1);
                }
                else{
                    $this->empty = false;
                }
            }
        }

        public function checkAcceptedFiles(){
            $toAccept = $this->field['accept'];
            $correctExt = false;

            if(isset($toAccept) && !empty($toAccept) && !empty($this->current)){
                foreach($this->current AS $file){
                    $ext = pathinfo($file['name'], PATHINFO_EXTENSION);

                    foreach ($toAccept as $key => $val) {
                        if(isset($this->acceptedFiles[$key]) && in_array($ext, $this->acceptedFiles[$key])){
                            $correctExt = true;
                        }
                    }
                }
            }

            if(!$correctExt){
                $this->throwError(7);
            }
            else{

            }
        }

        public function isNumeric(){
            $validate = Val::digits($this->current);

            if (!$validate && $this->empty) {
                $this->throwError(3);
            }
        }
        
        private function throwError($number){
            $this->parent->queue[] = array(
                'message' => $this->getMessage($number),
                'element' => $this->element,
                'column' => $this->column,
                'elementName' => $this->elementName,
                'status' => '0'
            );
            $this->response = false;
        }
        
        private function getMessage($number){
            switch($number){
                case 1:
                    return "Pole musi byc wypełnione";
                case 2:
                    return "Niepoprawny adres email";
                case 3:
                    return "Wartość musi się skladać z samych cyfr";
                case 4:
                    return "Plik musi być załączony";
                case 5:
                    return "Niepoprawny format";
                case 6:
                    return "Pola się nie zgadzają.";
                case 7:
                    return "Niepoprawne rozszerzenie";
            }
        }
    }

