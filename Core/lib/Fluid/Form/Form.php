<?php
    namespace Fluid\Form;

    use Fluid\Fluid;
    use Fluid\Request;
    use Fluid\Session\Session;

    class Form {
        public $fields = array();
        public $fieldsSchema = array();
        public $blockedElements = false;
        protected $action = null;
        protected $method = null;
        protected $enctype = null;
        protected $formName = null;
        protected $fluid = null;
        protected $ajax = true;
        protected $backUrl = null;
        protected $models = array();
        protected $queue = array();
        protected $isBegin = false;
        protected $isEnd = false;
        protected $populateData = array();
        protected $response = array();
        protected $toReturn = array();
        protected $checkedData = array();
        private $Request;
        private $uploadedFiles = false;

        public function __construct($forms = [], $options = []) {
            $this->Request = Request::getInstance();

            if(!is_array($forms)){
                $forms = array($forms);
            }

            if(!empty($forms)){
                foreach($forms AS $form){
                    $this->forms[get_class($form)] = $form;

                    if(isset($form->fields)){
                        foreach($form->fields AS $key => $data){
                            $this->field($key, get_class($form));
                        }
                    }
                }   
            }

            $this->action = (isset($options['action'])) ? $options['action'] : null;
            $this->method = (isset($options['method'])) ? $options['method'] : "POST";
            $this->enctype = (isset($options['enctype'])) ? $options['enctype'] : "application/x-www-form-urlencoded";

            if(!empty($options)){
                if(!is_null($this->action)){
                    if(isset($options['fluid']) && !empty($options['fluid']) && is_array($options['fluid'])){
                        $fluidParam = '';
                        $fluid = $options['fluid'];
                        $element = "form";

                        if(isset($fluid['ajax'])){
                            $ajax = ($fluid['ajax'] === true) ? "true" : "false";
                            $this->ajax = $fluid['ajax'];
                        }
                        else{
                            $ajax = "true";
                            $this->ajax = true;
                        }

                        if(isset($fluid['submitButton'])){
                            $submitButton = $fluid['submitButton'];
                        }
                        else{
                            $submitButton = "false";
                        }

                        $fluidParam .= "element: ".$element.", ajax: ".$ajax.", submitButton: ".$submitButton;
                        $fluidParam .= '"';
                    }
                    else{
                        $fluidParam = 'element: form, ajax: true';
                        $this->ajax = true;
                    }

                    $this->fluid = $fluidParam;
                }
                else{
                    throw new \Exception("You must provide form action.");
                }
            }
        }

        public function begin($options = []){
            if(!$this->isBegin){
                $this->formName = (isset($options['id'])) ? $options['id'] : $this->getFormName();
                $this->isBegin = true;
                $form = '<form action="'.$this->action.'" method="'.$this->method.'" enctype="'.$this->enctype.'" id="'.$this->formName.'" data-fluid="'.$this->fluid.'"';

                if(!empty($options)){
                    foreach($options AS $key => $val){
                        $form .= ' '.$key.'="'.$val.'"';
                    }
                }

                $form .= ">";

                echo $form;
            }
        }

        public function end(){
            if(!$this->isEnd && $this->isBegin){
                $form = "";

                if(!empty($this->queue)){
                    foreach($this->queue AS $error){
                        $form .= "<div class='error-group'>".$error['message']."</div>";
                    }
                }

                $form .= "</form>";
                $this->isEnd = true;

                echo $form;
            }
        }

        public function field($columnName, $formName = ""){
            if(!in_array($columnName,$this->fields) && !empty($formName)){
                $options = array();

                if(isset($this->forms[$formName]->fields)){
                    $fields = $this->forms[$formName]->fields;

                    if(isset($fields[$columnName])){
                        $options = $fields[$columnName];
                        if(!empty($this->populateData)){
                            foreach($this->populateData AS $key => $data){
                                if(isset($this->populateData[$key][$formName])){
                                    foreach($this->populateData[$key][$formName] AS $keySecond => $dataSecond){
                                        if($formName.ucfirst($keySecond) == $columnName){
                                            $options['value'] = $dataSecond;
                                            break;
                                        }
                                    }
                                }
                                elseif(!is_numeric($key)){
                                    foreach($this->populateData[$key] AS $keySecond => $dataSecond){
                                        if($formName.ucfirst($keySecond) == $columnName){
                                            $options['value'] = $dataSecond;
                                            break;
                                        }
                                    }
                                }
                            }
                        }         
                    }
                }

                $options['name'] = $columnName;

                foreach($this->queue AS $index => $data){
                   foreach($data AS $key => $val){
                        if($key == "column" && $val == $columnName){
                            $options['validated'] = $data;
                            unset($this->queue[$index]);
                            break;
                        }
                    } 
                }

                $field = new Field($options, $this, $formName);
                $this->fields[$columnName] = $field;

                if(isset($field->model->schema[$columnName])){
                    $this->fieldsSchema[$columnName] = $field->model->schema[$columnName];
                }
               
                return $field;
            }
            else{
                return $this->fields[$columnName];
            }
        }
        
        public function submit($text = "Wyślij"){
            echo '<div class="form-group"><button class="button btn sendBtn submit pull-right">'.$text.'</button><div class="clear_float"></div></div>';
        }

        public function breakLine($text = ''){
            echo '<div class="form-group break-line"><span>'.$text.'</span></div>';
        }

        public function chceckRequest(){
            if($this->ajax){
                if(isset($_POST['formName'])){
                    $this->formName = $_POST['formName'];

                    if(isset($_FILES['file']) && isset($_POST['fileInput'])){
                        $forms = &Session::get('forms');
                        $file = $_FILES['file'];
                        $fileInput = $_POST['fileInput'];

                        $sessionForm = &$forms[$this->formName];
                        $file['tmp_name'] = $this->moveTempFile($file);
                        $sessionForm[$fileInput][] = $file;
                    }

                    if(isset($_POST['data'])){
                        $data = json_decode($_POST['data'], true);
                        $this->parseRequestKeys($data);
                    }
                }

                return $this->checkedData;
            }
            else{
                //to do !return $data ordinary;
            }
        }

        public function processRequest($data = array()){
            $data = (!empty($data)) ? $data : $this->checkedData;

            if(!empty($data)){
                $this->populateData = $data;
                $validator = new Validate($this, $data);
                $this->response = $validator->validateFields();

                if(!$this->response){
                    return false;
                }
                else{
                    $this->moveTempFiles();

                    $forms = &Session::get("forms");
                    $formIndex = array_search($this->formName, $forms);
                    unset($forms[$formIndex]);

                    return true;
                }
            }
        }

        public function displayResponse(){
            if(!empty($this->queue)){
                $this->queue['status'] = 0;
                return json_encode($this->queue);
            }
            else{
                $response = [
                    'status' => 1,
                    'backUrl' => $this->backUrl
                ];

                return json_encode($response);
            }
        }

        public function setBackUrl($url){
            $this->backUrl = $url;
        }

        public function addError($message = "Wystąpił błąd", $element = null){
            if($element){
                if($this->ajax){
                    $response = [
                        'status' => 0,
                        ['message' => $message]
                    ];

                    echo json_encode($response);
                    die();
                }
                else{
                    $this->queue[] = [
                        'status' => '0',
                        'message' => $message,
                        'elementName' => $element,
                        'column' => '.....'
                    ];
                }
            }
            else{

                if($this->ajax){
                    $response = [
                        'status' => 0,
                        [
                            'message' => $message,
                            'elementName' => 'form'
                        ],
                    ];

                    echo json_encode($response);
                    die();
                }
                else{
                    $this->queue[] = [
                        'status' => '0',
                        'message' => $message
                    ];
                }
            }
        }
        
        public function populateData($data){
            if(empty($this->populateData)){
                $this->populateData = $data;
            }  
        }
        
        public function getValues(){
            return $this->toReturn;
        }
        
        public function getField($fieldName){
            if(isset($this->fields[$fieldName])){
                return $this->fields[$fieldName];
            }
            return false;
        }

        private function getFormName(){
            $params = $this->Request->params;
            $name = $params['module']['name'].ucfirst($params['controller']).ucfirst($params['action'])."Form";

            $name = $this->checkFormName($name);

            return $name;
        }

        private function checkFormName($name,$index = 1){
            $forms = &Session::get("forms");

            //$forms = [];
            if(is_array($forms)){
                if(isset($forms[$name])){
                    if(strrpos($name, "_") !== false){
                        $name = substr($name, 0, strrpos($name, "_"));
                    }

                    $name .= "_".$index;
                    $index++;

                    return $this->checkFormName($name, $index);
                }
                else{
                    $forms[$name] = [];
                    return $name;
                }
            }
            else{
                $forms = [];
                $forms[$name] = [];

                return $name;
            }
        }

        private function parseRequestKeys($data){
            $forms = &Session::get('forms');
            $sessionForm = &$forms[$this->formName];

            if(is_array($data) && !empty($data)){
                foreach($data AS $key => $d){
                    if(strpos($key, "data") !== false){
                        if(isset($sessionForm[$key])){
                            $sessionForm[$key]['canSave'] = true;
                            $d = $sessionForm[$key];
                        }

                        $key = substr($key, 4);
                        $key = explode("[", $key);

                        if(isset($key[0]) && empty($key[0]) && isset($key[1]) && isset($key[2])){
                            $key[1] = substr($key[1], 0, mb_strlen($key[1])-1);
                            $key[2] = substr($key[2], 0, mb_strlen($key[2])-1);
                            $this->checkedData[$key[1]][$key[2]] = $d;
                        }
                    }
                }
            }
        }

        private function moveTempFiles(){
            $forms = &Session::get('forms');
            $sessionForm = &$forms[$this->formName];

            if(!empty($sessionForm)){
                foreach($sessionForm AS $input){
                    if(isset($input['canSave']) && $input['canSave'] === true){
                        foreach($input AS $file){
                            if($file['error'] === 0){
                                $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
                                $fileName = date("YmdHis").substr(hash("sha256", time()),0, 10).".".$ext;

                                rename(UPLOAD_DIR.'temp'.DS.$file['tmp_name'], UPLOAD_DIR.$fileName);
                            }
                        }
                    }
                }
            }
        }

        private function moveTempFile($file){
            if($file['error'] === 0){
                $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
                $fileName = hash("sha256", time()).".".$ext;

                if(move_uploaded_file($file['tmp_name'], UPLOAD_DIR.'temp'.DS.$fileName)){
                    return $fileName;
                }
            }
        }
    }