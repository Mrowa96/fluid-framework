<?php
    namespace Fluid\Form;

    class Field extends Form{
        
        public $formName = null;
        public $form = null;
        private $name = null;
        private $element = '';
        private $elementCol = '';
        private $elementClass = '';
        private $finalElement = '';
        private $label = '';
        private $labelText = '';
        private $labelCol = '';
        private $errors = array();
        private $errorTypes= array();
        private $errorTypeIndex = 0;
        private $wrapperStart = '';
        private $wrapperEnd = '';
        private $options = array();
        private $defaultEmpty = false;
        private $viewAs = 'custom';
        private $parent = null;
        
        public function __construct($options, $parent, $formName) {
            if(!empty($options)){
                isset($options[0]) ? $this->labelText = $options[0] : $this->labelText = '';
                isset($options['type']) ? $this->type = $options['type'] : $this->type = '';
                isset($options['value']) ? $this->value = $options['value'] : $this->value = '';
                isset($options['readonly']) ? $this->readonly = 'readonly' : $this->readonly = '';
                isset($options['readonly']) ? $this->readonly = 'readonly' : $this->readonly = '';
                $this->parent = $parent;
                if(isset($options['name'])){
                    $tmpName = $options['name'];
                    $tmpName2 = str_split($tmpName);
                        
                    for($p = mb_strlen($tmpName)-1; $p > 0; $p--){
                        if($tmpName2[$p] == strtoupper($tmpName2[$p]) && $tmpName2[$p] != "_" && !is_numeric($tmpName2[$p])){
                            $this->formName = trim(substr($tmpName, 0, $p));
                            $columnName = trim(strtolower(substr($tmpName, $p)));
                            break;
                        }
                    }

                    $this->form = $this->parent->forms[$formName];
                    $this->name = 'data['.$this->formName.']['.$columnName.']';
                    $this->columnName = $options['name'];
                }
                else{
                    throw new \Exception("No column selected");
                }
                
                if(isset($options['validated'])){
                    $this->errors = $options['validated'];
                }
                
                $this->errorTypes = array(
                    0 => array(
                        'class' => 'error',
                        'placeholder' => isset($this->errors['message']) ? $this->errors['message'] : ''
                    )
                );
                
                $this->wrapperStart = '<div class="form-group">';
                $this->wrapperEnd = '</div>';
                $this->elementCol = 'col-lg-10 col-md-10 col-sm-12 col-xs-12';
                $this->labelAttr = '';
                $this->labelCol = 'col-lg-2 col-md-2 col-sm-12 col-xs-12';
                $this->labelText = $this->labelText;
            }
        }
        
        public function image($attributes = array(), $values = array()){
            $this->setProperties($attributes);
            $this->setValues($values);

            $this->element = '<img src="'.$this->value.'" class="img-responsive">';
            
            $this->finalElement .= '<div class="'.$this->elementCol.'">'.$this->element.'</div>';
            return $this;
        }
        
        public function textarea($attributes = array(), $values = array()){
            $selected = '';
            $readonly = '';

            $this->setProperties($attributes);
            $this->setValues($values);

            if($this->readonly){
                $readonly = 'readonly';
            }
            $this->element .= '<textarea '.$readonly.' name="'.$this->name.'" class="'.$this->elementClass.'">'.$this->value.'</textarea>';
            
            $this->finalElement .= '<div class="'.$this->elementCol.'">'.$this->element.'</div>';
            return $this;         
        }
        
        public function tinymce($attributes = array(), $values = array()){
            $selected = '';
            $readonly = '';

            $this->setProperties($attributes);
            $this->setValues($values);

            if($this->readonly){
                $readonly = 'readonly';
            }
            $this->element .= '<textarea '.$readonly.' name="'.$this->name.'" class="'.$this->elementClass.' tinymce">'.$this->value.'</textarea>'
                    . '<div class="filemanager" data-filemanager="path: /upload, viewAs:modal, show: click .fileBtn, valueContainter: name='.$this->name.'"></div>';
            
            $this->finalElement .= '<div class="'.$this->elementCol.'">'.$this->element.'</div>';
            return $this;         
        }
        
        public function textInput($attributes = array(), $values = array()){
            $this->elementType = 'text';
            $this->input($attributes, $values);
            
            return $this;
        }
        
        public function numberInput($attributes = array(), $values = array()){
            $this->elementType = 'number';
            $this->input($attributes, $values);
            
            return $this;
        }
        
        public function fileInput($attributes = array(), $values = array()){
            $this->elementType = 'file';
            $this->input($attributes, $values);
            
            return $this;
        }
        
        public function dateInput($attributes = [], $values = []){
            $this->elementType = 'date';
            $this->input($attributes, $values);
            
            return $this;
        }
        
        public function dateTimeInput($attributes = [], $values = []){
            $this->elementType = 'datetime-local';
            $this->input($attributes, $values);
            
            return $this;
        }
        
        public function passwordInput($attributes = [], $values = []){
            $this->elementType = 'password';
            $this->input($attributes, $values);
            
            return $this;
        }
        
        public function checkbox($attributes = []){
            $this->elementType = 'checkbox';
            $this->elementCol = "col-lg-2 col-md-2 col-sm-2 col-xs-1";
            $this->labelCol = "col-lg-10 col-md-10 col-sm-10 col-xs-11";
            $this->input($attributes, $this->value);

            return $this;
        }
        
        public function radio($attributes = [], $values = []){
            $this->elementType = 'radio';
            $this->elementCol = "col-lg-2 col-md-2 col-sm-2 col-xs-2";
            $this->labelCol = "col-lg-10 col-md-10 col-sm-10 col-xs-11";
            $this->input($attributes, $values);

            return $this;
        }
        
        public function dropDownList($attributes = [], $values = []){
            $selected = '';
            $readonly = '';

            $this->setProperties($attributes);

            if($this->readonly){
                $readonly = 'readonly';
            }
            $this->element .= '<select '.$readonly.' name="'.$this->name.'" class="'.$this->elementClass.'">';
                
            if(!empty($values)){
                foreach($values AS $key => $val){
                    if($this->value == $key){
                        $selected = 'selected';
                    }
                    else{
                        $selected = '';
                    }
                    
                    if($this->defaultEmpty){
                        $extra = "selected hidden disabled";
                        $this->element .= '<option '.$extra.' value="">Wybierz</option>';
                        $this->defaultEmpty = false;
                    }
                    else{
                        $this->element .= '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
                    }
                }
            }
            elseif($this->type[0] == "array"){
                if(method_exists($this->model, $this->type[1])){
                    $arr = $this->model->{$this->type[1]}();
                }
                else{
                    $arr = $this->model->{$this->type[1]};
                }
                
                foreach($arr AS $key => $val){
                    if($this->value == $key){
                        $selected = 'selected';
                    }
                    else{
                        $selected = '';
                    }
                    $this->element .= '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
                }
            }
            
            $this->element .= '</select>';
            $this->finalElement .= '<div class="'.$this->elementCol.'">'.$this->element.'</div>';
            return  $this;
        }
        
        public function label($attributes = array()){
            $this->setLabelProperties($attributes);
            $this->finalElement .= '<label for="'.$this->name.'" class="'.$this->labelCol.' control-label">'.$this->labelText.'</label>';
            
            return $this;
        }
        
        public function required($isRequired = true){
            $this->parent->fieldsSchema[$this->columnName]['required'] = $isRequired;
            return $this;
        }
        
        public function render($clear = true){
            if($clear){
                $float = '<div class="clear_float"></div>';
            }
            else{
                $float = '';
            }
            echo $this->wrapperStart.$this->finalElement.$float.$this->wrapperEnd;
        }
        
        private function input($attributes, $values){
            $this->element .= '<input type="'.$this->elementType.'" ';
            
            $this->setProperties($attributes);
            $this->setValues($values);
            
            if($this->readonly){
                $this->element .= ' readonly ';
            }
            
            if(!empty($this->value) && !is_array($this->value)){
                $this->element .= 'value="'.$this->value.'"';
            }
            
            $this->element .= 'class="'.$this->elementClass.'"';
            $this->element .= 'name="'.$this->name.'">';
            
            if($this->elementType === "file"){
                $this->element = '<div class="'.$this->elementCol.'"><div class="file-control"><i class="fa fa-plus-circle"></i>'.$this->element.'</div></div>';
                $this->element .= '<div class="filesPreview '.$this->elementCol.'"></div>';
            }
            else if($this->elementType === "checkbox"){
                $fluidElement = '<div class="checkbox-control" data-name="'.$this->name.'"></div>';
                $this->element = '<div class="'.$this->elementCol.'">'.$this->element.$fluidElement.'</div>';
            }
            else if($this->elementType === "radio"){
                $this->finalElement = "";
                $this->element = "";

                foreach($values AS $value){
                    $this->element .= '<div class="radio">';
                        $this->element .= "<div class='".$this->elementCol."'>";
                            $this->element .= "<input type='".$this->elementType."' name='".$this->name."' class='".$this->elementClass."' value='".$value[0]."'>";
                            $this->element .= "<span class='radio-control' data-name='".$this->name."'></span>";
                        $this->element .= "</div>";
                        $this->element .= "<span class='text ".$this->labelCol."'>".$value[1]."</span>";
                        $this->element .= '<div class="clear_float"></div>';
                    $this->element .= "</div>";
                }
            }
            else{
                $this->element = '<div class="'.$this->elementCol.'">'.$this->element.'</div>';
            }

            $this->finalElement .= $this->element;
        }
        
        private function setProperties($attributes ){
            if(!empty($attributes)){
                foreach($attributes AS $key => $attr){
                    switch($key){
                        case "class":
                            $this->elementClass .= $attr." ";
                            break;
                        case "parentClass":
                            $this->wrapperStart = '<div class="'.$attr.'">';
                            break;
                        case "columns":
                            $this->elementCol = $attr;
                            break;
                        case "defaultEmpty":
                            $this->defaultEmpty = $attr;
                            break;
                        case "viewAs":
                            $this->viewAs = $attr;
                            break;
                        case "fileManager":
                            $this->addFileManager = $attr;
                            break;
                        default:
                            $this->element .= $key.'="'.trim($attr).'" ';
                            break;
                    }
                }
            }
            
            if(strpos($this->elementClass, 'form-control') === false){
                $this->elementClass .= "form-control";
            }
            
            if(!empty($this->errors)){
                foreach($this->errorTypes[$this->errorTypeIndex] AS $key => $val){
                    if($key == "class"){
                        $this->elementClass .= ' '.$val;
                    }
                    else{
                        $this->element .= $key.'="'.trim($val).'" ';
                    }
                    
                }
            }
        }
        private function setLabelProperties($attributes = array()){
            if(!empty($attributes)){

                foreach($attributes AS $key => $attr){
                    switch($key){
                        case "value":
                            $this->labelText = $attr;
                            break;
                        case "columns":
                            $this->labelCol = $attr;
                            break;
                        default:
                            $this->labelAttr .= ' '.$key.'='.$attr.' ';
                            break;
                    }
                }
            }
            
        }
        
        private function setValues($values){
            if(is_array($values)){
                foreach($values AS $key => $val){
                    if($this->value == $key){
                        $this->value = $val;
                    }
                }
            }
            else{
                $this->value = $values;
            }
        }
        
    }