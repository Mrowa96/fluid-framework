<?php
    namespace Fluid\Model;

    use Fluid\Fluid;
    use Fluid\Database;
    use Fluid\Lib;
    use Fluid\Log;

    class Model{

        public $started = false;
        public $External;
        protected $table = null;
        protected $modelName = null;
        protected $tableName = null;
        protected $relativeSort = [];
        protected $searchData = [];
        protected $searchAccuracy = 3;
        protected $Database;
        protected $Log;
        protected $connectionName;
        protected $connection;

        public function __construct(){
            $this->Database = Database::getInstance();
            $this->Log = new Log();

            $names = $this->Database->setNames(get_class($this));
            $this->modelName = $names['model'];
            $this->tableName = "ff_".$names['table'];

            if(method_exists($this, "begin")){
                $this->begin();
            }
        }

        public function fetchAll($wheres = [], $selects = [], $groupBy = [], $orderBy = [], $limit = 0){
            $this->prepareSql();
            return $this->after($this->Database->fetchAll($wheres, $selects, $groupBy, $orderBy, $limit));
        }

        public function fetchRow($wheres = [], $selects = []){
            $this->prepareSql();

            $value = $this->after([$this->Database->fetchRow($wheres, $selects)]);

            if(isset($value[0])){
                return $value[0];
            }
            else{
                return [];
            }
        }

        public function insert($inserts = [], $multiple = false){
            $this->prepareSql();
            return $this->Database->insert($inserts, $multiple);
        }

        public function update($wheres = [], $sets = []){
            $this->prepareSql();
            return $this->Database->update($wheres, $sets);
        }

        public function remove($wheres = []){
            $this->prepareSql();
            return $this->Database->remove($wheres);
        }

        public function describe($tableName = null, $includeModelName = false){
            $this->prepareSql();
            return $this->Database->describe($tableName, $includeModelName);
        }

        public function exists($wheres = []){
            $this->prepareSql();
            return $this->Database->exists($wheres);
        }

        public function fetchAllWithRelatives($wheres = [], $selects = [], $groupBy = [], $orderBy = [], $limit = 0){
            $this->prepareSql();
            $results = $this->Database->fetchAll($wheres, $selects, $groupBy, $orderBy, $limit);

            if($results){
                $results = $this->getRelativeData($results);
            }

            return $results;
        }

        public function prepareTableViewData($wheres = [], $sortData = [], $searchData = []){
            $columns = $this->describe($this->tableName, true);
            $results = [];
            $final = [];

            if(!empty($sortData)){
                if(isset($sortData['table']) && isset($sortData['column']) && isset($sortData['type'])){
                    if($this->modelName === $sortData['table']){
                        $results = $this->fetchAllWithRelatives($wheres,[],[],[[$sortData['column'], $sortData['type']]]);
                    }
                    else{
                        $this->relativeSort = $sortData;
                        $results = $this->fetchAllWithRelatives($wheres);
                    }

                    $final['sortData'] = $sortData;
                }
            }
            else{
                $results = $this->fetchAllWithRelatives($wheres);
            }

            if(!empty($searchData)){
                if(isset($searchData['phrase']) && isset($searchData['column']) && isset($searchData['table'])){
                    $this->searchData = $searchData;
                    $results = $this->search($results);
                }
            }

            if(property_exists($this, $this->modelName."Table") && property_exists($this->{$this->modelName."Table"}, "schema")) {
                $schema = $this->{$this->modelName."Table"}->schema;

                if(!empty($schema)) {
                    foreach($schema AS $key => $columnData) {
                        if(isset($columnData['name']) && !empty($columnData['name'])) {
                            $exploded = Lib::explodeOnUpper($key);

                            if(count($exploded) === 2){
                                $final['columns'][$key] = [
                                    'table' => $exploded[0],
                                    'column' => strtolower($exploded[1]),
                                    'key' => $columnData,
                                ];
                            }
                            else{
                                throw new \Exception("Something went wrong with schema key splitting.");
                            }

                            $final['data'][$key] = [];
                        }
                    }
                }
            }
            else{
                if(!empty($columns)){
                    foreach($columns AS $column){
                        $final['columns'][] = $column;
                        $final['data'][$column] = [];
                    }
                }
            }

            if(isset($final['columns']) && !empty($final['columns'])){
                foreach($final['columns'] AS $columnKey => $columnData){
                    if(!empty($results) ){
                        foreach($results AS $result){
                            if(is_array($result)){
                                foreach($result AS $modelName => $data){
                                    foreach($data AS $key => $value){
                                        $key = $modelName.ucfirst($key);
                                        $value = (!$value) ? null : $value;

                                        if($key === $columnKey || $key === $columnData){
                                            $final['data'][$key][] = $value;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $final['dataLength'] = count($results);

            return $final;
        }

        protected function after($data){
            return $data;
        }

        protected function before($data){
            return $data;
        }

        protected function addTempConnection(){
            $name = $this->connectionName;
            $config = $this->connection;

            if($this->Database === null){
                $this->Database = Database::getInstance();
            }

            $this->Database->addConnection($config, $name, true, true);
        }

        private function search($data){
            $table = $this->searchData['table'];
            $column = $this->searchData['column'];
            $phrase = $this->searchData['phrase'];
            $final = [];

            //to merge?

            if($table === "*" && $column === "*"){
                foreach($data AS $row){
                    $keys = array_keys($row);
                    $breakLoop = false;

                    if(!empty($keys)){
                        foreach($keys AS $key){
                            if($breakLoop === false){
                                foreach($row[$key] AS $index => $value){
                                    $similarity = levenshtein($value, $phrase);

                                    if($similarity <= $this->searchAccuracy){
                                        $final[] = $row;
                                        $breakLoop = true;
                                        break;
                                    }
                                }
                            }
                            else{
                                break;
                            }
                        }
                    }
                }
            }
            else{
                foreach($data AS $row){
                    $keys = array_keys($row);
                    $breakLoop = false;

                    if(!empty($keys)){
                        foreach($keys AS $key){
                            if($breakLoop === false && $table === $key){
                                foreach($row[$key] AS $index => $value){
                                    if($index === $column){
                                        $similarity = levenshtein($value, $phrase);

                                        if($similarity <= $this->searchAccuracy){
                                            $final[] = $row;
                                            $breakLoop = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            else{
                                break;
                            }
                        }
                    }
                }
            }

            return $final;
        }

        private function prepareSql (){
            if(empty($this->connection) && isset($this->connectionName)){
                $this->Database->setConnection($this->connectionName);
            }
            else if(!empty($this->connection) && is_string($this->connectionName)){
                $this->addTempConnection();
            }

            $this->Database->setTable(get_class($this));
        }

        private function getRelativeData($results){

            if(is_array($results) && !empty($results)){
                foreach($results AS &$result){
                    $modelName = array_keys($result)[0];

                    foreach($result[$modelName] AS $key => $value){
                        if(mb_strpos($key, "_id") !== false){
                            $normalFormModel = mb_strcut($key, 0, mb_strpos($key, "_id"));
                            $model = Lib::concatOnUpper($normalFormModel,  "_", true);
                            $relativeData = $this->{$model}->fetchRow([['id', '=', $value]]);

                            $result[$model] = $relativeData[$model];
                        }
                    }
                }

                if(!empty($this->relativeSort)){
                    if(isset($this->relativeSort['table']) && isset($this->relativeSort['column']) && isset($this->relativeSort['type'])){
                        usort($results, function($a, $b){
                            $table = $this->relativeSort['table'];
                            $column = $this->relativeSort['column'];
                            $type = $this->relativeSort['type'];

                            switch($type){
                                case "ASC":
                                    return strnatcmp($a[$table][$column], $b[$table][$column]);
                                    break;
                                case "DESC":
                                    return strnatcmp($b[$table][$column], $a[$table][$column]);
                                    break;
                            }
                        });
                    }
                }
            }

            return $results;
        }
    }