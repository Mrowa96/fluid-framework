<?php
    namespace Fluid\TableView;

    use Fluid\Request;

    class Paginator{

        private $actualRows;
        private $maxRows;
        private $currentPage;
        private $pagination;
        private $document;
        private $pages = [];
        private $Request;

        public function __construct($actualRows, $maxRows, $currentPage){
            $this->actualRows = $actualRows;
            $this->maxRows = $maxRows;
            $this->currentPage = $currentPage;

            $this->Request = Request::getInstance();

            $this->document = new \DOMDocument();
            $this->document->formatOutput = true;
            $this->document->preserveWhiteSpace = true;

            $this->preparePages();
            $this->generatePagination();
        }

        public function addPagination(\DOMDocument $document, \DOMElement $element){
            if(!empty($this->pagination)){
                $pagination = $document->importNode($this->pagination, true);

                return $element->appendChild($pagination);
            }
            else{
                throw new \Exception("Pagination canno't be created for unkown reason.");
            }
        }

        private function generatePagination(){
            $pagination = $this->document->createElement("div");
            $firstBtn = $this->document->createElement('a');
            $prevBtn = $this->document->createElement('a');
            $nextBtn = $this->document->createElement('a');
            $lastBtn = $this->document->createElement('a');

            $firstBtn->setAttribute("class","firstBtn fa fa-angle-double-left action");
            $prevBtn->setAttribute("class","prevBtn fa fa-angle-left action");
            $nextBtn->setAttribute("class","nextBtn fa fa-angle-right action");
            $lastBtn->setAttribute("class","lastBtn fa fa-angle-double-right action");
            $pagination->setAttribute("class","pagination");

            if($this->pages['first'] !== false){
                $firstBtn->setAttribute("href",$this->Request->addParam("page", $this->pages['first']));
                $pagination->appendChild($firstBtn);
            }

            if($this->pages['prev'] !== false){
                $prevBtn->setAttribute("href",$this->Request->addParam("page", $this->pages['prev']));
                $pagination->appendChild($prevBtn);
            }

            if($this->pages['next'] !== false){
                $nextBtn->setAttribute("href",$this->Request->addParam("page", $this->pages['next']));
                $pagination->appendChild($nextBtn);
            }

            if($this->pages['last'] !== false){
                $lastBtn->setAttribute("href",$this->Request->addParam("page", $this->pages['last']));
                $pagination->appendChild($lastBtn);
            }

            $this->pagination = $pagination;
        }

        private function preparePages(){
            if($this->currentPage > 0){
                $first = 1;
                $last = ceil($this->actualRows / $this->maxRows);

                if($last > 1 && $this->currentPage <= $last){
                    if($this->currentPage - 1 > 0){
                        $prev = $this->currentPage - 1;
                    }
                    else{
                        $prev = false;
                        $first = false;
                    }

                    if($this->currentPage + 1 <= $last){
                        $next = $this->currentPage + 1;
                    }
                    else{
                        $next = false;
                        $last = false;
                    }
                }
                else{
                    $first = false;
                    $prev = false;
                    $next = false;
                    $last = false;
                }

                $this->pages = [
                    'first' => $first,
                    'prev' => $prev,
                    'next' => $next,
                    'last' => $last
                ];
            }
            else{
                throw new \Exception("CurrentPage canno't be lower than 0.");
            }
        }
    }