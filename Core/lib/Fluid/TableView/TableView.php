<?php
    namespace Fluid\TableView;

    use Fluid\Log;
    use Fluid\Request;
    use Fluid\Router;

    class TableView{
        private $element;
        private $head;
        private $body;
        private $foot;
        private $mobile;
        private $rows = [];
        private $dataLength = 0;
        private $maxRows = 5;
        private $page = 1;
        private $allowSort = true;
        private $sortData = [];
        private $Log;
        private $Request;

        /*
         * Prop:
         * page : Int
         * rowsActions : Array
         * tableActions : Array
         * maxRows : Int
         * dataLength : Int
         *
         */
        public function __construct($data){
            $this->element = new \DOMDocument();
            $this->element->formatOutput = true;
            $this->element->preserveWhiteSpace = true;

            $this->Log = new Log();
            $this->Request = Request::getInstance();

            if(!empty($data)){
                $this->data = $data;

                if(isset($this->data['dataLength'])){
                    if(is_integer($this->data['dataLength'])){
                        $this->dataLength = $this->data['dataLength'];
                    }
                    else{
                        $this->dataLength = (int) $this->data['dataLength'];
                    }
                }

                if(isset($this->data['maxRows'])){
                    if(is_integer($this->data['maxRows'])){
                        $this->maxRows = $this->data['maxRows'];
                    }
                    else{
                        $this->maxRows = (int) $this->data['maxRows'];
                    }
                }

                if(isset($this->data['page'])){
                    if(is_integer($this->data['page'])){
                        $this->page = $this->data['page'];
                    }
                    else{
                        $this->page = (int) $this->data['page'];
                    }
                }

                if(isset($this->data['sortData'])){
                    if(is_array($this->data['sortData'])){
                        $this->sortData = $this->data['sortData'];
                    }
                }
            }

            $this->createTable();
            $this->addColumns();
            $this->addData();
            $this->addRowsActions();
            $this->addTableActions();
            $this->addPagination();
        }

        public function render(){
            echo $this->element->saveHTML();
        }

        private function createTable(){
            $tableWrap = $this->element->createElement('div');
            $table = $this->element->createElement('table');
            $tableMobile = $this->element->createElement('div');
            $thead = $this->element->createElement('thead');
            $tbody = $this->element->createElement('tbody');
            $tfoot = $this->element->createElement('div');

            $tableWrap->setAttribute("class", "tableWrap");
            $tableWrap->setAttribute("data-fluid", "element: tableView,");
            $table->setAttribute("class", "table tableStandard hidden-sm hidden-xs");
            $table->setAttribute("data-table-view-role", "tableStandard");
            $tfoot->setAttribute("class", "tableFoot");
            $tfoot->setAttribute("data-table-view-role", "tableFoot");
            $tableMobile->setAttribute("class", "tableMobile hidden-lg hidden-md");
            $tableMobile->setAttribute("data-table-view-role", "tableMobile");

            $this->head = $table->appendChild($thead);
            $this->body = $table->appendChild($tbody);
            $this->foot = $tfoot;
            $this->mobile = $tableMobile;

            $tableWrap->appendChild($table);
            $tableWrap->appendChild($tableMobile);
            $tableWrap->appendChild($tfoot);
            $this->element->appendChild($tableWrap);
        }

        private function addPagination(){
            if($this->maxRows > 0 && $this->dataLength > $this->maxRows){
                $paginator = new Paginator($this->dataLength, $this->maxRows, $this->page);
                $paginator->addPagination($this->element, $this->foot);
            }
        }

        private function addRowsActions(){
            foreach($this->rows AS $row) {
                if (isset($this->data['rowsActions']) && !empty($this->data['rowsActions'])){
                    $actions = $this->data['rowsActions'];

                    foreach($actions AS $action){
                        if(is_array($action) && count($action) > 0){
                            if(isset($action[0])){
                                $element = $action[0];

                                if(isset($action[1])){
                                    if(is_array($action[1])){
                                        if(isset($action[1]['route'])){
                                            print_r($this->data);
                                            $Router = Router::getInstance();
                                            $params = $Router->getRouteParams($action[1]['route']);
                                        }
                                    }
                                    else{
                                        $a = $this->element->createElement('a', $element);
                                        $td = $this->element->createElement('td');

                                        $a->setAttribute("href", $action[1]."/".$row['id']);
                                        $a->setAttribute("class", "link");
                                        $a->setAttribute("data-table-view-role", "action");

                                        $td->appendChild($a);
                                    }
                                }
                                else{
                                    $td = $this->element->createElement('td', $element);
                                }

                                if(isset($action[2])){
                                    $td->setAttribute("class",$action[2]);
                                }
                            }

                            $row['standard']->appendChild($td);
                        }
                    }
                }
            }
        }

        private function addTableActions(){
            if (isset($this->data['tableActions']) && !empty($this->data['tableActions'])){
                $actions = $this->data['tableActions'];
                $actionsBlock = $this->element->createElement("div");
                $actionsBlock->setAttribute("class","actions");

                foreach($actions AS $action){
                    if(is_array($action) && count($action) > 0){
                        if(isset($action[0])){
                            $element = $action[0];

                            if(isset($action[1])){
                                $a = $this->element->createElement('a', $element);
                                $a->setAttribute("href", $action[1]);
                                $a->setAttribute("class", "action");

                                if(isset($action[2])){
                                    $a->setAttribute("class",$action[2]." action");
                                    $a->setAttribute("data-action",$action[2]);
                                }

                                $actionsBlock->appendChild($a);
                            }
                        }
                    }
                }

                $this->foot->appendChild($actionsBlock);
            }
        }

        private function addColumns(){
            if(isset($this->data['columns']) && !empty($this->data['columns'])){
                $columnsStandard = $this->element->createElement('tr');
                $cellStandard = null;

                foreach($this->data['columns'] AS $column){
                    if(is_array($column) && isset($column['key']['name']) && (isset($column['key']['hidden']) && $column['key']['hidden'] === false)){
                        if($this->allowSort === true){
                            $cellStandard = $this->element->createElement('th');
                            $a = $this->element->createElement('a', $column['key']['name']);

                            if(isset($this->data['sortData']) && !empty($this->data['sortData'])){
                                if($column['table'] == $this->data['sortData']['table'] && $column['column'] == $this->data['sortData']['column']){
                                    switch($this->data['sortData']['type']){
                                        case "ASC":
                                            $sortType = "DESC";
                                            break;
                                        case "DESC":
                                            $sortType = "ASC";
                                            break;
                                        default:
                                            $sortType = "ASC";
                                            break;
                                    }
                                }
                                else{
                                    $sortType = "ASC";
                                }
                            }
                            else{
                                $sortType = "ASC";
                            }

                            $a->setAttribute("href",$this->Request->addParams([
                                'sortTable' => $column['table'],
                                'sortColumn' => $column['column'],
                                'sortType' => $sortType
                            ]));

                            $cellStandard->appendChild($a);
                        }
                        else{
                            $cellStandard = $this->element->createElement('th', $column['key']['name']);
                        }
                    }
                    else if(is_string($column)){
                        if($this->allowSort === true){
                            $cellStandard = $this->element->createElement('th');
                            $a = $this->element->createElement('a', $column);

                            $cellStandard->appendChild($a);
                        }
                        else{
                            $cellStandard = $this->element->createElement('th', $column);
                        }
                    }

                    if($cellStandard !== null){
                        $columnsStandard->appendChild($cellStandard);
                    }
                }

                $this->head->appendChild($columnsStandard);
            }
        }

        private function addColumnMobile($text){
            if(is_string($text) && !empty($text)){
                $column = $this->element->createElement("div",$text);

                $column->setAttribute("class","columnMobile");

                return $column;
            }

            return null;
        }

        private function addData(){
            if(isset($this->data['data']) && !empty($this->data['data'])){
                if(isset($this->data['columns']) && !empty($this->data['columns'])){
                    $data = $this->prepareData();
                    $length = $this->getPreparedDataLength($data);
                    $columns = $this->data['columns'];

                    if($this->checkDataHomogeneity($data) && $this->dataLength > 0){
                        for($i = 0; $i < $length; $i++){
                            $rowStandard = $this->element->createElement('tr');
                            $rowMobile = $this->element->createElement('div');
                            $rowId = 0;

                            $rowStandard->setAttribute("class", "tableRow");
                            $rowMobile->setAttribute("class","tableRow");

                            foreach($columns AS $column => $columnData){
                                $cellStandard = null;
                                $cellMobile = null;
                                $columnMobile = null;
                                $dataMobile = null;

                                if(is_array($columnData) && isset($columnData['key']['name'])){
                                    $cellMobile = $this->element->createElement("div");
                                    $columnMobile = $this->addColumnMobile($columnData['key']['name']);

                                    $cellMobile->setAttribute("class","cellMobile");

                                    if(isset($columnData['key']['primaryKey']) && $columnData['key']['primaryKey'] === true){
                                        $rowId = $data[$column][$i];
                                    }

                                    if(isset($columnData['key']['hidden']) && $columnData['key']['hidden'] === true){
                                        continue;
                                    }

                                    if(isset($columnData['key']['type'])){
                                        switch($columnData['key']['type']){
                                            case "boolean":
                                                if(!empty($data[$column][$i]) && ($data[$column][$i])){
                                                    $toInsert = "Tak";
                                                }
                                                else{
                                                    $toInsert = "Nie";
                                                }

                                                $cellStandard = $this->element->createElement('td', $toInsert);
                                                $dataMobile = $this->element->createElement('div', $toInsert);

                                                $dataMobile->setAttribute("class", "dataMobile");
                                                break;
                                        }
                                    }
                                    else{
                                        $cellStandard = $this->element->createElement('td', $data[$column][$i]);
                                        $dataMobile = $this->element->createElement('div', $data[$column][$i]);

                                        $dataMobile->setAttribute("class", "dataMobile");
                                    }

                                }
                                else if(is_string($columnData)){
                                    $cellStandard = $this->element->createElement('td', $data[$columnData][$i]);
                                    $dataMobile = $this->element->createElement('div', $data[$column][$i]);

                                    $dataMobile->setAttribute("class", "dataMobile");
                                }

                                (!is_null($cellMobile)) ? $cellMobile->appendChild($columnMobile) : null;
                                (!is_null($cellMobile)) ? $cellMobile->appendChild($dataMobile) : null;

                                (!is_null($cellStandard)) ? $rowStandard->appendChild($cellStandard) : null;
                                (!is_null($cellMobile)) ? $rowMobile->appendChild($cellMobile) : null;
                            }

                            if($rowId){
                                $rowStandard->setAttribute("data-id", $rowId);
                                $rowMobile->setAttribute("data-id", $rowId);
                            }

                            $this->rows[] = [
                                'standard' => $rowStandard,
                                'mobile' => $rowMobile,
                                'id' => $rowId
                            ];
                            $this->body->appendChild($rowStandard);
                            $this->mobile->appendChild($rowMobile);
                        }
                    }
                }
            }
        }

        private function getPreparedDataLength($data){
            if(!empty($data)){
                $keys = array_keys($data);

                if(isset($keys[0])){
                    return count($data[$keys[0]]);
                }
            }
        }

        private function prepareData(){
            $preparedData = [];

            if($this->maxRows < $this->dataLength){
                $firstIndex = ($this->page - 1) * $this->maxRows;
                $lastIndex = $firstIndex + $this->maxRows;

                foreach($this->data['data'] AS $column => $data){
                    foreach($this->data['data'][$column] AS $index => $value){
                        if($index >= $firstIndex && $index < $lastIndex){
                            $preparedData[$column][] = $value;
                        }

                    }
                }
            }
            else{
                $preparedData = $this->data['data'];
            }

            return $preparedData;
        }

        private function checkDataHomogeneity($data){
            $results = [];

            foreach($data AS $key => $value){
                if(is_array($value) && !empty($value)){
                    $results[] = count($value);
                }
            }

            if(!empty($results)){
                foreach($results AS $index => $result){
                    if($index >= 1){
                        if($results[$index - 1] !== $results[$index]){
                            throw new \Exception("Prepared data is corruped - length of all data columns are not equal.");
                        }
                    }
                }
            }

            return true;
        }
    }