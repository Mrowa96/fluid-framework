<?php
    namespace Fluid;

    use Fluid\Model\External;
    use Fluid\Model\Form;
    use Fluid\Model\Table;

    /**
     * Class Fluid
     * @package Fluid
     */
    class Fluid {
        /**
         * @var string
         */
        public $mode = "PRODUCTION";
        /**
         * @var array
         */
        public $modules = [];
        /**
         * @var null
         */
        public $Request = null;
        /**
         * @var null
         */
        public $Router = null;
        /**
         * @var null
         */
        public $Database = null;
        /**
         * @var null
         */
        public $Log = null;
        /**
         * @var null
         */
        public static $Lib = null;
        /**
         * @var null
         */
        public static $module = null;
        /**
         * @var null
         */
        public static $instance = null;

        /**
         * Fluid constructor.
         */
        public function __construct(){}

        /**
         * @return Fluid|null
         */
        public static function getInstance(){
            if(self::$instance === null) {
                self::$instance = new Fluid();
            }
            return self::$instance;
        }

        /**
         * @throws \Exception
         */
        public function start(){
            $this->Log = new Log();
            self::$Lib = new Lib();

            $this->prepareExceptions();
            $this->includeMethods();
            $this->Database = Database::getInstance();
            $this->prepareModules();
            $this->Request = Request::getInstance();
            $this->setModule();
            $this->Router = Router::getInstance();
            $this->goDeep();
        }

        /**
         * @param $mode
         */
        public function setMode($mode){
            switch(strtolower($mode)){
                case "development":
                    $this->mode = "DEVELOPMENT";
                    $this->setModeParams(E_ALL,1,1);

                    break;

                case "production":
                    $this->mode = "PRODUCTION";
                    $this->setModeParams();
                    break;

                default:
                    print_r("Cannot recognize mode. Setting to DEVELOPMENT \n");
                    $this->setMode("development");
                    break;
            }
        }

        /**
         * @throws \Exception
         */
        private function goDeep(){
            $params = $this->Request->params;
            $controllersDir = CONTROLLERS_DIR.MODULE_NAME.DS;

            if(file_exists($controllersDir)){
                $controllerName = '\\'.MODULE_NAMESPACE.'\\'.ucfirst($params['controller']).'Controller';
                $actionName = $params['action']."Action";
                $controller = new $controllerName();

                if(method_exists($controller, "begin")){
                    if(isset($controller->beginExclude) && !empty($controller->beginExclude)){

                        if(in_array($params['action'], $controller->beginExclude) === false){
                            $controller->begin();
                        }
                    }
                    else{
                        $controller->begin();
                    }
                }

                if(method_exists($controller, $actionName)){
                    $controller->{$actionName}();
                }
                else{
                    throw new \Exception("Controller ".$controllerName." hasn't got method ".$actionName);
                }

                if(method_exists($controller, "end")){
                    $controller->end();
                }

                $controller->postEnd();
            }
            else{
                throw new \Exception("Module ".$params['module']['name']." hasn't got controllers catalog inside.");
            }
        }

        /**
         * @param null $message
         */
        public function goToError($message = null){
            $controllerName = '\\App\\Client\\ErrorController';

            $controller = new $controllerName('default', 'Client');

            if(method_exists($controller, "indexAction")){
                $controller->indexAction($message);
            }
        }

        /**
         * @param null|string $timezone
         */
        public function setTimezone($timezone = "Europe/Warsaw"){
            date_default_timezone_set($timezone);
        }

        /**
         *
         */
        private function includeMethods(){
            if(file_exists(FLUID_DIR.DS.'Methods.php')){
                require_once FLUID_DIR.DS.'Methods.php';
            }
        }

        /**
         * @throws \Exception
         */
        private function prepareModules(){
            $modulesFromDb = $this->getModules();
            $modulesStatic = file_exists(CONFIG_DIR.'modulesStatic.php') ? require CONFIG_DIR.'modulesStatic.php' : null;

            if(!empty($modulesFromDb)){
                $modules = $modulesFromDb;

                if(!empty($modulesStatic)){
                    foreach($modulesStatic['Module'] AS $module){
                        $module['static'] = true;
                        $modules[]['Module'] = $module;
                    }
                }
            }
            else{
                throw new \Exception("Module database list cannot be empty.");
            }

            if(!empty($modules)){
                foreach($modules AS $module){
                    $module = $module['Module'];
                    $preparedModule = [];

                    if(is_array($module) && !empty($module)){
                        $preparedModule['name'] = $module['name'];
                        $preparedModule['namespace'] = isset($module['namespace']) ? $module['namespace'] : ucfirst($module['name']);
                        $preparedModule['url'] = isset($module['url']) ? $module['url'] : "/".$module['name'];
                        $preparedModule['config'] = (file_exists(CONFIG_DIR.$preparedModule['name'].DS.'config.php')) ? require_once CONFIG_DIR.$preparedModule['name'].DS.'config.php' : [];
                        $preparedModule['url_suffix'] = (isset($preparedModule['config']['suffix'])) ? $preparedModule['config']['suffix'] : ".html";
                    }

                    if($this->checkModuleIntegration($preparedModule)){
                        $this->modules[] = $preparedModule;
                    }
                }
            }
        }

        /**
         * @return mixed
         * @throws \Exception
         */
        private function getModules(){
            if(!is_null($this->Database)){
                $this->Database->setTable("ModuleModel");
                return $this->Database->fetchAll([['active', '=', '1']]);
            }
            else{
                throw new \Exception("Cannot download modules list from database");
            }
        }

        /**
         * @param $module
         * @return bool
         * @throws \Exception
         */
        private function checkModuleIntegration($module){
            if(!file_exists(CONTROLLERS_DIR.$module['name'])){
                throw new \Exception("No controllers catalog for module - ".$module['name']);
            }

            if(!file_exists(MODELS_DIR.$module['name'])){
                throw new \Exception("No models catalog for module - ".$module['name']);
            }

            if(!file_exists(VIEWS_DIR.$module['name'])){
                throw new \Exception("No views catalog for module - ".$module['name']);
            }

            if(!file_exists(CONFIG_DIR.$module['name'])){
                throw new \Exception("No config catalog for module - ".$module['name']);
            }

            return true;
        }

        /**
         *
         */
        private function setModule(){
            $module = $this->Request->params['module'];

            if(isset($module) && !empty($module)){
                define("MODULE_NAME", $module['name']);
                define("MODULE_URL", $module['url']);
                define("MODULE_NAMESPACE", 'App\\'.$module['namespace']);
                define("MODULE_URL_SUFFIX", $module['url_suffix']);

                self::$module = $module;
            }
        }

        /**
         * @param int $errorReportng
         * @param int $startupErrors
         * @param int $errors
         */
        private function setModeParams($errorReportng = 0, $startupErrors = 0, $errors = 0){
            error_reporting($errorReportng);
            ini_set('display_startup_errors', $startupErrors);
            ini_set('display_errors', $errors);
        }

        /**
         *
         */
        private function prepareExceptions(){
            if($this->mode === "PRODUCTION"){
                set_exception_handler('exceptionHandlerProduction');
            }
        }
    }