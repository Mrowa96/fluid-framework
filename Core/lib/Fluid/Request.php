<?php
    namespace Fluid;

    class Request{

        public $url = null;
        public $baseUrl = null;
        public $query = null;
        public $host = null;
        public $suffix = null;
        public $params = [];
        public $custom = [];
        public static $instance = null;
        private $superArrays = ['post', 'get'];

        public function __construct(){
            $this->url = filter_input(INPUT_SERVER, "REQUEST_URI");
            $this->host = filter_input(INPUT_SERVER, "HTTP_HOST");

            $questionPos = strpos($this->url, "?");

            if($questionPos !== false){
                $this->baseUrl = substr($this->url, 1, $questionPos-1);
                $this->query = substr($this->url, $questionPos+1);
            }
            else {
                $this->baseUrl = substr($this->url, 1);
            }

            $modules = Fluid::getInstance()->modules;
            $parts = explode("/", $this->baseUrl);

            foreach($parts AS &$part){
                $part = $this->checkSuffix($part);
            }

            $moduleDetected = false;

            if(!empty($parts)){
                if(isset($parts[0])){
                    if(!empty($modules)){
                        foreach($modules AS $module){
                            if(strpos($module['url'], "/") === 0){
                                $module['url'] = substr($module['url'], 1);
                            }

                            if($module['url'] == $parts[0]){
                                $this->params['module'] = $module;
                                $moduleDetected = true;
                                break;
                            }
                        }
                    }
                    else{
                        throw new \Exception("No modules provided.");
                    }

                    switch($moduleDetected){
                        case true:
                            $controller = (isset($parts[1]) && !empty($parts[1])) ? $this->checkSuffix($parts[1]) : "index";
                            $controllerName = ucfirst($controller).'Controller.php';
                            $action = (isset($parts[2]) && !empty($parts[2])) ? $this->checkSuffix($parts[2]) : "index";

                            if(!file_exists(CONTROLLERS_DIR.$this->params['module']['name'].DS.$controllerName)){
                                $action = $controller;
                                $controller = "index";
                            }

                            $this->params['controller'] = $controller;
                            $this->params['action'] = $action;

                            break;

                        case false:
                            $currentModule = null;

                            foreach($modules AS $module){
                                if($module['url'] === "/"){
                                    $currentModule = $module;
                                    break;
                                }
                            }

                            if(is_null($currentModule)){
                                throw new \Exception("Don't provide module for default request - /.");
                            }
                            else{
                                $module = (isset($currentModule) && !empty($currentModule)) ? $currentModule : null;
                                $controller = (isset($parts[0]) && !empty($parts[0])) ? $this->checkSuffix($parts[0]) : "index";
                                $controllerName = ucfirst($controller).'Controller.php';
                                $action = (isset($parts[1]) && !empty($parts[1])) ? $this->checkSuffix($parts[1]) : "index";

                                if(!file_exists(CONTROLLERS_DIR.$module['name'].DS.$controllerName)){
                                    $action = $controller;
                                    $controller = "index";
                                }

                                $this->params['module'] = $module;
                                $this->params['controller'] = $controller;
                                $this->params['action'] = $action;
                            }
                            break;
                    }
                }
            }
        }

        public static function getInstance(){
            if(self::$instance === null) {
                self::$instance = new Request();
            }

            return self::$instance;
        }

        public function setParam($key, $value, $array = "get"){
            $this->custom[$array][$key] = $value;
        }

        public function addParam($key, $value, $urlToChange = null){
            if(is_null($urlToChange)){
                $url = $this->url;
            }
            else{
                $url = $urlToChange;
            }

            if(mb_strpos($url, "?") !== false){
                if(mb_strpos($url, "?") !== mb_strlen($url) - 1){
                    if(mb_strpos($url, $key."=") !== false){
                        $keyPos = mb_strpos($url, $key."=");
                        $keyLength = mb_strlen($key) + 2;

                        if(mb_strpos($url, "&", $keyPos+$keyLength) !== false){
                            $paramPosEnd = mb_strpos($url, "&", $keyPos+$keyLength);
                            $param = mb_substr($url, $keyPos, $paramPosEnd - $keyPos);

                            $url = str_replace($param,$key."=".$value, $url);
                        }
                        else{
                            $paramPosEnd = mb_strlen($url);
                            $param = mb_substr($url, $keyPos, $paramPosEnd - $keyPos);

                            $url = str_replace($param,$key."=".$value, $url);
                        }
                    }
                    else{
                        $url .= "&".$key."=".$value;
                    }
                }
                else{
                    $url .= $key."=".$value;
                }
            }
            else{
                $url .= "?".$key."=".$value;
            }

            return $url;
        }

        public function addParams($params){
            if(!empty($params)){
                $url = $this->url;

                foreach($params AS $key => $param){
                    $url = $this->addParam($key, $param, $url);
                }

                return $url;
            }
            else{
                throw new \Exception("Params cannot be empty.");
            }
        }

        public function getParams($superArrays = []){
            return $this->parseInput($superArrays);
        }

        public function getParam($param, $superArrays = []){
            $response = [];

            if(is_string($param) && !empty($param)){
                $response = $this->parseInput($superArrays, [$param], "array");
            }

            return $response[$param];
        }

        public function hasParams($params, $superArrays = []){
            $response = [];

            if(is_array($params) && !empty($params)){
                $response = $this->parseInput($superArrays, $params);
            }

            return $response;
        }

        public function hasParam($param, $superArrays = []){
            $response = [];

            if(is_string($param) && !empty($param)){
                $response = $this->parseInput($superArrays, [$param]);
            }

            return $response;
        }

        public function isPost(){
            if(!empty($_POST)){
                return true;
            }
            else{
                return false;
            }
        }

        private function parseInput($super, $searched = [], $returnType = "bool", $customArrays = false){
            $response = [];

            $super = is_string($super) ? [$super] : $super;
            $super = empty($super) ? $this->superArrays : $super;
            $searched = is_string($searched) ? [$searched] : $searched;

            if($customArrays === true){
                foreach($searched AS $search){
                    foreach($this->custom AS $array){
                        foreach($array AS $key => $val){
                            if(!empty($key) && !empty($val)){
                                if($search === $key){
                                    $result = $array[$key];
                                    $result = $this->checkSuffix($result);

                                    $response[$key] = $result;
                                }
                            }
                        }
                    }
                }
            }
            else if(!empty($super) && is_array($super)){
                foreach($super AS $superArray){
                    switch($superArray){
                        case "post":
                            $array = $_POST;
                            $filter = INPUT_POST;
                            break;
                        case "get":
                            $array = $_GET;
                            $filter = INPUT_GET;
                            break;
                        case "files":
                            $array = $_FILES;
                            $filter = INPUT_REQUEST;
                            break;
                    }

                    if(!empty($array) && !empty(($searched))){
                        foreach($searched AS $search){
                            foreach($array AS $key => $val){
                                if(!empty($key) && !empty($val)){
                                    if($search === $key){
                                        $result = filter_input($filter, $search, FILTER_SANITIZE_STRING);

                                        if($result !== null && $result !== false){
                                            $result = $this->checkSuffix($result);

                                            $response[$key] = $result;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(!empty($searched)){
                if($returnType === "bool"){
                    if(count($searched) === count($response)){
                        return true;
                    }
                    else if($customArrays === false){
                        return $this->parseInput($super, $searched, $returnType, true);
                    }
                    else{
                        return false;
                    }
                }
                else if($returnType === "array"){
                    if(isset($response) && !empty($response)){
                        return $response;
                    }
                    else if($customArrays === false){
                        return $this->parseInput($super, $searched, $returnType, true);
                    }
                    else{
                        return null;
                    }
                }
            }
            else{
                return $response;
            }
        }

        private function checkSuffix($string){
            if(is_string($string) && !empty($string)){
                if(strrpos($string, ".") !== false){
                    $string = substr($string, 0, strrpos($string, "."));
                }
            }

            return $string;
        }
    }