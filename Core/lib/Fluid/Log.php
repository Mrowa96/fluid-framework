<?php
    namespace Fluid;

    class Log{

        private $logsPath = null;
        private $extension = ".log";

        public function __construct($path = null){
            $this->logsPath = (!empty($path)) ? $path : I_LOG_DIR;
        }

        public function d($message, $clear = false, $trace = false){
            return $this->log($message, $clear, $trace, FLUID_LOG_DEBUG);
        }

        public function i($message, $clear = false, $trace = false){
            return $this->log($message, $clear, $trace, FLUID_LOG_INFO);
        }

        public function w($message, $clear = false, $trace = false){
            return $this->log($message, $clear, $trace, FLUID_LOG_WARNING);
        }

        public function e($message, $clear = false, $trace = false){
            return $this->log($message, $clear, $trace, FLUID_LOG_ERROR);
        }

        public function c($message){
            echo "<script>console.log('".$message."')</script>";
            return true;
        }

        private function log($message, $clear, $logTrace, $logLevel){
            $currentDatetime = date('Y-m-d');
            $catalogPath = $this->logsPath.DS.$currentDatetime;

            if(!file_exists($catalogPath)){
                mkdir($catalogPath, 0755);
            }

            switch($logLevel){
                case 0:
                    $fileName = "DEBUG".$this->extension;
                    break;
                case 1:
                    $fileName = "INFO".$this->extension;
                    break;
                case 2:
                    $fileName = "WARNING".$this->extension;
                    break;
                case 3:
                    $fileName = "ERROR".$this->extension;
                    break;
            }

            if(isset($fileName)){
                $time = date("H:i:s");
                $message = $time.": \t".$message;
                $flag = ($clear === true) ? null : FILE_APPEND;

                if($logTrace === true){
                    $trace = debug_backtrace();

                    if(isset($trace[1]) && isset($trace[1]['file'])){
                        $message .= "\t (Possible caller: ".$trace[1]['file'].")";
                    }
                }

                $message .= PHP_EOL;

                if(file_put_contents($catalogPath.DS.$fileName, $message, $flag)){
                    return true;
                }
                else{
                    return false;
                }
            }
        }
    }