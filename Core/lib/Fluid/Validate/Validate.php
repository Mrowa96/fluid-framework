<?php
    namespace Fluid\Validate;

    class Validate{

        public static function email($value){
            return filter_var($value, FILTER_VALIDATE_EMAIL);
        }

        public static function regex($value, $regex){
            return preg_match($regex, $value);
        }

        public static function notEmpty($value){
            if(!empty($value)){
                return true;
            }
            else{
                return false;
            }
        }

        public static function digits($value){
            return is_numeric($value);
        }
    }