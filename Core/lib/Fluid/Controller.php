<?php
    namespace Fluid;

    use Fluid\Auth\Auth;
    use Fluid\View\View;

    class Controller{

        public $External;
        public $Request;
        protected $View;
        protected $Log;
        protected $Auth;

        public function __construct($moduleName = MODULE_NAME, $moduleNamespace = MODULE_NAMESPACE){
            define('MODULE_VIEWS_DIR', VIEWS_DIR.$moduleName);
            define('MODULE_MODELS_DIR', MODELS_DIR.$moduleName);
            define('MODULE_CONTROLLERS_DIR', CONTROLLERS_DIR.$moduleName);
            define('MODULE_LAYOUTS_DIR', MODULE_VIEWS_DIR.DS.'layouts');
            define('MODULE_PARTIALS_DIR', MODULE_VIEWS_DIR.DS.'partials');

            if(file_exists(PLUGINS_DIR.DS.$moduleName)){
                define('MODULE_PLUGINS_DIR', PLUGINS_DIR.DS.$moduleName);
            }

            if(defined('MODULE_PLUGINS_DIR') && file_exists(MODULE_PLUGINS_DIR.DS.'ViewPlugin.php')){
                $viewClass = '\\'.$moduleNamespace.'\\'.'ViewPlugin';
                $this->View = new $viewClass();
            }
            else {
                $this->View = new View();
            }

            $this->Request = Request::getInstance();
            $this->Log = new Log();
            $this->Auth = new Auth();
        }

        public function postEnd(){
            $this->View->renderLayout();
        }

        public function redirect($url){
            //to rewrite with handle params
            header("Location: ".$url);
        }

        protected function setLayout($layout = "default"){
            $this->View->layout = $layout;
        }
    }