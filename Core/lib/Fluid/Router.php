<?php
    namespace Fluid;

    class Router{

        public static $instance;
        private $routers = [];
        private $params = [];
        private $query = "";
        private $Log;
        private $Request;

        public function __construct(){
            if(file_exists(CONFIG_DIR.MODULE_NAME.DS."routers.php")){
                $this->routers = require CONFIG_DIR.MODULE_NAME.DS."routers.php";

                $this->Log = new Log();
                $this->Request = Request::getInstance();
                $this->query = $this->Request->query;
                $this->params = $this->Request->params;

                $this->prepareRoute();
            }
        }

        public static function getInstance(){
            if(self::$instance === null) {
                self::$instance = new Router();
            }

            return self::$instance;
        }

        public function getRouteByName($name){
            if(!empty($this->routers)) {
                foreach ($this->routers AS $key => $route) {
                    if($key === $name){
                        return $route;
                    }
                }
            }

            return false;
        }

        public function getRouteParams($name){
            $route = $this->getRouteByName($name);
            $routeEx = "http://".filter_input(INPUT_SERVER, "HTTP_HOST")."/".MODULE_URL."/".$route[0].MODULE_URL_SUFFIX;
            $params = [];

            preg_match_all('/([\{\}])/', $routeEx, $paramsMatches, PREG_OFFSET_CAPTURE);

            if(count($paramsMatches)) {
                $paramsMatches = $paramsMatches[0];

                for ($i = 0; $i < count($paramsMatches); $i += 2) {
                    $start = $paramsMatches[$i][1];
                    $end = $paramsMatches[$i + 1][1];

                    $params[] = mb_substr($routeEx, $start + 1, $end - $start - 1);
                }
            }

            return $params;
        }

        private function prepareRoute(){
            if(!empty($this->routers)){
                foreach($this->routers AS $route){
                    if(isset($route[1])){
                        if(is_array($route[1])){
                            $routeEx = "http://".filter_input(INPUT_SERVER, "HTTP_HOST")."/".MODULE_URL."/".$route[0].MODULE_URL_SUFFIX;
                            $params = [];

                            preg_match_all('/([\{\}])/', $routeEx, $paramsMatches, PREG_OFFSET_CAPTURE);
                            if(count($paramsMatches)){
                                $paramsMatches = $paramsMatches[0];
                                $regEx = $routeEx;

                                for($i = 0; $i < count($paramsMatches); $i += 2){
                                    $start = $paramsMatches[$i][1];
                                    $end = $paramsMatches[$i + 1][1];

                                    $toReplace = mb_substr($routeEx, $start + 1, $end - $start - 1);
                                    $regEx = str_replace("{".$toReplace."}", '([\w\d]*)', $regEx);

                                    $params[] = $toReplace;
                                }

                                $regEx = $this->addslashes($regEx);

                                if(preg_match('/'.$regEx.'/', $this->getFullUrl($_SERVER), $matchedValues)){
                                    if(count($matchedValues)){

                                        foreach($matchedValues AS $key => $value){
                                            if($key > 0){
                                                $this->Request->setParam($params[$key - 1], $value);
                                            }
                                        }
                                    }

                                    $routeData = $route[1];

                                    $this->Request->params['controller'] = $routeData['controller'];
                                    $this->Request->params['action'] = $routeData['action'];

                                    break;
                                }
                            }
                        }
                        else if(is_string($route[1])){
                            $postRoute = $route[1];

                            if(strpos($route[0], "/") !== false){
                                $route = substr($route[0], 1);
                            }

                            $route = explode("/", $route);

                            if(!empty($route)){
                                $action = 'index';

                                if($route[0] === MODULE_URL){
                                    $controller = Lib::concatOnUpper($route[1],"-");

                                    if(isset($route[2])){
                                        $action = Lib::concatOnUpper($route[2],"-");
                                    }
                                }
                                else{
                                    $controller = Lib::concatOnUpper($route[0],"-");

                                    if(isset($route[1])){
                                        $action = Lib::concatOnUpper($route[1],"-");
                                    }
                                }

                                $this->route($controller, $action, $postRoute);
                            }
                        }
                    }
                }
            }
        }

        private function route($controller, $action, $route){
            if(empty($controller)) {
                $controller = "([a-zA-Z0-9])";
            }

            if($this->checkRegExp($controller, $this->params['controller'])){
                if(empty($action)) {
                    $action = "([a-zA-Z0-9])";
                }

                if($this->checkRegExp($action, $this->params['action'])){
                    $query = (!empty($this->query)) ? "?".$this->query : "";

                    header("Location: " . $route . $query);
                    exit;
                }
            }
        }

        private function urlOrgin( $s, $use_forwarded_host = false ){
            $ssl      = ( ! empty( $s['HTTPS'] ) && $s['HTTPS'] == 'on' );
            $sp       = strtolower( $s['SERVER_PROTOCOL'] );
            $protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );
            $port     = $s['SERVER_PORT'];
            $port     = ( ( ! $ssl && $port=='80' ) || ( $ssl && $port=='443' ) ) ? '' : ':'.$port;
            $host     = ( $use_forwarded_host && isset( $s['HTTP_X_FORWARDED_HOST'] ) ) ? $s['HTTP_X_FORWARDED_HOST'] : ( isset( $s['HTTP_HOST'] ) ? $s['HTTP_HOST'] : null );
            $host     = isset( $host ) ? $host : $s['SERVER_NAME'] . $port;

            return $protocol . '://' . $host;
        }

        private function getFullUrl( $s, $use_forwarded_host = false ){
            return $this->urlOrgin( $s, $use_forwarded_host ) . $s['REQUEST_URI'];
        }

        private function checkRegExp($pattern, $subject){
            if (strpos($pattern, "(") === 0 && strpos($pattern, ")") === mb_strlen($pattern) - 1) {
                $pattern = substr($pattern, 1, mb_strlen($pattern) - 2);

                if (preg_match("/" . $pattern . "/", $subject)) {
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                if($pattern === $subject){
                    return true;
                }
                else{
                    return false;
                }
            }
        }

        private function addslashes($string){
            $string = str_replace(["/", ".", ":"], ['\/', '\.', '\:'], $string);

            return $string;
        }
    }