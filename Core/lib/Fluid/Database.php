<?php
    namespace Fluid;

    use PDO;

    class Database{

        public static $instance = null;
        public $model = null;
        protected $sql = "";
        protected $table = null;
        private $modelArrayIndex = null;
        private $connections = [];
        private $connection = null;
        private $prevConnection = null;
        private $reservedWords = ['Group'];
        private $Log;

        public function __construct(){
            $this->Log = new Log();
            $configs = (array) require_once I_CF_DIR.'database.php';

            if(!empty($configs)){
                foreach($configs AS $configName => $config){
                    $connect = (count($configs) === 1) ? true : false;
                    $this->addConnection($config, $configName, $connect, false);
                }
            }
        }

        public function fetchAll($wheres = [], $selects = [], $groupBy = [], $orderBy = [], $limit = 0, $relatives = false){
            $this->sql = "SELECT ";
            $this->modelArrayIndex = $this->model;

            if(!empty($this->table) && is_string($this->table)){
                if(!empty($selects) && is_array($selects)){
                    foreach($selects AS $key => $select){
                        $this->sql .= $this->model.".`".$select."` AS '".$this->model.".".$select."'";

                        if($key < count($selects)-1){
                            $this->sql .= ", ";
                        }
                    }
                }
                else{
                    $columns = $this->describe();

                    if(!empty($columns)){
                        foreach($columns AS $key => $column){

                            if(in_array($this->model, $this->reservedWords)){
                                $this->model .= "R";
                            }

                            $this->sql .= $this->model.".`".$column."` AS '".$this->modelArrayIndex.".".$column."'";

                            if($key < count($columns)-1){
                                $this->sql .= ", ";
                            }
                        }
                    }
                    else{
                        $this->sql .= "* ";
                    }
                }

                $this->sql .= $this->getFrom();
                $this->sql .= $this->setWhere($wheres);

                if(!empty($groupBy) && is_array($groupBy)){
                    $this->sql .= "GROUP BY ";

                    foreach ($groupBy as $iterator => $group) {
                        if(is_string($group)){
                            $this->sql .= $this->model.".`".$group."`";
                        }
                        if($iterator+1 < count($groupBy)){
                            $this->sql .= ", ";
                        }
                    }
                }

                if(!empty($orderBy) && is_array($orderBy)){
                    $this->sql .= "ORDER BY ";

                    foreach ($orderBy as $iterator => $order) {
                        if(is_array($order) && count($order) === 2){
                            $this->sql .= $this->model.".`".$order[0]."` ".$order[1]." ";
                        }
                        if($iterator+1 < count($orderBy)){
                            $this->sql .= "AND ";
                        }
                    }
                }

                if(is_string((string) $limit) && !empty($limit)){
                    $this->sql .= "LIMIT ".$limit;
                }
            }

            $statement  = $this->connection['pdo']->prepare($this->sql);

            if($statement->execute()){
                $result  = $statement->fetchAll(\PDO::FETCH_ASSOC);
                $results = $this->bindToArray($result);
                $this->removeTempConnection();

                return $results;
            }
            else{
                throw new \Exception(print_r($statement->errorInfo(), true));
            }
        }

        public function fetchRow($wheres = [], $selects = []){
            $result = $this->fetchAll($wheres, $selects, [], [], 1);

            if(isset($result[0])){
                return $result[0];
            }
            else{
                return [];
            }
        }

        public function insert($inserts, $multiple = false){
            if(!empty($inserts)){
                if($multiple === false){
                    $inserts = [$inserts];
                }

                foreach($inserts AS $insert){
                    $this->sql = "INSERT INTO ";
                    $this->sql .= $this->getFullTableName()." ";

                    $columns = "(";
                    $values = " VALUES(";

                    foreach($insert AS $column => $data){
                        $columns .= "`".$column."`, ";
                        $values .= "'".$data."', ";
                    }

                    $columns = substr($columns, 0, mb_strlen($columns)-2).") ";
                    $values = substr($values, 0, mb_strlen($values)-2).") ";

                    $this->sql .= $columns.$values;
                    $statement  = $this->connection['pdo']->prepare($this->sql);
                    if(!$statement->execute()){
                        throw new \Exception(print_r($statement->errorInfo(), true));
                    }
                }

                $lastId = $this->connection['pdo']->lastInsertId();
                $this->removeTempConnection();

                return $lastId;
            }
            else{
                return false;
            }
        }

        public function update($wheres = [], $sets = []){
            $this->sql = "UPDATE ";
            $this->sql .= $this->getFullTableName()." ";

            if(!empty($sets)){
                $this->sql .= "SET ";

                foreach($sets AS $column => $data){
                    $this->sql .= "`".$column."`='".$data."', ";
                }

                $this->sql = substr($this->sql, 0, mb_strlen($this->sql)-2)." ";
            }

            $this->sql .= $this->setWhere($wheres, false);
            $statement  = $this->connection['pdo']->prepare($this->sql);

            if($statement->execute()){
                return true;
            }
            else{
                throw new \Exception(print_r($statement->errorInfo(), true));
            }
        }

        public function remove($wheres = []){
            $this->sql = "DELETE ";
            $this->sql .= $this->getFrom(true);
            $this->sql .= $this->setWhere($wheres);

            $statement  = $this->connection['pdo']->prepare($this->sql);

            if($statement->execute()){
                return true;
            }
            else{
                throw new \Exception(print_r($statement->errorInfo(), true));
            }
        }

        public function describe($tableName = null, $includeModelName = false){
            if(is_null($tableName)){
                $tableName = $this->getFullTableName();
            }

            $statement = $this->connection['pdo']->prepare("DESCRIBE ".$tableName);
            $statement->execute();
            $columns = $statement->fetchAll(\PDO::FETCH_COLUMN);

            if($includeModelName && !empty($columns)){
                foreach($columns AS $key => $column){
                    $columns[$key] = $this->model.ucfirst($column);
                }
            }

            return $columns;
        }

        public function exists($wheres = []){
            $result = $this->fetchRow($wheres);

            if(!empty($result)){
                return true;
            }
            else{
                return false;
            }
        }

        public function setTable($modelName){
            $names = $this->setNames($modelName);

            if(isset($names['model']) && isset($names['table'])){
                $this->model = $names['model'];
                $this->table = $names['table'];
            }
            else{
                throw new \Exception("FLuid cannot specify model or table name.");
            }
        }

        public function setNames($modelName){
            $prefix = "";
            $names = [];
            $model = [];

            $modelParts = explode("\\", $modelName);

            foreach($modelParts AS $key => $parts){
                if($key == count($modelParts)-1){
                    $model = $parts;
                }
                else{
                    if(strtolower($parts) !== "app"){
                        $prefix .= strtolower($parts)."_";
                    }
                }
            }

            if(strpos($model, "ExternalModel") !== false){
                $table = substr($model, 0 ,strpos($model, "ExternalModel"));
                $names['model'] = $table;
                $names['table'] = $prefix.lcfirst(Lib::concatOnUpper($table));
            }
            else if(strpos($model, "Model") !== false){
                $table = substr($model, 0 ,strpos($model, "Model"));
                $names['model'] = $table;
                $names['table'] = $prefix.lcfirst(Lib::concatOnUpper($table));
            }
            else if(strpos($model, "Form") !== false){
                return false;
            }
            else if(strpos($model, "Table") !== false){
                return false;
            }

            if(isset($names['model']) && isset($names['table'])){
                return $names;
            }
            else{
                throw new \Exception("Fluid cannot specify model or table name - ".$modelName);
            }
        }

        public function addConnection($config, $connectionName, $connect = false, $temp = false){
            if(is_array($config) && isset($config['pdoType'])){
                $connection = [];

                switch($config['pdoType']){
                    case "mysql":
                        $connection['pdo'] = new \PDO('mysql:host='.$config['host'].';dbname='.$config['name'].';charset=utf8;port='.$config['port'].'', $config['login'], $config['password']);
                        $connection['prefix'] = isset($config['prefix']) ? $config['prefix'] : null;
                        $connection['suffix'] = isset($config['suffix']) ? $config['suffix'] : null;
                        $connect = isset($config['default']) ? true : $connect;

                        break;
                }

                if(!empty($connection)){
                    $connection['temp'] = ($temp === true) ? true : false;
                    $this->connections[$connectionName] = $connection;

                    if($connect === true){
                        if($temp){
                            $this->setTempConnection($connectionName);
                        }
                        else{
                            $this->setConnection($connectionName);
                        }
                    }
                }
            }
        }

        public function setConnection($connectionName){
            if(array_key_exists($connectionName, $this->connections)){
                $this->connection = $this->connections[$connectionName];
            }
        }

        public function setTempConnection($connectionName){
            foreach($this->connections AS $connection){
                if(isset($connection['temp']) && $connection['temp'] === true){
                    $this->prevConnection = $this->connection;
                    $this->setConnection($connectionName);
                }
            }
        }

        public static function getInstance(){
            if(self::$instance === null) {
                self::$instance = new Database();
            }

            return self::$instance;
        }

        protected function bindToArray($data){
            $result = [];

            foreach($data AS $d){
                foreach($d AS $key => $val){
                    $newKey = explode(".", $key);
                    $row[$newKey[0]][$newKey[1]] = $val;
                }

                if(isset($row)){
                    $result[] = $row;
                }
            }

            return $result;
        }

        private function getFrom($deleteSyntax = false){
            if($deleteSyntax){
                return "FROM `".$this->model."` USING `".$this->getFullTableName()."` AS `".$this->model."` ";
            }
            else{
                return "FROM `".$this->getFullTableName()."` AS `".$this->model."` ";
            }
        }

        private function getFullTableName(){
            return $this->connection['prefix'].$this->table.$this->connection['suffix'];
        }

        private function setWhere($wheres, $addAlias = true){
            $sql = "";

            if(!empty($wheres) && is_array($wheres)){
                $sql .= "WHERE ";

                foreach ($wheres as $iterator => $where) {
                    if(is_array($where) && count($where) === 3){
                        if($addAlias){
                            $sql .= $this->model.".`".$where[0]."`".$where[1]."'".$where[2]."' ";
                        }
                        else{
                            $sql .= "`".$where[0]."`".$where[1]."'".$where[2]."' ";
                        }
                    }
                    if($iterator+1 < count($wheres)){
                        $sql .= "AND ";
                    }
                }
            }

            return $sql;
        }

        private function removeTempConnection(){
            if(isset($this->connection['temp']) && $this->connection['temp'] === true && !empty($this->prevConnection)){
                $this->connection = $this->prevConnection;
                $this->prevConnection = null;
            }
        }
    }