<?php
    namespace Fluid;

    class Folder{

        public $name;
        public $size;
        public $sizeUnit;
        private $folder;
        private $tempPath;

        public function __construct($folder){
            if(file_exists($folder) && is_dir($folder)){
                $info = pathinfo($folder);
                $sizeInfo = Lib::humanBytesFormat($this->getSize($folder));

                $this->folder = $folder;
                $this->name = $info['filename'];
                $this->size = $sizeInfo['size'];
                $this->sizeUnit = $sizeInfo['sizeUnit'];
            }
            else{
                throw new \Exception("Folder ".$folder." doesn't exist in given path");
            }
        }

        public function remove($evenIsNotEmpty = false){
            $files = array_diff(scandir($this->folder), array('.','..'));

            if(empty($this->tempPath)){
                $this->tempPath = $this->folder;
            }

            if(!empty($files) && $evenIsNotEmpty === true){
                foreach ($files as $file) {
                    if(is_dir($this->tempPath.DS.$file)){
                        $this->tempPath .= DS.$file;

                        $this->remove($evenIsNotEmpty);
                    }
                    else{
                        unlink($this->tempPath.DS.$file);
                    }
                }
            }

            return rmdir($this->tempPath);
        }

        private function getSize($path, $size = 0){
            $elements = array_diff(scandir($path), array('.','..'));

            if(!empty($elements)){
                foreach($elements AS $element){
                    if(is_dir($path.DS.$element)){
                        return $this->getSize($path.DS.$element, $size);
                    }
                    else if(is_file($path.DS.$element)){
                        $fileSize = filesize($path.DS.$element);

                        if($fileSize){
                            $size += $fileSize;
                        }
                    }
                }
            }

            return $size;
        }
    }