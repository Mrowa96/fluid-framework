<?php
    namespace Fluid\Auth;

    use App\LoginForm;
    use App\RegisterForm;
    use App\UserModel;
    use Fluid\Form\Form;
    use Fluid\Session\Session;

    class Controller extends \Fluid\Controller{

        protected $formRegister;
        protected $formLogin;
        private $User;

        public function __construct(){
            parent::__construct();

            $this->User = new UserModel();
            $this->formRegister = new Form(new RegisterForm(), ['action' => $this->View->url("register")]);
            $this->formLogin = new Form(new LoginForm(), ['action' => $this->View->url("login")]);

            $this->View->formRegister = $this->formRegister;
            $this->View->formLogin = $this->formLogin;
        }

        public function loginAction(){
            $form = $this->formLogin;

            if($this->Request->hasParam('formName')){
                $this->setLayout("empty");
                $form->setBackUrl($this->View->url("home"));
                $data = $form->chceckRequest();

                if($form->processRequest($data)){
                    $this->Auth->login($form->getValues());

                    if($this->Auth->failed()){
                        $form->addError($this->Auth->error());
                    }
                }

                echo $form->displayResponse();
            }

            exit;
        }

        public function registerAction(){
            $form = $this->formRegister;

            if($this->Request->hasParam('formName')){
                $this->setLayout("empty");
                $form->setBackUrl($this->View->url("home"));
                $data = $form->chceckRequest();

                if($form->processRequest($data)){
                    $this->Auth->register($form->getValues());

                    if($this->Auth->failed()) {
                        $form->addError($this->Auth->error());
                    }
                }

                echo $form->displayResponse();
            }

            exit;
        }

        public function logoutAction(){
            $this->Auth->logout();

            $this->redirect($this->View->url("home"));
        }
    }