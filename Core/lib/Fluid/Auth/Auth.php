<?php
    namespace Fluid\Auth;

    use App\UserModel;
    use Fluid\Session\Session;

    class Auth{

        public $name = "";
        public $email = "";
        public $id = 0;
        private $userData = [];
        private $logged = false;
        private $fail = false;
        private $error = null;
        private $namespace = "App\\Authorize";
        private $User;

        public function __construct(){
            $this->User = new UserModel();
            $userSession = Session::space($this->namespace);

            if(isset($userSession->logged) && $userSession->logged === true){
                $this->userData = $userSession->user['User'];
                $this->name = $this->userData['login'];
                $this->email = $this->userData['email'];
                $this->id = $this->userData['id'];
                $this->logged = true;
            }
        }

        public function isLogged(){
            return $this->logged;
        }

        public function login($data){
            if(isset($data['User']['login']) && isset($data['User']['password'])){
                $login = $data['User']['login'];
                $password = hash("sha256", $data['User']['password']);

                if($this->User->exists([['login', '=', $login]])){
                    $userData = $this->User->fetchRow(
                        [
                            ['login', '=', $login],
                            ['password', '=', $password]
                        ],
                        ['id','login','email']
                    );

                    if($userData){
                        $userSession = Session::space($this->namespace);
                        $userSession->logged = true;
                        $userSession->user = $userData;
                    }
                    else{
                        $this->setError(0);
                    }
                }
                else{
                    $this->setError(3);
                }
            }
        }

        public function register($data){
            if(isset($data['User']['password']) && isset($data['User']['password_confirm'])){
                $password = $data['User']['password'];
                $passwordC = $data['User']['password_confirm'];

                if($password === $passwordC){
                    if(isset($data['User']['login']) && isset($data['User']['email'])){
                        unset($passwordC);
                        $login = $data['User']['login'];
                        $email = $data['User']['email'];

                        if($this->User->exists([['login', '=', $login]]) === false){
                            $user = $this->User->insert([
                                'login' => $login,
                                'email' => $email,
                                'password' => hash("sha256", $password),
                            ]);

                            if($user){
                                $this->login($data);
                            }
                        }
                        else{
                            $this->setError(5);
                        }
                    }
                    else{
                        $this->setError(2);
                    }
                }
                else{
                    $this->setError(1);
                }
            }
            else{
                $this->setError(4);
            }
        }

        public function logout(){
            Session::destroySpace($this->namespace);
        }

        public function failed(){
            return $this->fail;
        }

        public function error(){
            return $this->error;
        }

        private function setError($index){
            $errors = [
                0 => "Podane hasło nie jest poprawne.",
                1 => "Podane hasła się nie zgadzają.",
                2 => "Nie podano wszystkich danych.",
                3 => "Nie znaleziono użytkownika o podanej nazwie.",
                4 => "Wystąpił nieznany błąd.",
                5 => "Ten login jest już zajęty.",
            ];

            $this->fail = true;
            $this->error = $errors[$index];
        }
    }