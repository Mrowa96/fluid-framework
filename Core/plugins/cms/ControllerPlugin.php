<?php
    namespace App\Cms;

    use Fluid;
    use Fluid\Session\Session;

    class ControllerPlugin extends Fluid\Controller{

        public $page = 1;
        public $sortData = [];
        public $searchData = [];
        private $categoryName;

        public function __construct(){
            parent::__construct();

            if(!Session::isStarted()){
                Session::start();
            }

            $cmsAuthorize = Session::space("Cms\\Authorize");

            if(!$cmsAuthorize->is("logged")){
                if($this->Request->params['action'] !== "login"){
                    $this->redirect("/manage/login");
                }
            }
            else{
                $this->View->userData = $cmsAuthorize->user['Users'];
                $this->View->menuData = $this->Categories->getForMenu();
            }

            if(!empty($this->categoryName)){
                $this->View->categoryName = $this->categoryName;
            }

            $this->getPageParam();
            $this->getSortParams();
            $this->getSearchParams();
        }

        private function getSearchParams(){
            if($this->Request->getParam("searchPhrase")){
                $searchPhrase = $this->Request->getParam("searchPhrase");
            }
            if($this->Request->getParam("searchColumn")){
                $searchColumn = $this->Request->getParam("searchColumn");
            }
            else{
                $searchColumn = "*";
            }
            if($this->Request->getParam("searchTable")){
                $searchTable = $this->Request->getParam("searchTable");
            }
            else{
                $searchTable = "*";
            }

            if(isset($searchPhrase) && isset($searchColumn) && isset($searchTable)){
                $this->searchData = [
                    'table' => $searchTable,
                    'column' => $searchColumn,
                    'phrase' => $searchPhrase,
                ];
            }
        }

        private function getPageParam(){
            if($this->Request->getParam("page")){
                $this->page = $this->Request->getParam("page");
            }
        }

        private function getSortParams(){
            if($this->Request->getParam("sortTable")){
                $sortTable = $this->Request->getParam("sortTable");
            }
            if($this->Request->getParam("sortColumn")){
                $sortColumn = $this->Request->getParam("sortColumn");
            }
            if($this->Request->getParam("sortType")){
                $sortType = $this->Request->getParam("sortType");
            }

            if(isset($sortTable) && isset($sortColumn) && isset($sortType)){
                $this->sortData = [
                    'table' => $sortTable,
                    'column' => $sortColumn,
                    'type' => $sortType,
                ];
            }
        }
    }