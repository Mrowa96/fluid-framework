<?php
    namespace App\Cms;

    class BubblePlugin{

        private $element = [];
        private $bubble  = null;

        public function __construct($element){
            $this->element = $element;

            if(!empty($this->element)){
                $this->processData();
            }
        }

        private function processData(){
            $element = $this->element;

            $color = $element['Categories']['css_color'];
            $size = "size".$element['Categories']['css_size'];

            $bubbleWrapO = "<span class='bubble ".$size."' style='background-color: ".$color.";'>";
            $linkO = "<a href='".$element['Categories']['url']."'>";
            $icon = "<span class='icon fa ".$element['Categories']['css_icon']."'></span>";
            //$title = "<span class='icon fa ".$element['Categories']['name']."'></span>";
            $linkE = "</a>";
            $bubbleWrapE = "</span>";

            $this->bubble = $bubbleWrapO.$linkO.$icon.$linkE.$bubbleWrapE;
        }

        public function render(){
            echo $this->bubble;
        }
    }