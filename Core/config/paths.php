<?php
    define("DS", "/");

    define("ROOT_DIR", getcwd().DS);
    define("CORE_DIR", ROOT_DIR."..".DS."..".DS."Core".DS);
    define("ASSETS_DIR", ROOT_DIR."..".DS."..".DS."Assets".DS);

    define("CONFIG_DIR", CORE_DIR."config".DS);
    define("LIB_DIR", CORE_DIR."lib".DS);
    define("CONTROLLERS_DIR", CORE_DIR."controllers".DS);
    define("MODELS_DIR", CORE_DIR."models".DS);
    define("VIEWS_DIR", CORE_DIR."views".DS);
    define("PLUGINS_DIR", CORE_DIR."plugins".DS);
    define("CONFIGS_DIR", CORE_DIR."configs".DS);
    define("FLUID_DIR", LIB_DIR."Fluid".DS);

    define("I_LOG_DIR", ROOT_DIR."log".DS);
    define("I_CF_DIR", ROOT_DIR."config".DS);
    define("I_DATA_DIR", ROOT_DIR."data".DS);
    define("I_UPLOAD_DIR", I_DATA_DIR."upload".DS);

    define("LOADER", FLUID_DIR."Loader.php");