<?php
    return [
        'Module' => [
            [
                'name' => 'default',
                'namespace' => 'Client',
                'url' => '/'
            ],
            [
                'name' => 'cms',
                'url' => '/manage'
            ]
        ]
    ];