<?php
    return [
        'extensions' => [
            'styles' => '.css',
            'scripts' => '.js',
            'views' => '.phtml'
        ],
        'assets' => [
            'styles' => [
                [
                    'href' => 'app',
                    'media' => 'screen'
                ]
            ],
            'scripts' => [
                'Cms'
            ]
        ]
    ];