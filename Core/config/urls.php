<?php
    define("ASSETS_URL", "http://ff-assets.localhost");

    define("STYLES_URL", ASSETS_URL."/styles/");
    define("IMAGES_URL", ASSETS_URL."/images/");
    define("SCRIPTS_URL", ASSETS_URL."/scripts/");