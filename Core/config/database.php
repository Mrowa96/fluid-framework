<?php
    return [
        'system' => [
            'pdoType' => 'mysql',
            'host' => 'localhost',
            'login' => 'root',
            'password' => 'root',
            'name' => 'fluid-system',
            'port' => '3306',
            'prefix' => 'fs_'
        ]
    ];
