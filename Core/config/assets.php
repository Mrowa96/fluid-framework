<?php
    //global assets - will be included on EVERY site in each module
    return [
        'styles' => [
            ['href' => 'font-awesome.min'],
            ['href' => 'Bootstrap/bootstrap.min']
        ],
        'scripts' => [
            'Fluid'
        ]
    ];