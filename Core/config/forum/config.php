<?php
    return [
        'assets' => [
            'styles' => [
                [
                    'href' => 'app',
                    'media' => 'screen'
                ]
            ],
            'scripts' => [
                'Forum'
            ]
        ],
        'suffix' => ".html"
    ];