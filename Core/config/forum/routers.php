<?php
    return [
        'group' => ['grupa/{groupUrl},{groupId}', ['controller' => 'Group', 'action' => 'index']],
        'topic' => ['temat/{topicUrl},{topicId}', ['controller' => 'Topic', 'action' => 'index']]
    ];