<?php
    namespace App\Cms;

    use Fluid;
    use Fluid\Session\Session;
    use Fluid\Form\Form;

    class IndexController extends ControllerPlugin {

        public function indexAction(){
            $this->setLayout("index");
           // $this->View->categories = $this->Category->getForIndex();
        }

        public function loginAction(){
          /*  $form = new Form($this->Form->User, ['action' => '/manage/login']);

            if($this->Request->hasParam('formName')){
                $this->setLayout("empty");
                $form->setBackUrl("/manage");
                $data = $form->chceckRequest();

                if($form->processRequest($data)){
                    $values = $form->getValues();
                    $login = $values['Users']['login'];
                    $password = $values['Users']['password'];
                    $userData = $this->User->login($login, $password);

                    if($userData){
                        if(!Session::isStarted()){
                            Session::start();
                        }

                        $cmsAuthorize = Session::space("Cms\\Authorize");
                        $cmsAuthorize->logged = true;
                        $cmsAuthorize->user = $userData;
                    }
                    else{
                        $form->addError("Niepoprawne dane lub użytkownik o takim loginie nie istnieje.");
                    }
                }

                echo $form->displayResponse();
            }
            else{
                $this->setLayout("authorization");
                $cmsAuthorize = Session::space("Cms\\Authorize");

                if($cmsAuthorize->is('logged')){
                    $this->redirect('/manage');
                }
                else{
                    $this->View->form = $form;
                }
            }*/
        }

        public function logoutAction(){
            $cmsAuthorize = Session::space("Cms\\Authorize");

            if($cmsAuthorize->is('logged') && $cmsAuthorize->is('user')){
                unset($cmsAuthorize->logged);
                unset($cmsAuthorize->user);
                $this->redirect('/manage/login');
            }
        }
    }