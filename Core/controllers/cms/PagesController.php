<?php
    namespace App\Cms;

    use Fluid\TableView\TableView;

    class PagesController extends ControllerPlugin{

        public function indexAction(){
            $data = $this->Pages->prepareTableViewData($this->sortData, $this->searchData);
            $data['page'] = $this->page;;
            $data['rowsActions'] = [['Edytuj', '/manage/pages/edit', 'edit'], ['Usuń', '/manage/pages/delete', 'delete']];
            $data['tableActions'] = [['Dodaj nowy element', '/manage/pages/add', 'add'], ['Usuń wszystkie', '/manage/pages/dellete-all', 'deleteAll']];

            $table = new TableView($data);

            $this->View->table = $table;
        }

        public function searchAction(){

        }
    }