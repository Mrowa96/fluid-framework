<?php
    namespace App\Cms;

    use Fluid\File;
    use Fluid\Folder;

    class FilesController extends ControllerPlugin{

        public $beginExclude = ['index','download', 'add'];
        private $relativePath = null;
        private $path = I_DATA_DIR;
        private $data;

        public function begin(){
            if($this->Request->isPost()){
                if($this->Request->hasParam('data')){
                    $this->data = json_decode($this->Request->getParam('data'), true);
                }
                else{
                    exit;
                }
            }
            else{
                exit;
            }
        }

        public function indexAction(){}
        public function addAction(){
            $params = json_decode(filter_input(INPUT_POST, "extraData"), true);
            $fileData = $_FILES['file'];

            $fileData['path'] = $this->path.$params['dir'].DS.$fileData['name'];
            $file = new File($fileData, true);

            echo json_encode([
                'failed' => false
            ]);
            exit;
        }
        public function contentAction(){
            if(isset($this->data['path']) && !empty($this->data['path'])) {

                $this->data['path'] = $this->handleLastChar($this->data['path']);

                $this->relativePath = $this->data['path'];
                $this->path .= $this->data['path'];

                if (!empty($this->relativePath) && !empty($this->path)) {
                    $results = scandir($this->path);
                    $folders = [];
                    $files = [];
                    $final = [];

                    foreach ($results AS $result) {
                        if ($result !== "." && $result !== '..') {
                            if (is_dir($this->path . $result)) {
                                $folder = new Folder($this->path . $result);

                                $folderData = [
                                    'name' => $folder->name,
                                    'dir' => $this->relativePath,
                                    'path' => $this->relativePath . DS . $folder->name,
                                    'size' => $folder->size,
                                    'sizeUnit' => $folder->sizeUnit,
                                    'type' => 'folder'
                                ];

                                $files[] = $folderData;
                            }
                            else if (is_file($this->path . $result)) {
                                $file = new File($this->path . $result);

                                $fileData = [
                                    'name' => $file->name,
                                    'ext' => $file->ext,
                                    'dir' => $this->relativePath,
                                    'path' => $this->relativePath . DS . $file->name . "." . $file->ext,
                                    'category' => $file->type,
                                    'size' => $file->size,
                                    'sizeUnit' => $file->sizeUnit,
                                    'type' => 'file'
                                ];

                                if ($fileData['category'] === "image") {
                                    //to do resizing
                                    $fileData['thumb'] = $fileData['path'];
                                }

                                $folders[] = $fileData;
                            }
                        }
                    }

                    $final = array_merge($files, $folders);

                    echo json_encode($final);
                }
            }

            exit;
        }
        public function renameAction(){
            if(isset($this->data['newName']) && !empty($this->data['newName'])){
                $path = $this->path.$this->handleLastChar($this->data['dir']);

                if(rename($path.$this->data['oldName'], $path.$this->data['newName'])){
                    $response = [
                        'failed' => false
                    ];
                }
                else{
                    $response = [
                        'failed' => true,
                        'message' => 'Cannot rename, because unknown error.'
                    ];
                }
            }

            echo json_encode($response);
            exit;
        }
        public function deleteAction(){
            $response = ['failed' => true,];

            if (!empty($this->data)) {
                foreach ($this->data AS $path) {
                    if (isset($path['dir']) && isset($path['name'])) {
                        $finalPath = $this->path.$path['dir'].$path['name'];

                        if(is_dir($finalPath)){
                            $folder = new Folder($finalPath);
                            $folder->remove(true);

                            $response = [
                                'failed' => false
                            ];
                        }
                        else if(is_file($finalPath)){
                            if(unlink($finalPath)){
                                $response = [
                                    'failed' => false
                                ];
                            }
                            else{
                                $response = [
                                    'failed' => true,
                                    'message' => 'Cannot rename, bacause unknown error.'
                                ];
                            }
                        }
                    }
                }
            }

            echo json_encode($response);
            exit;
        }
        public function downloadAction(){
            if($this->Request->hasParam("path")){
                $file = $this->path.$this->Request->getParam("path");

                if (file_exists($file)) {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="'.basename($file).'"');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($file));
                    readfile($file);
                    exit;
                }
            }
        }
        public function newFolderAction(){
            $response = ['failed' => true];

            if(isset($this->data['name']) && !empty($this->data['name']) && isset($this->data['dir'])){
                $path = $this->path.$this->data['dir'].DS.$this->data['name'];
                if(mkdir($path)){
                    $response = [
                        'failed' => false
                    ];
                }
            }

            echo json_encode($response);
            exit;
        }
        public function copyAction(){
            $response = ['failed' => true];

            if(isset($this->data['paths']) && !empty($this->data['paths'])) {
                $placeToPaste = $this->data['placeToPaste'];
                $paths = $this->data['paths'];

                if(!empty($paths)){
                    foreach($paths AS $path){
                        if(isset($path['dir']) && isset($path['name'])){
                            $oldPath = $this->path.$path['dir'].$path['name'];
                            $newPath = $this->path.$placeToPaste.DS.$path['name'];

                            if(copy($oldPath,$newPath)){
                                $response = [
                                    'failed' => false
                                ];
                            }
                        }
                    }
                }
            }

            echo json_encode($response);
            exit;
        }
        public function moveAction(){
            $response = ['failed' => true];

            if(isset($this->data['paths']) && !empty($this->data['paths'])) {
                $placeToPaste = $this->data['placeToPaste'];
                $paths = $this->data['paths'];

                if(!empty($paths)){
                    foreach($paths AS $path){
                        if(isset($path['dir']) && isset($path['name'])){
                            $oldPath = $this->path.$path['dir'].$path['name'];
                            $newPath = $this->path.$placeToPaste.DS.$path['name'];

                            if(rename($oldPath,$newPath)){
                                $response = [
                                    'failed' => false
                                ];
                            }
                        }
                    }
                }
            }

            echo json_encode($response);
            exit;
        }

        private function handleLastChar($string){
            if($string[mb_strlen($string) - 1] !== DS){
                return $string."/";
            }
            else{
                return $string;
            }
        }
    }