<?php
    namespace App\Cms;

    use Fluid\Form\Form;

    class InformationsController extends ControllerPlugin{

        public function indexAction(){

            $form = new Form($this->Form->Informations, ['action' => '/manage/informations']);

            if($this->Request->hasParam('formName')){
                $this->setLayout("empty");
                $form->setBackUrl("/manage/informations");
                $data = $form->chceckRequest();

                if($form->processRequest($data)){
                    $values = $form->getValues();
                }

                echo $form->displayResponse();
            }
            else{
                $this->View->form = $form;
            }


        }
    }