<?php
    namespace App\Client;

    use Fluid;

    class ErrorController extends Fluid\Controller{

        public function indexAction($message = null){
            $this->View->error = $message;
            $this->View->layout = "error";

            $this->View->renderLayout();
        }
    }