<?php
    namespace App\Forum;

    use Fluid;
    use Fluid\Session\Session;

    class IndexController extends ControllerPlugin {

        private $Category;

        public function begin(){
            $this->Category = new CategoryModel();
        }

        public function indexAction(){
            $this->View->data  = $this->Category->getAllWithGroups();
        }
    }