<?php
    namespace App\Forum;

    use Fluid;
    use Fluid\TableView\TableView;

    class GroupController extends ControllerPlugin {

        private $Topic;

        public function begin(){
            $this->Topic = new TopicModel();
        }

        public function indexAction(){
            if($this->Request->hasParams(["groupId"])){
                $id = $this->Request->getParam("groupId");
                $data = $this->Topic->prepareTableViewData([['hidden', '=', '0'], ['group_id', '=', $id]]);

                $data['rowsActions'] = [['Otwórz', ['route' => 'topic'], 'open']];
                $data['tableActions'] = [['Dodaj nowy element', '/manage/pages/add', 'add'], ['Usuń wszystkie', '/manage/pages/dellete-all', 'deleteAll']];

                $table = new TableView($data);

                $this->View->table = $table;
            }
            else{
                throw new \Exception("Param id is empty.");
            }
        }
    }