<?php
    namespace App;

    class RegisterForm extends UserModel{

        public $fields = [
            'UserLogin' => ['Login', 'required' => true],
            'UserPassword' => ['Hasło', 'required' => true],
            'UserPassword_confirm' => ['Ponów hasło', 'required' => true],
            'UserEmail' => ['Email', 'required' => true, 'validate' => 'email'],
            //'UserEmail' => ['Email', 'accept' => ['image', 'video'], 'required' => true],
        ];
    }