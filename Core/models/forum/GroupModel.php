<?php
    namespace App\Forum;

    use Fluid\Model\Model;

    class GroupModel extends Model{

        protected function after($data){
            $newData = [];

            foreach($data AS $group){
                if($group['Group']['hidden'] === 1){
                    continue;
                }

                $newData[] = $group;
            }

            return $newData;
        }
    }