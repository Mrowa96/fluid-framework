<?php
    namespace App\Forum;

    use Fluid\Model\Table;

    class TopicTable extends Table{

        public $schema = [
            'TopicId' => [
                'name' => 'Id',
                'primaryKey' => true,
                'hidden' => true
            ],
            'TopicName' => [
                'name' => 'Tytuł'
            ],
        ];
    }