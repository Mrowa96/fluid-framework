<?php
    namespace App\Forum;

    use Fluid\Model\Model;

    class CategoryModel extends Model{

        private $Group;

        protected function begin(){
            $this->Group = new GroupModel();
        }

        protected function after($data){
            $newData = [];

            foreach($data AS $category){
                if($category['Category']['hidden'] === 1){
                    continue;
                }

                $newData[] = $category;
            }

            return $newData;
        }

        public function getAllWithGroups(){
            $categories = $this->fetchAll(null, null, null, [['position', 'ASC']]);
            $results = [];

            if(!empty($categories)){
                foreach($categories AS $category){
                    $category['Category']['groups'] = $this->Group->fetchAll([['category_id', '=', $category['Category']['id']]]);
                    $results[] = $category;
                }
            }

            return $results;
        }
    }