<?php
    namespace App\Forum;

    use App\UserModel;
    use Fluid\Model\Model;

    class TopicModel extends Model{

        protected $User;
        protected $Group;
        protected $TopicTable;

        public function begin(){
            $this->User = new UserModel();
            $this->Group = new GroupModel();
            $this->TopicTable = new TopicTable();
        }
    }