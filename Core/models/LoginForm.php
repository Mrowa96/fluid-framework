<?php
    namespace App;

    class LoginForm extends UserModel{

        public $fields = [
            'UserLogin' => ['Login', 'required' => true],
            'UserPassword' => ['Hasło', 'required' => true],
        ];
    }