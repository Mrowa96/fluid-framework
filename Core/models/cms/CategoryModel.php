<?php
    namespace App\Cms;

    use Fluid\Model\Model;

    class CategoryModel extends Model{

        public function getForIndex(){
            $results = $this->Categories->fetchAll([['active', '=', '1'], ['module_id', '!=', '0']]);

            foreach($results AS &$result){
                $module = $this->Modules->fetchRow([['id', '=', $result['Categories']['module_id']]]);
                if(isset($module['Modules']['url'])){
                    $result['Categories']['url'] = "/manage".$module['Modules']['url'];
                }
                else{
                    $result['Categories']['url'] = "/manage";
                }
            }

            return $results;
        }

        public function getForMenu(){
            $results = $this->Categories->fetchAll([['active', '=', '1']]);

            foreach($results AS &$result){
                $module = $this->Modules->fetchRow([['id', '=', $result['Categories']['module_id']]]);

                if(isset($module['Modules']['url'])){
                    $result['Categories']['url'] = "/manage".$module['Modules']['url'];
                }
                else{
                    $result['Categories']['url'] = "/manage";
                }

            }

            return $results;
        }
    }