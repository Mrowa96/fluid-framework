<?php
    namespace App\Cms;

    use App\PageModel;

    class PageTable extends PageModel{

        public $schema = [
            'PagesId' => [
                'name' => 'Id',
                'primaryKey' => true
            ],
            'PagesTitle' => [
                'name' => 'Tytuł'
            ],
            'PagesSpecial' => [
                'name' => 'Wyróznij',
                'type' => 'boolean'
            ],
            'AddressName' => [
                'name' => 'Imię'
            ],
            'AddressSurname' => [
                'name' => 'Nazwisko'
            ]
        ];
    }