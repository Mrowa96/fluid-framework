<?php
    namespace App\Cms;

    use App\InformationModel;

    class InformationForm extends InformationModel{

        public $fields = [
            'InformationsName' => ['Nazwa firmy'],
            'InformationsSubname' => ['Dodatkowa treść'],
        ];
    }