<?php
    namespace App;

    use Fluid\Model\Model;

    class InstanceExternalModel extends Model{

        protected $connectionName = "system";
        protected $connection = [
            'pdoType' => 'mysql',
            'host' => 'localhost',
            'login' => 'root',
           'password' => 'root',
            'name' => 'fluid-system',
           'port' => '3306',
            'prefix' => 'fs_'
       ];
    }